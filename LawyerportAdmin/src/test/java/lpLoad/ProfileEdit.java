package lpLoad;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ProfileEdit {

	
	public WebDriver driver;
	private Robot robot;
	
/**
 * Initially waits for n seconds before starting to poll for element in
 * intervals and stops polling for that element after given time.
 * 
 * @param pauseUntilStart
 *            The amount of time to wait until starting to look for element
 *            in seconds.
 * @param timeToWait
 *            The amount of time to to continue to look for element before
 *            timing out in seconds.
 * @param timesToRetry
 *            The amount of times to retry looking within the given time.
 *            NOTE: this must be less than the max timeToWait or it will
 *            only try once.
 * @return The wait object to use to wait on the element given to it.
 */
public FluentWait<WebDriver> wait(int pauseUntilStart, int timesToRetry, int timeToWait) {
	int poll = timesToRetry;
	if (timesToRetry > timeToWait) {
		poll = timeToWait;
	}
	if (pauseUntilStart > 0) {
		LocalTime timeStamp = LocalTime.now();
		while (LocalTime.now().isBefore(
				timeStamp.plusSeconds(pauseUntilStart))) {
			// Stall for a few seconds before polling for element and
			// waiting between...
		}
	}
	return new FluentWait<WebDriver>(driver)
			.withTimeout(timeToWait, TimeUnit.SECONDS)
			.pollingEvery(poll, TimeUnit.SECONDS)
			.ignoring(StaleElementReferenceException.class)
			.ignoring(NoSuchElementException.class)
			.ignoring(ElementNotVisibleException.class);
}

	
	protected void forceWait(int sleepTime, String text){
		System.out.println(text + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
	
	protected static void forceWait(int sleepTime){
		System.out.println("Wait..." + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
	
@Parameters({"browserName","version","platform"})
@BeforeClass
public void beforeMethod(String browserName, String version, String platform) throws MalformedURLException{
	DesiredCapabilities cap;
	
	if(browserName.equals("chrome"))
		cap=DesiredCapabilities.chrome();
	else cap = DesiredCapabilities.firefox();;
	
	cap.setJavascriptEnabled(true);
	cap.setVersion(version);
	cap.setCapability("Platform", platform);
			
	driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
	
	

}

public void setUser(String text){
	WebElement element = driver.findElement(By.id("username"));
	element.sendKeys(text);
//	Assert.assertEquals(element.getAttribute("value"), text);
}

public void setPass(String text){
	WebElement element = driver.findElement(By.id("password"));
	element.sendKeys(text);
	//Assert.assertEquals(element.getAttribute("value"), text);
}

public void setClient(String text){
	WebElement element = driver.findElement(By.id("clientCode"));
	element.sendKeys(text);
	//Assert.assertEquals(element.getAttribute("value"), text);
}

public void down() throws AWTException{
	robot = new Robot();
	robot.setAutoDelay(1000);
	robot.keyPress(KeyEvent.VK_DOWN);
    robot.keyRelease(KeyEvent.VK_DOWN);
  
}

public void enter() throws AWTException{
	robot.setAutoDelay(500);
robot.keyPress(KeyEvent.VK_ENTER);
robot.keyRelease(KeyEvent.VK_ENTER);
}

@Parameters({ "username", "password", "clientCode"})
@Test(priority=1)
public void test4(String username, String password, String clientCode) throws Throwable{
	Random rando = new Random();
	int random = rando.nextInt(10)+1;
	pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory geni = PFCaller.genericFactory();
	driver.get("https://beta1.lawyerport.com");
	
	wait(2,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
	wait(3,1,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
	
	System.out.println("Login Page Loaded");

	setUser(username);
	setPass(password);
	setClient(clientCode);
	
	forceWait(2, "user data entered, +");
	WebElement signIn = driver.findElement(By.id("submitButton"));
	
	signIn.click();
	forceWait(3,"Loading Main Page, +");

	
	wait(3,2,120).until(ExpectedConditions.presenceOfElementLocated((By.id("navDir"))));
	if(driver.getCurrentUrl().contains("lb/cas")){System.err.println("log-in Failure");}
	WebElement directoryTab = driver.findElement(By.id("navDir"));
	directoryTab.click();
	forceWait(3, "loading directory, +");
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-4"))).sendKeys(geni.randomLastName());
	forceWait(2);
	wait(1,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("dir-run-advanced-search"))).click();
	System.out.println("getting Directory");
	wait(2,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@class, 'search-click-through')])["+random+"]"))).click();
	
	wait(4,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("edit-profile"))).click();
	
	System.out.println("Editing Title");
	wait(4,3,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("button-title-edit"))).click();
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).sendKeys("DR.");
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-5"))).sendKeys("Rubenstein");
	if(random<3){
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-4"))).sendKeys("skip");
	}
	if(random>2&&random<7){
		wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-4"))).sendKeys("doc");
	}
	if(random>6){
		wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-4"))).sendKeys("ol' bones");
	}
	wait(2,1,120).until(ExpectedConditions.elementToBeClickable(By.id("form-button-Save"))).click();
	System.out.println("Editing Language");
	wait(2,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("pA-toc-biography"))).click();
	wait(3,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("button-persons-bio-lang-edit"))).click();

	wait(3,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).clear();
	wait(1,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).sendKeys("French");
	wait(2,1,120).until(ExpectedConditions.elementToBeClickable(By.id("form-button-Save"))).click();

	
	forceWait(1, "Profile Edit Complete");
	forceWait(100, "Waiting To Launch next browser");
}

@Parameters({ "username", "password", "clientCode"})
@Test(priority=1)
public void test5(String username, String password, String clientCode) throws Throwable{
	Random rando = new Random();
	int random = rando.nextInt(10)+1;
	pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory geni = PFCaller.genericFactory();
	driver.get("https://beta1.lawyerport.com");
	
	wait(2,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
	wait(3,1,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
	
	System.out.println("Login Page Loaded");

	setUser(username);
	setPass(password);
	setClient(clientCode);
	
	forceWait(2, "user data entered, +");
	WebElement signIn = driver.findElement(By.id("submitButton"));
	
	signIn.click();
	forceWait(3,"Loading Main Page, +");


	
	wait(3,2,120).until(ExpectedConditions.presenceOfElementLocated((By.id("navDir"))));
	if(driver.getCurrentUrl().contains("lb/cas")){System.err.println("log-in Failure");}
	WebElement directoryTab = driver.findElement(By.id("navDir"));
	directoryTab.click();
	forceWait(3, "loading directory, +");
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-4"))).sendKeys(geni.randomLastName());
	forceWait(2);
	wait(1,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("dir-run-advanced-search"))).click();
	System.out.println("getting Directory");
	wait(2,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@class, 'search-click-through')])["+random+"]"))).click();
	
	wait(4,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("edit-profile"))).click();
	
	System.out.println("Editing Title");
	wait(4,3,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("button-title-edit"))).click();
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).sendKeys("DR.");
	wait(2,1,120).until(ExpectedConditions.elementToBeClickable(By.id("form-button-Save"))).click();
	System.out.println("Editing Language");
	wait(2,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("pA-toc-biography"))).click();
	wait(3,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("button-persons-bio-lang-edit"))).click();

	wait(3,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).clear();
	wait(1,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).sendKeys("French");
	wait(2,1,120).until(ExpectedConditions.elementToBeClickable(By.id("form-button-Save"))).click();

	
	forceWait(1, "Profile Edit Complete");
	forceWait(100, "Waiting To Launch next browser");
}

@Parameters({ "username", "password", "clientCode"})
@Test(priority=2)
public void test1(String username, String password, String clientCode) throws Throwable{
	Random rando = new Random();
	int random = rando.nextInt(10)+1;
	pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory geni = PFCaller.genericFactory();
	driver.get("https://beta1.lawyerport.com");
	
	wait(2,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
	wait(3,1,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
	
	System.out.println("Login Page Loaded");

	setUser(username);
	setPass(password);
	setClient(clientCode);
	
	forceWait(2, "user data entered, +");
	WebElement signIn = driver.findElement(By.id("submitButton"));
	
	signIn.click();
	forceWait(3,"Loading Main Page, +");

	
	wait(3,2,120).until(ExpectedConditions.presenceOfElementLocated((By.id("navDir"))));
	if(driver.getCurrentUrl().contains("lb/cas")){System.err.println("log-in Failure");}
	WebElement directoryTab = driver.findElement(By.id("navDir"));
	directoryTab.click();
	forceWait(3, "loading directory, +");
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-4"))).sendKeys(geni.randomLastName());
	forceWait(2);
	wait(1,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("dir-run-advanced-search"))).click();
	System.out.println("getting Directory");
	wait(2,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@class, 'search-click-through')])["+random+"]"))).click();
	
	wait(4,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("edit-profile"))).click();
	
	System.out.println("Editing Title");
	wait(4,3,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("button-title-edit"))).click();
	wait(3,1,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).sendKeys("DR.");
	wait(2,1,120).until(ExpectedConditions.elementToBeClickable(By.id("form-button-Save"))).click();
	System.out.println("Editing Language");
	wait(2,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("pA-toc-biography"))).click();
	wait(3,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("button-persons-bio-lang-edit"))).click();

	wait(3,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).clear();
	wait(1,2,120).until(ExpectedConditions.visibilityOfElementLocated(By.id("form-field-0"))).sendKeys("French");
	wait(2,1,120).until(ExpectedConditions.elementToBeClickable(By.id("form-button-Save"))).click();

	
	forceWait(1, "Profile Edit Complete");
	forceWait(100, "Waiting To Launch next browser");
}

@AfterClass
public void afterMethod(){
	try{
		driver.close();
	} catch(Exception ignore){
	
	}
	try{
		driver.close();
	} catch(Exception ignore){

}
}}
