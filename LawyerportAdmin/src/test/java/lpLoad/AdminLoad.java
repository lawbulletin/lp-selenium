package lpLoad;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pageFactoryLibrary.pagefactorylibaray.AddContactFactory;
import pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory;
import pageFactoryLibrary.pagefactorylibaray.OrgProfilePageFactory;


public class AdminLoad{

	public String baseURL= "https://beta1.lawyerport.com";
	private String  node1, node2, node3;
	public static WebDriver driver;
	
//	private String chromeDriverLocal = "C:/Users/bgalatzer-levy/Desktop/JavaLibs/chromedriver.exe";
   public static String hub= "http://localhost:4444/grid/register";
   public static String hub2= "http://54.149.189.30:1111/wd/hub";
   public static String chromeLocal = "C:/GridResources/chromedriver.exe";
   
  //  @Parameters({ "username", "password", "clientCode"})
	public static WebDriver getDriver() throws Throwable{
		
		DesiredCapabilities capability = null;
		System.out.println("firefox");
		 capability= DesiredCapabilities.firefox();
		capability.setBrowserName("firefox");
		capability.setVersion("35.0.1");
		capability.setJavascriptEnabled(true);
			//172.16.2.184   
			// chrome version = 44.0.2403.125 m, firefox = 35.0.1
		if (driver != null) {
				
				return driver;
			}
			driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
		
		return driver;
		}
		
	
	public RemoteWebDriver getRemoteWebDriver(String browser, String urlToHub) {

		DesiredCapabilities capability;
		switch (browser.toLowerCase().trim()) {

		case "chrome":
			capability = DesiredCapabilities.chrome();
			break;
		case "internet explorer":
			capability = DesiredCapabilities.internetExplorer();
			break;
		default:
			capability = DesiredCapabilities.firefox();
			break;
		}

		try {
			return new RemoteWebDriver(new URL(urlToHub), capability);
		} catch (Exception badUrl) {
			System.err.println(badUrl);
			return null;
		}
	}
	protected void forceWait(int sleepTime, String text){
		System.out.println(text + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
	
	protected static void forceWait(int sleepTime){
		System.out.println("Wait..." + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
	@BeforeClass(alwaysRun=true)
	public void setUp() throws Throwable{
		System.out.println("Initiating Admin Load Test....");
	     System.out.println();
	     System.out.println("Statements that contain a \"+\" followed by a number indicate that the test has been intenionally");
	     System.out.println("    delayed to emulate normal use or allow an element to load properly");
	     System.out.println();
			
	       driver =  getDriver();//getRemoteWebDriver("firefox",hub);
	       driver.get(baseURL);
		   forceWait(2);
	}
	
	@Test(groups="orgTest")
	@Parameters({ "username", "password", "clientCode"})
	public void kick(String username, String password, String clientCode){
	
     
	
		//Assert.assertEquals(driver.getTitle(), GenericLoadFactory.PAGE_TITLE);
		
		wait(5,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
		wait(5,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
		//WebElement userField = driver.findElement(By.id("username"));
		//Assert.assertTrue(userField.isDisplayed());
		System.out.println("Login Page Loaded");

		setUser(username);
		setPass(password);
		setClient(clientCode);
		
		forceWait(1, "user data entered, +");
		WebElement signIn = driver.findElement(By.id("submitButton"));
		
		signIn.click();
		forceWait(3,"Loading Main Page, +");
	
	}
	//*[@id='dynContent1']/div[4]/div[2]/p/a
	
	/**
	 * Initially waits for n seconds before starting to poll for element in
	 * intervals and stops polling for that element after given time.
	 * 
	 * @param pauseUntilStart
	 *            The amount of time to wait until starting to look for element
	 *            in seconds.
	 * @param timeToWait
	 *            The amount of time to to continue to look for element before
	 *            timing out in seconds.
	 * @param timesToRetry
	 *            The amount of times to retry looking within the given time.
	 *            NOTE: this must be less than the max timeToWait or it will
	 *            only try once.
	 * @return The wait object to use to wait on the element given to it.
	 */
	public FluentWait<WebDriver> wait(int pauseUntilStart, int timesToRetry,
			int timeToWait) {
		int poll = timesToRetry;
		if (timesToRetry > timeToWait) {
			poll = timeToWait;
		}
		if (pauseUntilStart > 0) {
			LocalTime timeStamp = LocalTime.now();
			while (LocalTime.now().isBefore(
					timeStamp.plusSeconds(pauseUntilStart))) {
				// Stall for a few seconds before polling for element and
				// waiting between...
			}
		}
		return new FluentWait<WebDriver>(driver)
				.withTimeout(timeToWait, TimeUnit.SECONDS)
				.pollingEvery(poll, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class)
				.ignoring(NoSuchElementException.class)
				.ignoring(ElementNotVisibleException.class);
	}
	
	public void setUser(String text){
		WebElement element = driver.findElement(By.id("username"));
		element.sendKeys(text);
		Assert.assertEquals(element.getAttribute("value"), text);
	}
	
	
	public void setPass(String text){
		WebElement element = driver.findElement(By.id("password"));
		element.sendKeys(text);
		Assert.assertEquals(element.getAttribute("value"), text);
	}
	
	public void setClient(String text){
		WebElement element = driver.findElement(By.id("clientCode"));
		element.sendKeys(text);
		Assert.assertEquals(element.getAttribute("value"), text);
	}
	
	public String getUsername(){
		WebElement element = driver.findElement(By.id("welcome-name"));
		String temp = element.getText().toString().toUpperCase();
		String user = temp.substring(9);
		return user;
	}
	
	@AfterClass(alwaysRun=true)
	public void tearDown(){
		driver.close();
		driver.quit();
		}

	//threadPoolSize = 3, invocationCount = 3, timeOut = 1000
	
	@Test(dependsOnMethods="kick", groups={"orgTest", "profileTest"})	
	public void checkLanding() throws MalformedURLException, Throwable{
		GenericLoadFactory geni = null;
		try {
			 geni = PFCaller.genericFactory();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		String user;
		
		user = getUsername();
		System.out.println(user);
		Assert.assertTrue(user.contains("ZZZEDIT"), "Username Displayed Correctly");
	}
	
	@Test(dependsOnMethods="checkLanding", groups={"orgTest", "profileTest"})
	public void findDirectory(){
		WebElement directoryTab = driver.findElement(By.id("navDir"));
		forceWait(3,"Locating Navigation Bar,");
		
		directoryTab.click();
		forceWait(3, "loading directory, +");
	}
	
	@Test(dependsOnMethods="findDirectory", groups={"orgTest"})
	public void searchOrg() throws Throwable{
		
		OrgProfilePageFactory op= PFCaller.orgProfileFactory();
		GenericLoadFactory geni = PFCaller.genericFactory();
	String org = geni.randomOrg();
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//li/a)[3]"))).click();
	
	forceWait(1, "loading org directory page, +");
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("form-field-0"))).sendKeys(org);
	forceWait(1, "entering organization name +");
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("dir-org-run-advanced-search"))).click();
	forceWait(1,"Loading Results +");
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//p/a)[2]"))).click();
	forceWait(1, "Loading Organization page +");
	//Assert.assertEquals(getDriver().findElement(By.xpath("//*[@id='dynContent1']/div[3]")).getText().toString(), org, org+" organization profile found");
	//Assert.assertEquals(op.getOrgTitle().getText().toString(), org);
	
 }
	

	@Test(dependsOnMethods="searchOrg",groups={"orgTest"})
	public void openEditFields() throws Throwable{
		forceWait(3,"Retrieving Page Title, +");
		OrgProfilePageFactory orgProfile = PFCaller.orgProfileFactory();
		String s = orgProfile.getOrgTitle().getText().toString();
		System.out.println(s);
		wait(3,3,15).until(ExpectedConditions.elementToBeClickable(orgProfile.getEditButton())).click();
		forceWait(2, "Waiting for Edit Fields to populate +");
		
	}
	
	@Test(dependsOnMethods="openEditFields", groups={"orgTest"})
	public void openContactModel() throws Throwable{
		AddContactFactory contact = PFCaller.contactFactory();
		OrgProfilePageFactory orgProfile = PFCaller.orgProfileFactory();

	wait(3,3,15).until(ExpectedConditions.elementToBeClickable(orgProfile.getAddContactInfoButton())).click();
	forceWait(3,"Waiting for add contact model to load, +");

	String c = contact.getAddContactInfoHeader().getText().toString();
	Assert.assertEquals(c.contains("Add Contact"), true, "Add Contact Modal Opened Successfully");
	
	}
	
	@Test(dependsOnMethods="openContactModel", groups={"orgTest"})
	public void enterNewAddress() throws Throwable{
		AddContactFactory contact = PFCaller.contactFactory();
		String zip, city,office, address = "123 LBPC St.", phone = "312-311-2222";
		zip = "60606";
		office="New Office";
		city = "Chicago";
	
		contact.getAddSOfficeName().sendKeys(office);
		contact.getAddStreetAddress().sendKeys(address);
		contact.getAddZip().sendKeys(zip);
		contact.getAddCity().sendKeys(city);
		contact.getAddPhone().sendKeys(phone);
		forceWait(3);
		contact.getSave().click();
	}
	
	@Test(dependsOnMethods="enterNewAddress", groups="orgTest")
	public void verifyEdit() throws Throwable{
		forceWait(3);
		OrgProfilePageFactory orgProfile = PFCaller.orgProfileFactory();
		System.out.println(orgProfile.getAddressLine1().getText().toString());
		System.out.println(orgProfile.getAddressLine2().getText().toString());
		System.out.println(orgProfile.getAddressLine3().getText().toString());
		
	}
	

	
}
