package lpLoad;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import pageFactoryLibrary.pagefactorylibaray.AddContactFactory;
import pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory;

public class OrgProfile extends ProfileEdit{
	
	
	
	private String address = "123 LBPC St.";
	private String phone = "312-311-2222";
	private String zip = "60606";
	private String city = "Chicago";


	@Parameters({ "username", "password", "clientCode"})
	@Test(priority=1)
	public void test3(String username, String password, String clientCode) throws Throwable{
		GenericLoadFactory geni = PFCaller.genericFactory();
		AddContactFactory contact = PFCaller.contactFactory();

		driver.get("https://beta1.lawyerport.com");
		
		wait(2,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
		wait(3,1,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
		
		System.out.println("Login Page Loaded");

		setUser(username);
		setPass(password);
		setClient(clientCode);
		
		forceWait(1, "user data entered, +");
		WebElement signIn = driver.findElement(By.id("submitButton"));
		
		signIn.click();
		System.out.println("Loading Main Page, +");

		
		wait(4,2,120).until(ExpectedConditions.presenceOfElementLocated((By.id("navDir"))));
		if(driver.getCurrentUrl().contains("lb/cas")){System.err.println("log-in Failure");}
		WebElement directoryTab = driver.findElement(By.id("navDir"));
		directoryTab.click();
		forceWait(3, "loading directory, +");
		
		String org = geni.randomOrg();
		wait(1, 1, 120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-org")))).click();
		
		forceWait(1, "loading org directory page, +");
		wait(2, 1, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("form-field-0")))).sendKeys(org);
		forceWait(1, "entering organization name +");
		wait(1, 1, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("div[id='dir-org-run-advanced-search-container'] a:nth-of-type(2)")))).click();
		forceWait(1,"Loading Results +");
		wait(1, 2, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//p/a)[2]")))).click();
		forceWait(1, "Loading Organization page +");
		
		forceWait(3,"Retrieving Page Title, +");


		wait(1,2,120).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("edit-profile")))).click();
		forceWait(2, "Waiting for Edit Fields to populate +");
		
		wait(2,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='button-org-contact-add']")))).click();
		forceWait(3,"Waiting for add contact model to load, +");
		
		
		wait(3,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-2")))).sendKeys(address);
		wait(3,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-1")))).sendKeys("Run's House");
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-10")))).sendKeys(zip);
		wait(3,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-3")))).sendKeys("Only criminals use PO Boxes");
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-9")))).sendKeys(city);
		wait(3,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-19")))).sendKeys("www.lawyerport.com");
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-11")))).sendKeys(phone);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-button-Save")))).click();
		
		
		forceWait(10, "Test Completed! ");
		forceWait(100, "Waiting To Launch next browser");
		
	}
  
	@Parameters({ "username", "password", "clientCode"})
	@Test(priority=2)
	public void test2(String username, String password, String clientCode) throws Throwable{
		GenericLoadFactory geni = PFCaller.genericFactory();
		AddContactFactory contact = PFCaller.contactFactory();

		driver.get("https://beta1.lawyerport.com");
		
		wait(2,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
		wait(3,1,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
		
		System.out.println("Login Page Loaded");

		setUser(username);
		setPass(password);
		setClient(clientCode);
		
		forceWait(1, "user data entered, +");
		WebElement signIn = driver.findElement(By.id("submitButton"));
		
		signIn.click();
		System.out.println("Loading Main Page, +");
		
		wait(4,2,120).until(ExpectedConditions.presenceOfElementLocated((By.id("navDir"))));
		if(driver.getCurrentUrl().contains("lb/cas")){System.err.println("log-in Failure");}
		WebElement directoryTab = driver.findElement(By.id("navDir"));
		directoryTab.click();
		forceWait(3, "loading directory, +");
		
		String org = geni.randomOrg();
		wait(1, 1, 120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-org")))).click();
		
		forceWait(1, "loading org directory page, +");
		wait(2, 1, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("form-field-0")))).sendKeys(org);
		forceWait(1, "entering organization name +");
		wait(1, 1, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("div[id='dir-org-run-advanced-search-container'] a:nth-of-type(2)")))).click();
		forceWait(1,"Loading Results +");
		wait(1, 2, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//p/a)[2]")))).click();
		forceWait(1, "Loading Organization page +");
		
		forceWait(3,"Retrieving Page Title, +");


		wait(1,2,120).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("edit-profile")))).click();
		forceWait(2, "Waiting for Edit Fields to populate +");
		
		wait(2,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='button-org-contact-add']")))).click();
		forceWait(3,"Waiting for add contact model to load, +");
		
		
		wait(3,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-2")))).sendKeys(address);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-10")))).sendKeys(zip);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-9")))).sendKeys(city);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-11")))).sendKeys(phone);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-button-Save")))).click();
		
		
		forceWait(10, "Test Completed! ");
		forceWait(100, "Waiting To Launch next browser");
		
	}
	
	@Parameters({ "username", "password", "clientCode"})
	@Test(priority=2)
	public void test6(String username, String password, String clientCode) throws Throwable{
		GenericLoadFactory geni = PFCaller.genericFactory();
		AddContactFactory contact = PFCaller.contactFactory();

		driver.get("https://beta1.lawyerport.com");
		
		wait(2,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
		wait(3,1,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
		
		System.out.println("Login Page Loaded");

		setUser(username);
		setPass(password);
		setClient(clientCode);
		
		forceWait(1, "user data entered, +");
		WebElement signIn = driver.findElement(By.id("submitButton"));
		
		signIn.click();
		System.out.println("Loading Main Page, +");

		
		wait(4,2,120).until(ExpectedConditions.presenceOfElementLocated((By.id("navDir"))));
		if(driver.getCurrentUrl().contains("lb/cas")){System.err.println("log-in Failure");}
		WebElement directoryTab = driver.findElement(By.id("navDir"));
		directoryTab.click();
		forceWait(3, "loading directory, +");
		
		String org = geni.randomOrg();
		wait(1, 1, 120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-org")))).click();
		
		forceWait(1, "loading org directory page, +");
		wait(2, 1, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("form-field-0")))).sendKeys(org);
		forceWait(1, "entering organization name +");
		wait(1, 1, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("div[id='dir-org-run-advanced-search-container'] a:nth-of-type(2)")))).click();
		forceWait(1,"Loading Results +");
		wait(1, 2, 120).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("(//p/a)[2]")))).click();
		forceWait(1, "Loading Organization page +");
		
		forceWait(3,"Retrieving Page Title, +");


		wait(1,2,120).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("edit-profile")))).click();
		forceWait(2, "Waiting for Edit Fields to populate +");
		
		wait(2,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='button-org-contact-add']")))).click();
		forceWait(3,"Waiting for add contact model to load, +");
		
		
		wait(3,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-2")))).sendKeys(address);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-10")))).sendKeys(zip);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-9")))).sendKeys(city);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-field-11")))).sendKeys(phone);
		
		wait(1,2,120).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form-button-Save")))).click();
		
		
		forceWait(10, "Test Completed! ");
		forceWait(100, "Waiting To Launch next browser");
		
	}
}
