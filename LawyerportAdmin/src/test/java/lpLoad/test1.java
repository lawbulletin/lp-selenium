package lpLoad;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pageFactoryLibrary.pagefactorylibaray.AddContactFactory;
import pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory;
import pageFactoryLibrary.pagefactorylibaray.OrgProfilePageFactory;

public class test1 extends AdminLoad{
	@BeforeClass(alwaysRun=true)
	public void setUp() throws Throwable{
		System.out.println("Initiating Admin Load Test....");
	     System.out.println();
	     System.out.println("Statements that contain a \"+\" followed by a number indicate that the test has been intenionally");
	     System.out.println("    delayed to emulate normal use or allow an element to load properly");
	     System.out.println();
			
	       driver =  getDriver();//getRemoteWebDriver("firefox",hub);
	       driver.get(baseURL);
		   forceWait(2);
	}
	
	@Test(groups="orgTest")
	@Parameters({ "username", "password", "clientCode"})
	public void kick(String username, String password, String clientCode){
	
     
	
		//Assert.assertEquals(driver.getTitle(), GenericLoadFactory.PAGE_TITLE);
		
		wait(5,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("signin-button")))).click();
		wait(5,2,20).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("username"))));
		//WebElement userField = driver.findElement(By.id("username"));
		//Assert.assertTrue(userField.isDisplayed());
		System.out.println("Login Page Loaded");

		setUser(username);
		setPass(password);
		setClient(clientCode);
		
		forceWait(1, "user data entered, +");
		WebElement signIn = driver.findElement(By.id("submitButton"));
		
		signIn.click();
		forceWait(3,"Loading Main Page, +");
	
	}
	
	@AfterClass(alwaysRun=true)
	public void tearDown(){
		driver.close();
		driver.quit();
		}

	//threadPoolSize = 3, invocationCount = 3, timeOut = 1000
	
	@Test(dependsOnMethods="kick", groups={"orgTest", "profileTest"})	
	public void checkLanding() throws Throwable{
		
		
			try {
				GenericLoadFactory	geni = PFCaller.genericFactory();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		
		
		String user;
		
		user = getUsername();
		System.out.println(user);
		Assert.assertTrue(user.contains("ZZZEDIT"), "Username Displayed Correctly");
	}
	
	@Test(dependsOnMethods="checkLanding", groups={"orgTest", "profileTest"})
	public void findDirectory(){
		WebElement directoryTab = driver.findElement(By.id("navDir"));
		forceWait(3,"Locating Navigation Bar,");
		
		directoryTab.click();
		forceWait(3, "loading directory, +");
	}
	
	@Test(dependsOnMethods="findDirectory", groups={"orgTest"})
	public void searchOrg() throws Throwable{
		
		OrgProfilePageFactory op= PFCaller.orgProfileFactory();
		GenericLoadFactory geni = PFCaller.genericFactory();
	String org = geni.randomOrg();
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//li/a)[3]"))).click();
	
	forceWait(1, "loading org directory page, +");
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("form-field-0"))).sendKeys(org);
	forceWait(1, "entering organization name +");
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("dir-org-run-advanced-search"))).click();
	forceWait(1,"Loading Results +");
	wait(3, 3, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//p/a)[2]"))).click();
	forceWait(1, "Loading Organization page +");
	//Assert.assertEquals(getDriver().findElement(By.xpath("//*[@id='dynContent1']/div[3]")).getText().toString(), org, org+" organization profile found");
	//Assert.assertEquals(op.getOrgTitle().getText().toString(), org);
	
 }
	

	@Test(dependsOnMethods="searchOrg",groups={"orgTest"})
	public void openEditFields() throws Throwable{
		forceWait(3,"Retrieving Page Title, +");
		OrgProfilePageFactory orgProfile = PFCaller.orgProfileFactory();
		String s = orgProfile.getOrgTitle().getText().toString();
		System.out.println(s);
		wait(3,3,15).until(ExpectedConditions.elementToBeClickable(orgProfile.getEditButton())).click();
		forceWait(2, "Waiting for Edit Fields to populate +");
		
	}
	
	@Test(dependsOnMethods="openEditFields", groups={"orgTest"})
	public void openContactModel() throws Throwable{
		AddContactFactory contact = PFCaller.contactFactory();
		OrgProfilePageFactory orgProfile = PFCaller.orgProfileFactory();

	wait(3,3,15).until(ExpectedConditions.elementToBeClickable(orgProfile.getAddContactInfoButton())).click();
	forceWait(3,"Waiting for add contact model to load, +");

	String c = contact.getAddContactInfoHeader().getText().toString();
	Assert.assertEquals(c.contains("Add Contact"), true, "Add Contact Modal Opened Successfully");
	
	}
	
	@Test(dependsOnMethods="openContactModel", groups={"orgTest"})
	public void enterNewAddress() throws Throwable{
		AddContactFactory contact = PFCaller.contactFactory();
		String zip, city,office, address = "123 LBPC St.", phone = "312-311-2222";
		zip = "60606";
		office="New Office";
		city = "Chicago";
	
		contact.getAddSOfficeName().sendKeys(office);
		contact.getAddStreetAddress().sendKeys(address);
		contact.getAddZip().sendKeys(zip);
		contact.getAddCity().sendKeys(city);
		contact.getAddPhone().sendKeys(phone);
		forceWait(3);
		contact.getSave().click();
	}
	
	@Test(dependsOnMethods="enterNewAddress", groups="orgTest")
	public void verifyEdit() throws Throwable{
		
		OrgProfilePageFactory orgProfile = PFCaller.orgProfileFactory();
		wait(5,2,20).until(ExpectedConditions.visibilityOf(orgProfile.getOrgTitle()));
		System.out.println(orgProfile.getAddressLine1().getText().toString());
		System.out.println(orgProfile.getAddressLine2().getText().toString());
		System.out.println(orgProfile.getAddressLine3().getText().toString());
		
	}
	
	
}
