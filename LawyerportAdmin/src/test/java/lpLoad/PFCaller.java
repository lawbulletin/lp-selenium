package lpLoad;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import pageFactoryLibrary.pagefactorylibaray.AddContactFactory;
import pageFactoryLibrary.pagefactorylibaray.DirectoryPageFactory;
import pageFactoryLibrary.pagefactorylibaray.DirectoryResultFactory;
import pageFactoryLibrary.pagefactorylibaray.GenericLoadFactory;
import pageFactoryLibrary.pagefactorylibaray.LPMainPageFactory;
import pageFactoryLibrary.pagefactorylibaray.LandingPageFactory;
import pageFactoryLibrary.pagefactorylibaray.OrgDirectoryPageFactory;
import pageFactoryLibrary.pagefactorylibaray.OrgProfilePageFactory;
import pageFactoryLibrary.pagefactorylibaray.ProfilePageFactory;
import pageFactoryLibrary.pagefactorylibaray.SearchResultsFactory;


public class PFCaller extends AdminLoad{
	
	
	
	public static LandingPageFactory landingFactory() throws Throwable{
		LandingPageFactory pfName = PageFactory.initElements(getDriver(), LandingPageFactory.class);
		return  pfName;
	}
	
	public static AddContactFactory contactFactory() throws Throwable{
		AddContactFactory pfName = PageFactory.initElements(getDriver(), AddContactFactory.class);
		return  pfName;
	}
	
	public static LPMainPageFactory lpMainFactory() throws Throwable{
		LPMainPageFactory pfName = PageFactory.initElements(getDriver(), LPMainPageFactory.class);
		return pfName;
	}
	
	public static OrgProfilePageFactory orgProfileFactory() throws Throwable{
		OrgProfilePageFactory pfName = PageFactory.initElements(getDriver(), OrgProfilePageFactory.class);
		return  pfName;
	}
	
	public static GenericLoadFactory genericFactory() throws Throwable{
		GenericLoadFactory pfName = PageFactory.initElements(getDriver(), GenericLoadFactory.class);
		return  pfName;
	}
	public static DirectoryPageFactory directoryFactory() throws Throwable{
		DirectoryPageFactory pfName = PageFactory.initElements(getDriver(), DirectoryPageFactory.class);
		return pfName;
	}
	
	public static DirectoryResultFactory dirResultsFactory() throws Throwable{
		DirectoryResultFactory pfName = PageFactory.initElements(getDriver(), DirectoryResultFactory.class);
		return pfName;
	}
	
	
	
	public static OrgDirectoryPageFactory orgFactory() throws Throwable{
		OrgDirectoryPageFactory pfName = PageFactory.initElements(getDriver(), OrgDirectoryPageFactory.class);
		return  pfName;
	}
	

	public static ProfilePageFactory profileFactory() throws Throwable{
		ProfilePageFactory pfName =  PageFactory.initElements(getDriver(), ProfilePageFactory.class);
		return pfName;}

	public static SearchResultsFactory searchResultFactoy() throws Throwable{
		SearchResultsFactory pfName = PageFactory.initElements(getDriver(), SearchResultsFactory.class);
		return pfName;}
	
}



