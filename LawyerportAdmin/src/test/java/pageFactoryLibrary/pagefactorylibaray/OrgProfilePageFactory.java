package pageFactoryLibrary.pagefactorylibaray;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OrgProfilePageFactory {

	@FindBy(how = How.CLASS_NAME, using = "profile-title")
	private WebElement orgTitle;
	
	public WebElement getOrgTitle(){
		return orgTitle;
	}
	
	@FindBy(how = How.ID, using = "org-toc-corp-contact-li") 
	 private WebElement  contactInfo;

	 public WebElement getContactInfo(){ return contactInfo;}

	@FindBy(how = How.ID, using = "org-toc-corp-description-li") 
	 private WebElement  corporateDescription;

	 public WebElement getCorporateDescription(){ return corporateDescription;}

	@FindBy(how = How.ID, using = "org-toc-corp-management-li") 
	 private WebElement  corporateManagement;

	 public WebElement getCorporateManagement(){ return corporateManagement;}

	@FindBy(how = How.ID, using = "org-toc-corp-org-li") 
	 private WebElement  corporateOrgnaization;

	 public WebElement getCorporateOrgnaization(){ return corporateOrgnaization;}

	@FindBy(how = How.ID, using = "org-toc-corp-our-people-li") 
	 private WebElement  ourPeople;

	 public WebElement getOurPeople(){ return ourPeople;}

	@FindBy(how = How.ID, using = "org-toc-corp-industries-li") 
	 private WebElement  industries;

	 public WebElement getIndustries(){ return industries;}

	@FindBy(how = How.ID, using = "org-toc-corp-news-pubs-honors-li") 
	 private WebElement  newsPubsHonors;

	 public WebElement getNewsPubsHonors(){ return newsPubsHonors;}

	@FindBy(how = How.ID, using = "org-toc-corp-news-pubs-honors-li") 
	 private WebElement  profilePic;

	 public WebElement getProfilePic(){ return profilePic;}
	 


@FindBy(how = How.ID, using = "edit-profile") 
 private WebElement  editButton;

 public WebElement getEditButton(){ return editButton;}

 @FindBy (how = How.XPATH, using = "//*[@id='dynContent1']/div[3]")
	private WebElement profileTitle;
	
	public WebElement getProfileTitle(){
		return profileTitle;
	}

@FindBy(how = How.XPATH, using = "(//ul/li/div/div/span)[1]") 
 private WebElement  editPrimaryContactInfoButton;

 public WebElement getEditPrimaryContactInfoButton(){ return editPrimaryContactInfoButton;}

@FindBy(how = How.XPATH, using = "(//ul/li/div/div/span)[2]") 
 private WebElement  deletePrimaryContactInfoButton;

 public WebElement getDeletePrimaryContactInfoButton(){ return deletePrimaryContactInfoButton;}

@FindBy(how = How.XPATH, using = "//*[@id='button-org-contact-add']") 
 private WebElement  addContactInfoButton;

 public WebElement getAddContactInfoButton(){ return addContactInfoButton;}

@FindBy(how = How.XPATH, using = "//*[@id='button-org-contact-add']") 
 private WebElement  editContactInfoButton;

 public WebElement getEditContactInfoButton(){ return editContactInfoButton;}

@FindBy(how = How.XPATH, using = "(//ul/li/div/div)[7]") 
 private WebElement  makePrimaryContactInfoButton;

 public WebElement getMakePrimaryContactInfoButton(){ return makePrimaryContactInfoButton;}

@FindBy(how = How.XPATH, using = "(//ul/li/div/div)[8]") 
 private WebElement  address;

 public WebElement getAddressLine1(){ return address;}

@FindBy(how = How.XPATH, using = "(//ul/li/div/div)[9]") 
 private WebElement  phone;

 public WebElement getAddressLine2(){ return phone;}
 
 @FindBy(how = How.XPATH, using = "(//ul/li/div/div)[10]") 
 private WebElement  phone2;

 public WebElement getAddressLine3(){ return phone2;}
 
 @FindBy(how = How.XPATH, using = "(//ul/li/div/div)[11]") 
 private WebElement  addressLine4;

 public WebElement getAddressLine4(){ return addressLine4;}
 
 @FindBy(how = How.XPATH, using = "(//ul/li/div/div)[12]") 
 private WebElement  addressLine5;

 public WebElement getAddressLine5(){ return addressLine5;}
 
 @FindBy(how = How.XPATH, using = "(//ul/li/div/div)[13]") 
 private WebElement  addressLine6;

 public WebElement getAddressLine6(){ return addressLine6;}
}
