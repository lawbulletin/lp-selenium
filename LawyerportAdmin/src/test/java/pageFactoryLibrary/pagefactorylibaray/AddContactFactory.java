package pageFactoryLibrary.pagefactorylibaray;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddContactFactory {



@FindBy(how = How.XPATH, using = "(//div/div/div/div/div)[1]") 
 private WebElement  addContactInfoHeader;

 public WebElement getAddContactInfoHeader(){ return addContactInfoHeader;}

@FindBy(how = How.ID, using = "form-field-2") 
 private WebElement  addStreetAddress;

 public WebElement getAddStreetAddress(){ return addStreetAddress;}

@FindBy(how = How.ID, using = "form-field-0") 
 private WebElement  addSOfficeName;

 public WebElement getAddSOfficeName(){ return addSOfficeName;}

@FindBy(how = How.ID, using = "form-field-9") 
 private WebElement  addCity;

 public WebElement getAddCity(){ return addCity;}

@FindBy(how = How.ID, using = "form-field-10") 
 private WebElement  addZip;

 public WebElement getAddZip(){ return addZip;}

@FindBy(how = How.ID, using = "form-field-11") 
 private WebElement  addPhone;

 public WebElement getAddPhone(){ return addPhone;}

@FindBy(how = How.ID, using = "form-field-17") 
 private WebElement  addEmail;

 public WebElement getAddEmail(){ return addEmail;}

@FindBy(how = How.ID, using = "form-field-18") 
 private WebElement  addWebSite;

 public WebElement getAddWebSite(){ return addWebSite;}

@FindBy(how = How.ID, using = "form-add-next-check-box") 
 private WebElement  createAnotherContactInfo;

 public WebElement getCreateAnotherContactInfo(){ return createAnotherContactInfo;}

@FindBy(how = How.ID, using = "form-button-Close") 
 private WebElement  cancel;

 public WebElement getCancel(){ return cancel;}

@FindBy(how = How.ID, using = "form-button-Save") 
 private WebElement  save;

 public WebElement getSave(){ return save;}
}
