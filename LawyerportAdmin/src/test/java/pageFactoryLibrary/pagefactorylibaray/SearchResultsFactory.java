package pageFactoryLibrary.pagefactorylibaray;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


//This class contains elements for more than one kind of search result page,
//be sure to specify what kind of search results each call/method refers to in its name
public class SearchResultsFactory {

	@FindBy(how = How.XPATH, using = "//a[contains(@class, 'search-click-through')]")
	private WebElement directoryFirstResult;
	
	@FindBy(how = How.XPATH, using = "(//p [contains(@id, 'search-header-sub-title')])[1]")
	private WebElement directoryResultCount;
	
	@FindBy(how = How.XPATH, using = "(//a)[17]")
	private WebElement cdFirstResult;
	
	public WebElement getCDFirstResult(){
		return cdFirstResult;
	}
	
	public WebElement getDirectoryResultCount(){
		return directoryResultCount;
	}
	
	public WebElement getDirectoryFirstResult(){
		return directoryFirstResult;
	}
}
