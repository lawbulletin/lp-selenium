package pageFactoryLibrary.pagefactorylibaray;



import java.util.ArrayList;
import java.util.Random;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class GenericLoadFactory{
	public static final String PAGE_TITLE= "Lawyerport";
	public static final String PAGE_URL = "https://www.beta1.lawyerport.com";
	WebDriver driver;
	
//	public GenericLoadFactory(WebDriver driver){
//		this.driver =driver;
//	}
	
	
	
	
	
	@FindBy(id="navDir") 
	 private WebElement  directoryTab;

	 public WebElement getDirectoryTab(){ return directoryTab;}
	 
	 @FindBy(how = How.ID, using = "//ul/li[1]") 
	 private WebElement  people;

	 public WebElement getPeopleTab(){ return people;}

	@FindBy(how = How.XPATH, using = "//ul/li[2]") 
	 private WebElement  orgTab;

	 public WebElement getOrgTab(){ return orgTab;}

	@FindBy(id = "form-field-0") 
	 private WebElement  firstName;
	
	public WebElement getFirstName(){ return firstName;}

	public WebElement getOrgName(){ return firstName;}
	


@FindBy(how = How.ID, using = "form-field-1") 
 private WebElement  personPrimaryOrgSearch;

 public WebElement getPersonPrimaryOrgSearch(){ return personPrimaryOrgSearch;}

@FindBy(how = How.ID, using = "form-field-4") 
 private WebElement  personLastNameSearch;

 public WebElement getPersonLastNameSearch(){ return personLastNameSearch;}

@FindBy(how = How.ID, using = "dir-org-run-advanced-search") 
 private WebElement  orgSearchButton;

 public WebElement getOrgSearchButton(){ return orgSearchButton;}

@FindBy(how = How.ID, using = "dir-org-reset-advanced-search-bottom") 
 private WebElement  orgResetButton;

 public WebElement getOrgResetButton(){ return orgResetButton;}

@FindBy(how = How.ID, using = "dir-run-advanced-search") 
 private WebElement  personSearchButton;

 public WebElement getPersonSearchButton(){ return personSearchButton;}

@FindBy(how = How.ID, using = "dir-reset-advanced-search-bottom") 
 private WebElement  personResetButton;

 public WebElement getPersonResetButton(){ return personResetButton;}
	
 @FindBy(how = How.XPATH, using = "(//p/a)[2]") 
 private WebElement  firstResult;

 public WebElement getFirstResult(){ return firstResult;}	 

		//Returns a random name from a list of common surnames 
		public String randomLastName(){
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("Jones");
			list.add("Smith");
			list.add("Williams");
			list.add("Stewart");
			list.add("Martinson");
			list.add("Cohen");
			list.add("Levy");
			list.add("Hogan");
			list.add("Patel");
			list.add("Kennedy");
			list.add("Davis");
			list.add("Miller");
			list.add("Taylor");
			list.add("Anderson");
			list.add("White");
			list.add("Jackson");
			list.add("Garcia");
			list.add("Martinez");
			list.add("Lee");
			list.add("Lewis");
			list.add("Walker");
			list.add("Hall");
			list.add("Allen");
			list.add("Young");
			list.add("King");
			list.add("Lopez");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Last Name: " + name);
			return name;
		}

		//Returns a random name from a list of common first names
		public String randomName(){
			
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("John");
			list.add("William");
			list.add("Benjamin");
			list.add("Robert");
			list.add("David");
			list.add("Noah");
			list.add("Liam");
			list.add("Ryan");
			list.add("Bryan");
			list.add("Ethan");
			list.add("Eric");
			list.add("Alexander");
			list.add("Jason");
			list.add("Joshua");
			list.add("Neil");
			list.add("Michael");
			list.add("Alejandro");
			list.add("Dylan");
			list.add("James");
			list.add("Jose");
			list.add("Javiar");
			list.add("James");
			list.add("Matthew");
			list.add("Muhammad");
			list.add("Martin");
			list.add("Joseph");
			list.add("Richard");
			list.add("Charles");
			list.add("Thomas");
			list.add("Nicholas");
			list.add("Jack");
			list.add("Sebastian");
			list.add("Mary");
			list.add("Patricia");
			list.add("Linda");
			list.add("Barbara");
			list.add("Elizabeth");
			list.add("Jennifer");
			list.add("Sophia");
			list.add("Sarah");
			list.add("Emily");
			list.add("Olivia");
			list.add("Emma");
			list.add("Miriam");
			list.add("Maria");
			list.add("Margaret");
			list.add("Dorothy");
			list.add("Rachel");
			list.add("Ashley");
			list.add("Julia");
			list.add("Angela");
			list.add("Zoe");
			list.add("Mia");
			list.add("Isaac");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Name: " + name);
			return name;
		}

		//Returns a random Organization from a list of organizations created for this 
		public String randomOrg(){
			
			int i;
			String organization;
			ArrayList<String> list = new ArrayList<String>();
			list.add("Three-Ring Binder Regulatory Commission");
			list.add("The Council on Fort Ann Relations");
			list.add("Floating Cloud of Mystery");
			list.add("Office of Four and Affairs");
			list.add("Committee for Truth and Reconciliation (LBPC Jurisdiction)");
			list.add("Heart and Knox Academy");
			list.add("Community of Commuter Communes");
			list.add("HiJuan Escotel Associates");
			list.add("Law Firm X");
			list.add("People for the Ethical Treatment of Hiren Patel");
			list.add("The Union of Local Organizations United");
			list.add("Law Bullet Pushing Company");
			list.add("Regulations Solid");
			list.add("Software Testers Anonymous");
			list.add("MudGaard");
			
			
			i = list.size();
			int random = new Random().nextInt(i);
			organization = list.get(random);
			System.out.println("Random Organization: " + organization);
			return organization;
		}
		
}
