package pageFactoryLibrary.pagefactorylibaray;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProfilePageFactory {
	
	@FindBy(id = "persons-bio-heading")
	private WebElement bioHeader;
	
	@FindBy (how = How.XPATH, using = "(//div[contains(@id, 'streaming-JV-list')]/div[contains(@class, 'streaming-content-item')]/div)[1]/a")
	private WebElement JVRTitle1;
	
	@FindBy (how = How.XPATH, using = "(//div[contains(@id, 'streaming-JV-list')]/div[contains(@class, 'streaming-content-item')]/div)[3]/a")
	private WebElement JVRTitle2;
	
	@FindBy(how = How.CLASS_NAME, using = "profile-title")
	private WebElement profileTitle;
	
	@FindBy(how = How.XPATH, using = "(//div[2])[16]")
	private WebElement address;
	
	@FindBy(id = "pA-toc-biography")
	private WebElement biography;
	
	@FindBy(id = "pA-toc-practice-areas")
	private WebElement practiceAreas;
	
	@FindBy(id ="pA-toc-industries" )
	private WebElement industries;
	
	@FindBy(id = "pA-toc-cases" )
	private WebElement casesAndMatters;
	
	@FindBy(id ="pA-toc-news" )
	private WebElement newsPublSpeaking;
	
	@FindBy(id ="pA-toc-affiliations" )
	private WebElement affiliationsAndHonors;
	
	@FindBy(id = "pA-toc-work-history")
	private WebElement workHistory;
	
	@FindBy(id = "pA-toc-admissions" )
	private WebElement admissionsAndEd;
	
	@FindBy(id = "judges-toc-admissions" )
	private WebElement judgeAdmissionsAndEd;
	
	@FindBy(how = How.ID, using = "streaming-content") 
	 private WebElement  streamingContent;

	public WebElement getStreamingContent(){ return streamingContent;}
	 
	@FindBy(id = "button-title-edit")
	private WebElement editName;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@id, 'button-persons-contact-edit-')])[1]")
	private WebElement  editPrimaryContact;
	
	@FindBy(how = How.ID, using = "edit-profile") 
	 private WebElement  editButton;

	 public WebElement getEditButton(){ return editButton;}
	
	@FindBy(id = "button-persons-contact-add")
	private WebElement addContact;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@id, 'button-persons-contact-edit-')])[2]")
	private WebElement editOtherContact;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'button-persons-contact-del-')]")
	private WebElement  deleteContact ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'button-persons-contact-primary')]")
	private WebElement makePrimary;
	
	@FindBy(how = How.XPATH, using = "(//div[3])[5]")
	private WebElement telephone;
	
	@FindBy(id = "button-portrait-edit")
	private WebElement  editPicture;
	
	@FindBy(id = "button-persons-bio-dob-edit")
	private WebElement editDOB;
	
	@FindBy(id = "button-persons-bio-pob-edit")
	private WebElement editPOB;
	
	@FindBy(id = "button-persons-bio-lang-edit")
	private WebElement editLanguages;
	
	@FindBy(id = "button-persons-bio-bio-edit")
	private WebElement editBio;
	
	@FindBy(id="person-case-case-org-link-0")
	private WebElement caseOrgLink1;
	
	@FindBy(id = "streaming-section")
	private WebElement streamingSelection;
	
	@FindBy(id = "stream-container")
	private WebElement streamingContainer;
	
	@FindBy(how = How.CLASS_NAME, using = "user-pic") 
	 private WebElement  userPic;

	public WebElement getUserPic(){ return userPic;}
	
	public WebElement getAddress() {return address;}
	
	public WebElement getTelephone() {return telephone;}
	
	public WebElement getAddContact(){
		return  addContact;
	}
	
	public WebElement getAffiliationsAndHonors (){
		return affiliationsAndHonors;
	}
	
	public WebElement getAdmissionsAndEd (){
		return  admissionsAndEd ;
	}
	
	public WebElement getJudgeAdmissionsAndEd (){
		return  judgeAdmissionsAndEd ;
	}
	
	public WebElement getBiography (){
		return  biography ;
	}
	
	
	
	public WebElement getDeleteContact (){
		return  deleteContact ;
	}
	
	public WebElement getEditDOB (){
		return  editDOB ;
	}
	
	public WebElement getEditLanguages (){
		return   editLanguages;
	}
	
	public WebElement getEditName (){
		return  editName ;
	}
	
	public WebElement getEditPicture(){
		return  editPicture ;
	}
	
	public WebElement getEditPOB (){
		return  editPOB ;
	}
	
	public WebElement getEditPrimaryContact (){
		return  editPrimaryContact ;
	}
	
	public WebElement getIndustries (){
		return  industries ;
	}
	
	public WebElement getMakePrimary (){
		return  makePrimary ;
	}
	
	public WebElement getNewsPublSpeaking (){
		return newsPublSpeaking;
	}
	
	public WebElement getPracticeAreas (){
		return practiceAreas;
	}
	
	public WebElement getWorkHistory (){
		return workHistory;
	}
	
	public WebElement getCaseOrgLink1(){
		return caseOrgLink1;
	}
	
	public WebElement getCasesAndMatters(){
		return casesAndMatters;
	}
	public WebElement getJVRTitle1(){
		return JVRTitle1;
	}
	
	public WebElement getJVRTitle2(){
		return JVRTitle2;
	}
	
	public WebElement getBioHeader() {
		return bioHeader;
	}

	public WebElement getProfileTitle() {
	return profileTitle;
	}
	
	public WebElement getStreamingSelection() {
		return streamingSelection;
		
	}
	
	public WebElement getStreamingContainer(){
		return streamingContainer;
	}
	@FindBy(how = How.ID, using = "judges-toc-jud-experience") 
	 private WebElement  judgeExpierience;

	 public WebElement getJudgeExpierience(){ return judgeExpierience;}

	@FindBy(how = How.ID, using = "judges-toc-awards") 
	 private WebElement  judgeAwards;

	 public WebElement getJudgeAwards(){ return judgeAwards;}

	@FindBy(how = How.ID, using = "judges-toc-publications-seminars") 
	 private WebElement  judgePubsAndSeminars;

	 public WebElement getJudgePubsAndSeminars(){ return judgePubsAndSeminars;}

	@FindBy(how = How.ID, using = "judges-toc-organizations") 
	 private WebElement  judgeOrganizations;

	 public WebElement getJudgeOrganizations(){ return judgeOrganizations;}

	@FindBy(how = How.ID, using = "judges-toc-family") 
	 private WebElement  judgeFamily;

	 public WebElement getJudgeFamily(){ return judgeFamily;}

	@FindBy(how = How.ID, using = "judges-toc-admissions") 
	 private WebElement  judgeAdmissions;

	 public WebElement getJudgeAdmissions(){ return judgeAdmissions;}

	@FindBy(how = How.ID, using = "judges-toc-civic") 
	 private WebElement  judgeCivic;

	 public WebElement getJudgeCivic(){ return judgeCivic;}

	@FindBy(how = How.ID, using = "judges-toc-additional-info") 
	 private WebElement  judgeAdditionalInfoc;

	 public WebElement getJudgeAdditionalInfoc(){ return judgeAdditionalInfoc;}

	@FindBy(how = How.ID, using = "additional-info") 
	 private WebElement  additionalInfo;

	 public WebElement getAdditionalInfo(){ return additionalInfo;}

	@FindBy(how = How.ID, using = "civic") 
	 private WebElement  civic;

	 public WebElement getCivic(){ return civic;}



	@FindBy(how = How.ID, using = "family") 
	 private WebElement  family;

	 public WebElement getFamily(){ return family;}

	@FindBy(how = How.XPATH, using = "(//div[1])[43]") 
	 private WebElement  organizations;

	 public WebElement getOrganizations(){ return organizations;}



	@FindBy(how = How.ID, using = "awards") 
	 private WebElement  awards;

	 public WebElement getAwards(){ return awards;}

	@FindBy(how = How.ID, using = "jud-experience") 
	 private WebElement  judExperience;

	 public WebElement getJudgeProfileExpierience(){ return judExperience;}

	@FindBy(how = How.ID, using = "subTabContent1") 
	 private WebElement  contentArea1;

	 public WebElement getContentArea1(){ return contentArea1;}

	@FindBy(how = How.CLASS_NAME, using = "profile-subtitle") 
	 private WebElement  profileSubtitle;

	 public WebElement getProfileSubtitle(){ return profileSubtitle;}

	@FindBy(how = How.ID, using = "summary-profile-container") 
	 private WebElement  summaryContainer;

	 public WebElement getSummaryContainer(){ return summaryContainer;}

	@FindBy(how = How.CLASS_NAME, using = "powered-by") 
	 private WebElement  poweredBy;

	 public WebElement getPoweredBy(){ return poweredBy;}

	@FindBy(how = How.CLASS_NAME, using = "section-title") 
	 private WebElement  sectionTitle;

	 public WebElement getSectionTitle(){ return sectionTitle;}

	@FindBy(how = How.XPATH, using = "(//span[1]") 
	 private WebElement  sectionJudge;

	 public WebElement getSectionJudge(){ return sectionJudge;}

	@FindBy(how = How.XPATH, using = "//span[3]/a") 
	 private WebElement  sectionLegal;

	 public WebElement getSectionLegal(){ return sectionLegal;}

	@FindBy(how = How.XPATH, using = "//span[5]/a") 
	 private WebElement  sectionProfessional;

	 public WebElement getSectionProfessional(){ return sectionProfessional;}

	@FindBy(how = How.CLASS_NAME, using = "judge-profile-content") 
	 private WebElement  judgeProfileContent;

	 public WebElement getJudgeProfileContent(){ return judgeProfileContent;}

	@FindBy(how = How.XPATH, using = "(//div[contains(@class, 'info-box-content')])[1]") 
	 private WebElement  dOBBox;

	 public WebElement getDOBBox(){ return dOBBox;}

	@FindBy(how = How.XPATH, using = "(//div[contains(@class, 'info-box-content')])[2]") 
	 private WebElement  pOBBox;

	 public WebElement getPOBBox(){ return pOBBox;}

	@FindBy(how = How.XPATH, using = "(//div[contains(@class, 'info-box-content')])[3]") 
	 private WebElement  familyBox;

	 public WebElement getFamilyBox(){ return familyBox;}

	@FindBy(how = How.ID, using = "switch-judge") 
	 private WebElement  switchJudge;

	 public WebElement getSwitchJudge(){ return switchJudge;}

	@FindBy(how = How.ID, using = "switch-attorney") 
	 private WebElement  switchAttorney;

	 public WebElement getSwitchAttorney(){ return switchAttorney;}

	@FindBy(how = How.ID, using = "pA-toc-contact-info-li") 
	 private WebElement  contactInfo;

	 public WebElement getContactInfo(){ return contactInfo;}

	@FindBy(how = How.XPATH, using = "//span[3]/a") 
	 private WebElement  education;

	 public WebElement getEducation(){ return education;}

	@FindBy(how = How.XPATH, using = "(//span[1])[10]") 
	 private WebElement  admissions;

	 public WebElement getAdmissions(){ return admissions;}

	@FindBy(how = How.XPATH, using = "(//span[1]/a)[2]") 
	 private WebElement  firstSchool;

	 public WebElement getFirstSchool(){ return firstSchool;}
	 
	 @FindBy(how = How.XPATH, using = "(//span/a)[1]") 
	 private WebElement  switchBetweenProfileSectionSubTabs;

	 public WebElement getSwitchBetweenProfileSectionSubTabs(){ return switchBetweenProfileSectionSubTabs;}

	@FindBy(how = How.XPATH, using = "(//span/a)[2]") 
	 private WebElement  affiliation1;

	 public WebElement getAffiliation1(){ return affiliation1;}

	 @FindBy(how = How.XPATH, using = "//p") 
	 private WebElement  noRecordFound;
	 
	 public WebElement getNoRecordFound(){ return noRecordFound;}
	 
	@FindBy(how = How.XPATH, using = "(//span/a)[3]") 
	 private WebElement  affiliation2;

	 public WebElement getAffiliation2(){ return affiliation2;}

	@FindBy(how = How.XPATH, using = "(//span/a)[4]") 
	 private WebElement  affiliation3;

	 public WebElement getAffiliation3(){ return affiliation3;}

	@FindBy(how = How.ID, using = "honors-pros-heading") 
	 private WebElement  professionalHonorsHeader;

	 public WebElement getProfessionalHonorsHeader(){ return professionalHonorsHeader;}

	@FindBy(how = How.ID, using = "honors-civs-heading") 
	 private WebElement  civicHonorsHeader;

	 public WebElement getCivicHonorsHeader(){ return civicHonorsHeader;}

		@FindBy(how = How.XPATH, using = "(//div[@class = 'pub-title'])[1]") 
		 private WebElement  chicagoDLB;

		 public WebElement getChicagoDLB(){ return chicagoDLB;}

		@FindBy(how = How.XPATH, using = "(//div[@class = 'pub-title'])[2]") 
		 private WebElement  chicagoLawyer;

		 public WebElement getChicagoLawyer(){ return chicagoLawyer;}

		@FindBy(how = How.XPATH, using = "(//div[@class = 'pub-title'])[3]") 
		 private WebElement  JVR;

		 public WebElement getJVR(){ return JVR;}


		@FindBy(how = How.XPATH, using = "(//div[@class = 'pub-title'])[4]") 
		 private WebElement  appelateSummary;

		public WebElement getAppelateSummary(){ return appelateSummary;}

	@FindBy(how = How.XPATH, using = "(//span[1]/a)[4]") 
	 private WebElement  thirdSchool;

	 public WebElement getThirdSchool(){ return thirdSchool;}

	@FindBy(how = How.XPATH, using = "(//span[1]/a)[3]") 
	 private WebElement  secondSchool;

	 public WebElement getSecondSchool(){ return secondSchool;}

	@FindBy(how = How.ID, using = "ardcLink") 
	 private WebElement  regulatoryStatusLink;

	 public WebElement getRegulatoryStatusLink(){ return regulatoryStatusLink;}

	@FindBy(how = How.XPATH, using = "//li[1]/div") 
	 private WebElement  firstStateAdmission;

	 public WebElement getFirstStateAdmission(){ return firstStateAdmission;}

	@FindBy(how = How.XPATH, using = "//li[2]/div") 
	 private WebElement  secondStateAdmission;

	 public WebElement getSecondStateAdmission(){ return secondStateAdmission;}

	@FindBy(how = How.XPATH, using = "//li[3]/div") 
	 private WebElement  thirdStateAdmission;

	 public WebElement getThirdStateAdmission(){ return thirdStateAdmission;}

	@FindBy(how = How.ID, using = "{groupId}") 
	 private WebElement  personHistoryGroup;

	 public WebElement getPersonHistoryGroup(){ return personHistoryGroup;}

	@FindBy(how = How.CLASS_NAME, using = "wh-title") 
	 private WebElement  personHistoryTitle;

	 public WebElement getPersonHistoryTitle(){ return personHistoryTitle;}

	@FindBy(how = How.CLASS_NAME, using = "profile-content-subtext wh-location") 
	 private WebElement  personHistoryLocation;

	 public WebElement getPersonHistoryLocation(){ return personHistoryLocation;}

	@FindBy(how = How.CLASS_NAME, using = "profile-content-subtext wh-date") 
	 private WebElement  personHistoryDates;

	 public WebElement getPersonHistoryDates(){ return personHistoryDates;}
	
	@FindBy(how = How.ID, using = "persons-admissions-education-educationItem-0") 
	 private WebElement  school1;

	 public WebElement getSchool1(){ return school1;}

	@FindBy(how = How.ID, using = "persons-admissions-education-educationItem-1") 
	 private WebElement  school2;

	 public WebElement getSchool2(){ return school2;} 

	@FindBy(how = How.ID, using = "ardcLink") 
	 private WebElement  checkILRegulatoryStatus;

	 public WebElement getCheckILRegulatoryStatus(){ return checkILRegulatoryStatus;}
	 

	 /**
	  * WebElements below this line are specific to Judge Tom Hogan and are used for Regression Automation
	  **/
	 @FindBy(how = How.XPATH, using = "//li[1]/div") 
	 private WebElement  hoganSpecificAdmissionsCheck;

	 public WebElement getHoganSpecificAdmissionsCheck(){ return hoganSpecificAdmissionsCheck;}
	 
	 @FindBy(how = How.XPATH, using = "(//a[3])[2]") 
	 private WebElement  hoganSpecificWorkHistoryCheck;

	 public WebElement getHoganSpecificWorkHistoryCheck(){ return hoganSpecificWorkHistoryCheck;}
}
