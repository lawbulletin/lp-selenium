package pageFactoryLibrary.pagefactorylibaray;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DirectoryResultFactory {
	
	@FindBy(how = How.XPATH, using = "//a[contains(@class, 'search-click-through')]")
	private WebElement firstResult;
	
	@FindBy(how = How.XPATH, using = "(//p [contains(@id, 'search-header-sub-title')])[1]")
	private WebElement resultCount;
	
	@FindBy(how = How.XPATH, using = "(//a)[18]")
	private WebElement firstResultOrganization;
	
	
	@FindBy( id="header-logo")
	private WebElement homeButton;
	
	@FindBy( id="filter-0-1")
	private WebElement govFilter;
	
	@FindBy( id="filter-0-0")
	private WebElement lawFirmFilter;
	
	@FindBy( id="filter-1-0")
	private WebElement cityFilter;
	
	@FindBy(how = How.CLASS_NAME, using = "reset-filters")
	private WebElement resetFilters;
	
	@FindBy(how = How.CLASS_NAME, using = "yui3-paginator-next")
	private WebElement next;
	
	@FindBy(how = How.CLASS_NAME, using = "yui3-paginator-last")
	private WebElement last;
	
	@FindBy(how = How.CLASS_NAME, using = "yui3-paginator-first")
	private WebElement first;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(@class, 'search-result-paging')])[1]")
	private WebElement page2;
	
	@FindBy(how = How.XPATH, using = "(//a [contains(@class,'search-click-through')])[2]")
	private WebElement secondResult;
	
	@FindBy(how = How.XPATH, using = "(//a)[5]")
	private WebElement resetSearch;

	public WebElement getSecondResult(){	
		return secondResult;}
	
	public WebElement getFirstResultOrg(){	
	return firstResultOrganization;
	}
	public WebElement getResetSearch(){
		return resetSearch;
	}
	
	public WebElement getCityFilter(){
		return cityFilter;
	}
	public WebElement getPage2Button(){
		return page2;
	}
	
	public WebElement getNextPage(){
		return next;
	}
	
	public WebElement getFirstPage(){
		return first;
	}
	
	public WebElement getLastPage(){
		return last;
	}
	public WebElement getResetFilter(){
		return resetFilters;
	}
	public Integer getIntResultCount(){
		String count = resultCount.getText().toString().trim();
	
		count = count.substring(9, 11).trim();
		Integer x = Integer.valueOf(count);
		return x;
	}
	
	public WebElement getLawFirmFilter(){
		return lawFirmFilter;
	}
	
	public WebElement getGovFilter() {
		return govFilter;
	}
	
	public WebElement getHome(){
		return homeButton;
	}
	
	public WebElement getResultCount(){
		return resultCount;
	}
	
	
//	private int resultCount(){
//		DirectoryResultFactory dResults =  PageFactory.initElements(getDriver(), DirectoryResultFactory.class);
//		
//		WebElement resultCount = dResults.getResultCount();
//		String count = resultCount.getText().toString();
//		String count2 = count.substring(12);
//		count = count2.trim();
//		int results = Integer.parseInt(count);
//		return results;
//	}
	
//	public int getResultNumber(){
//		return resultTotal();
//	}
//	
	public WebElement getFirstResult(){
		return firstResult;
	}
}
