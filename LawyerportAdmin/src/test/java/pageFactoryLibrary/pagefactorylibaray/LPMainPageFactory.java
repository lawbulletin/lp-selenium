package pageFactoryLibrary.pagefactorylibaray;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * 
 * @author hipatel
 * @author bgalatzer-levy
 *
 */
public class LPMainPageFactory  {

	@FindBy(id = "welcome-name")
	private WebElement welcomeName;
	
	@FindBy (how = How.ID, using = "header-logo")
	private WebElement headerLogo; 

	@FindBy (id = "button-welcome-link")
	private WebElement welcome;
	
	@FindBy (how = How.ID, using ="my-profile-edit")
	private WebElement editProfile;
	
	@FindBy (how = How.ID, using = "my-profile-logout")
	private WebElement logoutLink;
	
	@FindBy (how = How.ID, using = "navHome")
	private WebElement homeIcon;
	
	@FindBy (how = How.ID, using = "navDir")
	private WebElement directory;
	
	@FindBy (how = How.ID, using = "navNews")
	private WebElement news;
	
	@FindBy (how = How.ID, using = "navRes")
	private WebElement legalResearch;	

	@FindBy (how = How.LINK_TEXT, using = "Verdicts & Settlements")
	private WebElement verdictSettlemtn;
	
	@FindBy (how = How.LINK_TEXT, using = "Appellate Case Summaries")
	private WebElement appellateCaseSumm;
	
	@FindBy (id = "navRes")
	private WebElement courtDocket;
	
//	@FindBy (how = How.LINK_TEXT, using = "COURT RULES")
//	private WebElement courtRules;
		
	@FindBy (how = How.ID, using = "navEvents")
	private WebElement events;
	
	@FindBy (how = How.ID, using = "navVend")
	private WebElement legalMarketplace;
	
	@FindBy (how = How.ID, using = "searchType")
	private WebElement searchDropdown;
	
	@FindBy (how = How.CSS, using = "select[id='searchType'] option:nth-of-type(1)")
	private WebElement searchDropdownSelectionPeople;
	
	@FindBy (how = How.CSS, using = "select[id='searchType'] option:nth-of-type(2)")
	private WebElement searchDropdownSelectionOrganization;
	
//	@FindBy (how = How.CSS, using = "select[id='searchType'] option:nth-of-type(3)")
//	private WebElement searchDropdownSelectionNews;
	
	@FindBy (how = How.ID, using = "searchText")
	private WebElement searchTextbox;
	
	@FindBy (id = "searchEnterButton")
	private WebElement searchEnterButton;
	
	@FindBy(id ="my-profile-admin") 
	private WebElement myProfileAdmin;
	
	public WebElement getMyAdmin(){
		return myProfileAdmin;
	}

	public WebElement getEditCC(){
		return editCC;
	}

	@FindBy(id = "client-code-input")
	private WebElement ccInput;
	
	public WebElement getClientCodeInput(){
		return ccInput;
	}
	
	@FindBy(id="client-code-button")
		private WebElement editCC;
	
	@FindBy(how = How.XPATH, using = "//area")
	private WebElement quickTour;
	
	public WebElement getQuickTour(){
		return quickTour;
	}
	public WebElement getWelcomeName(){
		return welcomeName;
	}
	public void clickSearchButton(){
		searchEnterButton.click();
	}
	
	public WebElement getSearchButton(){
		return searchEnterButton;
	}
	public WebElement headerLogo() {
		return headerLogo;
	}
	
	public WebElement getWelcomeLink() {
		return welcome;
	
	}
	
	public WebElement getEditMyProfileLink(){
		return editProfile;
	}
	

	public WebElement getLogoutLink() {
		return logoutLink;
	}
	
	public WebElement getHomeIcon() {
		return homeIcon;
	}
	
	public WebElement getDirectoryTab(){
		return directory;
	}
	
//	public void directory() {
//		directory.click();
//	}
	
	public WebElement getNewsLink() {
		return news;
	}

	public WebElement getLegalResearchLink() {
		return legalResearch;
	}
	
	public WebElement getJvrLink() {
		return verdictSettlemtn;
	}
	
	public WebElement getAppCaseSummariesLink() {
		return appellateCaseSumm; 
	}
	
	public WebElement getCourtDocket() {
		return courtDocket;
	}
	
//	public WebElement courtRulesLink() {
//		return courtRules; 
//	}
	
	public WebElement getEvents() {
		return events;
	}
	
	public WebElement getLegalMarketplace() {
		return legalMarketplace;
	}
	
	public WebElement getSearchDropdown() {
		return searchDropdown;
	}
	
	public WebElement getSearchDropdownSelectionPeople() {
		return searchDropdownSelectionPeople;
	}
	
	public WebElement getSearchDropdownSelectionOrganization() {
		return searchDropdownSelectionOrganization;
	}
	
//	public WebElement searchDropdownSelectionNews() {
//		return searchDropdownSelectionNews;
//	}
	
	public WebElement getSearchTextbox() {
		return searchTextbox;
	}
	
}
