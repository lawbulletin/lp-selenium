package pageFactoryLibrary.pagefactorylibaray;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

//https://weblogs.java.net/blog/johnsmart/archive/2010/08/09/selenium-2web-driver-land-where-page-objects-are-king
//http://mestachs.wordpress.com/2012/08/13/selenium-best-practices/
public class DirectoryPageFactory{
	
	@FindBy(how = How.ID, using ="edit-profile")
	private WebElement editButton;
	
	@FindBy (how = How.XPATH, using = "//li[@id='navDir']")
	private WebElement getDirctory; 

	@FindBy(how = How.ID, using = "form-field-0")
	private WebElement firstName;
	
	@FindBy(how = How.ID, using = "form-field-1")
	private WebElement primaryOrg;
	
	@FindBy(how = How.ID, using = "form-field-2")
	private WebElement middleName;
	
	@FindBy(how = How.ID, using = "form-field-3")
	private WebElement peopleTitle;
	
	@FindBy(how = How.ID, using = "form-field-4")
	private WebElement lastName;
	
	@FindBy(how = How.ID, using = "form-field-5")
	private WebElement peopleOrgCity;
	
	@FindBy(id = "form-field-6")
	private WebElement FormerlyKnown;
	
	@FindBy(how = How.ID, using = "form-field-7")
	private WebElement orgDropDown;
	
	@FindBy(how = How.ID, using = "form-field-9")
	private WebElement selectState;
	
	@FindBy(how = How.ID, using = "form-field-10")
	private WebElement stateStartYear;
	
	@FindBy(how = How.ID, using = "form-field-11")
	private WebElement stateEndYear;
	
	@FindBy(how = How.ID, using = "dir-run-advanced-search")
	private WebElement search;
	
	@FindBy (how = How.ID, using = "btnPracticeArea")
	private WebElement practiceArea; 
	
	@FindBy(how = How.XPATH, using = "(//span[2])[2]")
	private WebElement firstNameLabel;

	
	@FindBy(id = "people")
	private WebElement peopleTab;
	
	@FindBy(id = "subtab-org")
	private WebElement orgButton;
	
	@FindBy( id="dir-reset-advanced-search-bottom")
	private WebElement resetButton;
	
	@FindBy(how = How.CLASS_NAME, using = "directory-search-label")
	private WebElement directoryCentral;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[3]")  
	private WebElement primaryOrganizationLabel;

	@FindBy(how = How.XPATH, using = "(//span[2])[4]")   
	private WebElement middleNameLabel;

	@FindBy(how = How.XPATH, using = "(//span[2])[5]")   
	private WebElement titleLabel;

	@FindBy(how = How.XPATH, using = "(//span[2])[6]")   
	private WebElement lastNameLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[7]")   
	private WebElement primaryOrganizationCityLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[8]")   
	private WebElement akaLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[9]")   
	private WebElement orgTypeLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[10]")   
	private WebElement stateLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[11]")   
	private WebElement startYearLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[12]")   
	private WebElement endYearLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[13]")   
	private WebElement organizationLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[14]")   
	private WebElement orgStartYearLabel;
	
	@FindBy(how = How.XPATH, using = "(//span[2])[15]")   
	private WebElement orgEndYearLabel;

	@FindBy(id = "court-admission-header-text")
	private WebElement courtAdmissions;
	
	@FindBy( id="state-admission-header-text")
	private WebElement stateAdmissions;
	
	@FindBy(id="language-header-text") 
	private WebElement languageLabel;
	@FindBy(id="form-field-20")
	private WebElement languageField;
	@FindBy( id="practice-area-header-text")
	private WebElement practiceAreaLabel;
	@FindBy( id="btnPracticeArea")
	private WebElement addButton;
	

	 
	public WebElement getPracticeAreaLabel(){ return practiceAreaLabel;}
	
	public WebElement getAddPracticeAreaBtn(){return addButton;}
	
	public WebElement getLanguageField(){return languageField;}	
	public WebElement getLanguageLabel(){return languageLabel;}	
	public WebElement getCtAdmissions(){return courtAdmissions;}
	public WebElement getStAdmissions(){return stateAdmissions;}
	
	
	public WebElement getFirstNameLabel() { return firstNameLabel;}
	public WebElement getPrimaryOrganizationLabel() { return primaryOrganizationLabel;}
	public WebElement getMiddleNameLabel() { return middleNameLabel;}
	public WebElement getTitleLabel() { return titleLabel;}
	public WebElement getLastNameLabel() { return lastNameLabel;}
	public WebElement getPrimaryOrganizationCityLabel() { return primaryOrganizationCityLabel;}
	public WebElement getAkaLabel() { return akaLabel;}
	public WebElement getOrgTypeLabel() { return orgTypeLabel;}
	public WebElement getStateLabel() { return stateLabel;}
	public WebElement getStartYearLabel() { return startYearLabel;}
	public WebElement getEndYearLabel() { return endYearLabel;}
	public WebElement getOrganizationLabel() { return organizationLabel;}
	public WebElement getOrgStartYearLabel() { return orgStartYearLabel;}
	public WebElement getOrgEndYearLabel() { return orgEndYearLabel;}

	
	public WebElement getOrgType(){
		return orgDropDown;
	}
	
	public WebElement getDirectoryCentral(){
		return directoryCentral;
	}
	
	public WebElement getReset(){
		return resetButton;
	}
	
	public WebElement getPeopleTab() {
		return peopleTab;
	}
	
	public WebElement getOrgButton() {
		return orgButton;
	}
	
	public WebElement getEditButton() {
		return editButton;
	}
	
	public WebElement navDIR() {
		return getDirctory;
	}
	
	public WebElement getFirstName() {
		return firstName;
	}
	public WebElement getPrimaryOrg() {
		return primaryOrg; 
	}
	public WebElement getMiddleName() {
		return middleName; 
	}
	public WebElement getLastName() {
		return lastName;
	}
	public WebElement getTitle() {
		return peopleTitle;
	}
	
	public WebElement getPeopleOrgCity() {
		return peopleOrgCity; 
	}
	public WebElement getFormelyknownAs() {
		return FormerlyKnown;
	}

	public Select selectStateAdmission(){
		return new Select(selectState);
	}

	public Select selectStateStartYear(){
		return new Select(stateStartYear);
	}
	
	public Select selectStateEndYear(){
		return new Select(stateEndYear);
	}
	
	public WebElement getSearchButton() {

		return search;		
	}
}