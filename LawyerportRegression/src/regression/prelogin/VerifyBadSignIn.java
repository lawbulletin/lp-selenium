package regression.prelogin;

import java.util.ArrayList;
import java.util.List;

import lawyerportdominion.AbstractTest;
import lawyerportdominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pagefactories.LoginPageFactory;
import seleniumwebdriver.SeleniumDriver;

public class VerifyBadSignIn extends AbstractTest{

	LoginPageFactory log = PFCaller.loginFactory();

	@Test
	public void verifyValidUserInvalidPassword(){
		System.out.println("Verify Valid User, Invalid Password Credentials Error Message");
		log.getUserName().sendKeys("researchtest@lawyerport.com");
		log.getPassword().sendKeys("xyzq");
		log.getClient().sendKeys("client");
		forceWait(2);
		SeleniumDriver.getDriver().findElement(By.id("submitButton")).click();
		forceWait(3/2);

		List<WebElement> errorMessages = SeleniumDriver.getDriver().findElements(By.xpath("//*[@id='msg']"));
		
		List<String> messages = new ArrayList<String>();
		
		for(WebElement m : errorMessages){
			String msg = m.getText().toString();
			messages.add(msg);
		}

		for(String s : messages){
			if(s.contains("credentials")){System.out.println("Expected message: " + s + "   - Pass");}
			if(!s.contains("credentials")){System.err.println("Fail");}
		}
		System.out.println("------------------");
	}
	
	@Test
	public void verifyInvalidUserSomePassword(){
		System.out.println("Verify Invalid User Credentials Error Message");
		log.getUserName().sendKeys("HijuanEscotel@SqaCoedSquashTeam.net");
		log.getPassword().sendKeys("test");
		log.getClient().sendKeys("client");
		log.getSubmitButton().click();
		
		e = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains (@id, 'msg')]"));
		
		String msg = e.getText().toString();
		
			if(!msg.contains("credentials")){
				System.err.println("Fail");
			}
			else{System.out.println("Expected message: " + msg + "   - Pass");}
		
		System.out.println("------------------");
	}
	
}


