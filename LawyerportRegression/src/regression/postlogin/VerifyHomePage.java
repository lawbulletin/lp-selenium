package regression.postlogin;
//done
import java.util.List;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pagefactories.LPMainPageFactory;
import pagefactories.LandingPageFactory;
import pagefactories.LoginPageFactory;

public class VerifyHomePage extends AbstractSprint{
	LoginPageFactory log = PFCaller.loginFactory();
	LandingPageFactory land = PFCaller.landingFactory();
	LPMainPageFactory home = PFCaller.lpMainFactory();
	
	
	@Test
	public void verifyHomeLinks() throws Exception{
		int j = 0;
		Boolean[] links = {home.getHomeIcon().isEnabled(), home.getDirectoryTab().isEnabled(),  
		home.getNewsLink().isEnabled(), home.getLegalResearchLink().isEnabled(), home.getEvents().isEnabled(), 
		home.getLegalMarketplace().isEnabled(), home.headerLogo().isEnabled(), home.getSearchDropdown().isEnabled(), 
		home.getSearchTextbox().isEnabled(), home.getWelcomeLink().isEnabled(), home.getWelcomeName().isEnabled()};
		
		String[] elements = new String[11];
		elements[0] = "Home Link";
		elements[1] = "Directory Link";
		elements[2] = "News Link";
		elements[3]= "Research Link";
		elements[4]= "Events Link";
		elements[5] = "Legal Market Link";
		elements[6] = "Lawyerport Logo";
		elements[7] = "Quick Search Drop-Down";
		elements[8] = "Quick Search Input Field";
		elements[9] = "Welcome Link";
		elements[10] = "User Name: " + home.getWelcomeName().getText().toString();
		
		
		for(Boolean b : links)
			if(b){
				System.out.println(elements[j]+ " - Pass");
				j=j+1;
			}
			else{System.err.println(elements[j]+" - Fail");
				j= j+1;
			}
//			for(j=0;j<11;j++){
//				System.out.println(links[j]);//+"   "+ elements[j], 34);
//			}
			//else{System.err.println("Fail - Element Not Preseent: "+elements[j]);
	
	}

	@Test
	public void verifyUserDropDown() throws Exception{
		System.out.println("Verify Non-Admin DropDown Menu Options");

		WebElement Select = home.getWelcomeLink();
		List<WebElement> options = Select.findElements(By.partialLinkText("o"));
		
		for(WebElement option : options){
			System.out.println(option);
			if(option.getText().toString().contains("Edit")){
				condition1 = true;
			}
			else if(option.getText().toString().contains("Log"))
				condition2=true;
		}
		
		tfTest3();

		}
	
	
}
