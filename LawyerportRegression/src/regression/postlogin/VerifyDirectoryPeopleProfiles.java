package regression.postlogin;

import java.util.HashMap;
import java.util.Map;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pagefactories.LPMainPageFactory;
import pagefactories.ProfilePageFactory;
import seleniumwebdriver.SeleniumDriver;

public class VerifyDirectoryPeopleProfiles extends AbstractSprint{

	LPMainPageFactory main = PFCaller.lpMainFactory();
	ProfilePageFactory profile = PFCaller.profileFactory();
	
	@Before
	public void reset(){
	  
		 SeleniumDriver.getDriver().get("https://beta1.lawyerport.com/judges#/1009805");
	    forceWait(1);
	    System.out.println("------------------------------");
	    forceWait(2);
	    }
	    
	@After
	public void finish(){
	    	main.getHomeIcon().click();
	    	forceWait(1);
	    	deleteCookies();
	    	System.out.println("Test Complete");
	    }
	    
	@Test
	public void test1() throws Exception{
	System.out.println("Verify Judge Profile Information");
		
		Map<String, Boolean> info = new HashMap<String, Boolean>();
    	condition1=true;
		
		String name = getString(profile.getProfileTitle());
		String title = getString(profile.getProfileSubtitle());
		String address = getString(profile.getAddress());
		String organization = getString(profile.getOrganizations());
		String phone = getString(profile.getTelephone());
		String judicialExp = getString(profile.getContentArea1());
		
		profile.getSectionLegal().click();
		forceWait(2);
		String legalExp = getString(profile.getJudgeProfileContent());
		
		profile.getSectionProfessional().click();
		forceWait(2);
		String proExp = getString(profile.getContentArea1());
	
		profile.getJudgePubsAndSeminars().click();
		forceWait(2);
		String pubs = getString(profile.getJudgeProfileContent());
		
		profile.getAwards().click();
		forceWait(2);
		String awards = getString(profile.getJudgeProfileContent());
		
		profile.getJudgeOrganizations().click();
		forceWait(2);
		String orgs = getString(profile.getJudgeProfileContent());
		
		profile.getJudgeFamily().click();
		forceWait(1);
		String dob = getString(profile.getDOBBox());
		String pob = getString(profile.getPOBBox());
		String family = getString(profile.getFamilyBox());
		
		profile.getJudgeAdmissionsAndEd().click();
		forceWait(1);
		String admissions = getString(profile.getJudgeProfileContent());
		
		profile.getEducation().click();
		forceWait(1);
		String education = getString(profile.getJudgeProfileContent());
		
		profile.getCivic().click();
		forceWait(1);
		String civic = getString(profile.getJudgeProfileContent());
		
		profile.getAdditionalInfo().click();
		forceWait(1);
		String additionalInfo = getString(profile.getJudgeProfileContent());
		
		String cdlb = getString(profile.getChicagoDLB());
		String chiLawyer = getString(profile.getChicagoLawyer());
		String jvr = getString(profile.getJVR());
		String appellateSummaries = getString(profile.getAppelateSummary());

		
	stringToMap(info, name, "Hon. Thomas L. Hogan");
	stringToMap(info, title ,"Judge" );
	stringToMap(info, organization, "Circuit Court of Cook County > COUNTY DEPARTMENT Law Division > Jury Section");
	stringToMap(info, address, "Daley Ctr, Suite 1906 | Chicago, IL 60602");
	stringToMap(info, phone, "312-603-5935 p");
	stringToMap(info, judicialExp, "In August 1997, Hogan was appointed a judge in the Circuit Court of Cook County, filling the vacancy created by the retirement of Judge Gino DiVito. Hogan was elected to a full six-year term of office in November 1998, and was retained for additional terms in 2004 and 2010. He initially served in the 1st Municipal District Traffic Center then moved to the 1st Municipal District�s civil section. In January of 2002, he was transferred to the Law Division, where he presided over a trial call. Hogan retired from judicial service effective December 31, 2014.");
	stringToMap(info, legalExp, "Hogan has been a staff attorney for the Chicago Transit Authority (1980-81); an associate at Walsh, Case, Coale, Brown & Burke (1981-82), James J. Riedy, Ltd. (1982-83), Cornelius P. Callahan & Associates (1986-90), and Gorham, Metge, Bowman & Hourigan (Chicago) (1990-92); a partner (with his brother Edward M. Hogan) at Hogan & Hogan (1983-85, 1990) and Connelly & Schroeder (Chicago) (1996-97); and managing attorney at Canadian Pacific Legal Services (Chicago) (1992-96).");
	stringToMap(info, proExp, "No Information At This Time");
	stringToMap(info, pubs, "Speaker, �Jury Service,� IJA Judicial Perspective cable television program, 2005; panelist, �Work-Life Balance,� Navigating the Moral Compass: How Work-Life Balance Affects Ethical Behavior and Real-World Strategies for Acting Ethically, CBA seminar, 2007; speaker, �Navigating through Law Division and 2005,� CBA Civil Practice Section committee meeting, 2007; speaker, �Motions for Pretrial, Petitions to Settle Minor Causes or Wrongful Death Claims, Motions to Adjudicate Liens, Post Trial Motions,� Motion Practice in Cook County Circuit Court�s Law Division, CBA, 2008; panelist, �What Civil Court Judges Want You to Know,� National Business Institute Trial Practice Judicial Forum, 2008, 2010; speaker, Litigation Skills II Conference: Trial, Law Bulletin seminar, 2008; speaker, �On the Coverage Campaign Trail,� CBA and YLS Insurance Law Committees, 2008; speaker, �Recent Topics/Issues on Damages in Tort Cases,� CBA Tort Litigation Committee meeting, 2008; speaker, Judicial Perspective on Medical Malpractice Trials, ITLA, 2008; speaker, �Motion Practice in Cook County Circuit Court Law Division,� CBA seminar, 2009-10; participant, CBA/IJA Eight O'Clock Call, 2009-10; speaker, Litigation Skills I Conference, Law Bulletin seminar, 2009; speaker, Litigation Skills III Conference, Law Bulletin seminar, 2009; speaker, �Case Evaluation: Managing the High Verdict Potential Case,� Law Bulletin seminar, 2009; panelist, �The Value of Mock Trials, Deliberations & Focus Groups,� Juror Deliberation & Jury Management Conference, Law Bulletin Seminars, 2011; panelist, �Civil Court Judicial Forum: Advanced Discovery and Trial Practice,� National Business Institute, 2011; speaker, �Motions In Limine, Motions for Directed Verdict and Post Trial Motions,� Motion Practice in Cook County Circuit Court Law Division, CBA seminar, 2012; panelist, �As Judges See It: Top Mistakes Attorneys Make in Civil Litigation,� National Business Institute Judicial Forum, 2012; moderator, CBA Eight O�Clock Call, November and December 2012; speaker, �Motions In Limine, Motions for Directed Verdict and Post Trial Motions,� Motion Practice in Cook County Circuit Court�s Law Division, CBA CLE seminar, 2013; moderator, �Pro Se Litigants �� A How To for Attorneys,� CBA/IJA Eight O�Clock Call, 2013; participant, Probate, CBA/IJA Eight O'Clock Call, 2013; moderator, Law Divisions of Illinois Circuit Courts, CBA/IJA Eight O�Clock Call, 2013; speaker, �Motions In Limine, Motions for Directed Verdict and Post Trial Motions,� CBA CLE seminar, 2014; speaker, �View from the Bench,� ISBA Law Ed CLE seminar on medical malpractice, 2014; moderator, Circuit Court of Cook County Criminal Court, CBA/IJA Eight O�Clock Call, 2014; panelist, �As Judges See It: Top Mistakes Attorneys Make in Civil Litigation,� National Business Institute judicial forum and seminar, 2014; moderator, Eight O�Clock Call, Chicago Bar Association, 2014; moderator, Circuit Court of Cook County Commercial Calendar Section of the Law Division, CBA/IJA Eight O�Clock Call, 2014.");
	stringToMap(info, awards, "2012, Vanguard Award, Chicago Bar Association; 2012, Judge of the Year, ABOTA Illinois Chapter; 2013, Award for Excellence in the Field of Law, Saint Ignatius Law Society; 2013, Celtic Man of the Year, Celtic Legal Society of Chicago.");
	stringToMap(info, orgs, "Chicago Bar Association: Circuit Court Committee (chair 2004-05), Board of Managers (2007-09), Bench & Trial Bar Committee, Trial Advocacy Seminar, Interfaith Committee; State Bar of Wisconsin; Illinois Judges Association.");
	stringToMap(info, dob, "July 27, 1954");
	stringToMap(info, pob, "Michigan City, IN");
	stringToMap(info, family, "He is married and has two sons. He resides in Chicago's Beverly neighborhood.");
	stringToMap(info, admissions, "1980 IL,1994 WI");
	stringToMap(info, education, "High School, St. Ignatius College Prep, Chicago, 1972");
	stringToMap(info, civic, "Beverly Area Planning Association: board member (1993-00); University of Notre Dame Board of Alumni Association: regional director (term expired in 2001); St. Cajetan Parish Council; St. Ignatius Alumni Association.");
	stringToMap(info, additionalInfo, "Hobbies: reading historical novels and mysteries and playing tennis.");
	stringToMap(info, cdlb, "CHICAGO DAILY LAW BULLETIN (86)");
	stringToMap(info, chiLawyer, "CHICAGO LAWYER (6)");
	stringToMap(info, jvr, "JURY VERDICT REPORTER (273)");
	stringToMap(info, appellateSummaries, "APPELLATE SUMMARY (28)");
	
    	for(Map.Entry<String, Boolean> entry : info.entrySet()){
    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
    		if(!entry.getValue()){System.err.println("Missing Label Text: " + entry.getKey());
    			condition1 = false;
    		}
    	tfTest();
    	}
	}
	
	@Test
	public void test2() throws Exception{
		System.out.println("Verify Attorney Profile Information");
		
		Map<String, Boolean> info = new HashMap<String, Boolean>();
    	condition1=true;
    	
		profile.getSwitchAttorney().click();
		forceWait(2);
		
		String name = getString(profile.getProfileTitle());
		String title = getString(profile.getProfileSubtitle());
		String address = getString(profile.getAddress());
		String organization = getString(profile.getOrganizations());
		String phone = getString(profile.getTelephone());
		
		profile.getAffiliationsAndHonors().click();
		forceWait(1);
		String proTitle = getString(profile.getProfessionalHonorsHeader());
		String honor1 = getString(profile.getAffiliation1());
		String civic = getString(profile.getCivicHonorsHeader());
		String honors = getString(profile.getSwitchBetweenProfileSectionSubTabs());
		
		profile.getSwitchBetweenProfileSectionSubTabs().click();
		forceWait(1);
		String noHonor = getString(profile.getNoRecordFound());
		
		profile.getWorkHistory().click();
		forceWait(1);
		String jSection = getString(profile.getHoganSpecificWorkHistoryCheck());
		
		profile.getAdmissionsAndEd().click();
		forceWait(1);
		String admissions = getString(profile.getAdmissions());
		String education = getString(profile.getEducation());
		String wisconsin = getString(profile.getHoganSpecificAdmissionsCheck());
		
		profile.getEducation().click();
		forceWait(1);
		String kent = getString(profile.getSchool1());
		
		stringToMap(info, name, "Hon. Thomas L. Hogan");
		stringToMap(info, title, "Judge");
		stringToMap(info, organization, "Circuit Court of Cook County > COUNTY DEPARTMENT Law Division > Jury Section");
		stringToMap(info, address, "Daley Ctr, Suite 1906 | Chicago, IL 60602");
		stringToMap(info, phone, "312-603-5935 p");
		stringToMap(info, kent, "Chicago-Kent College of Law, Illinois Institute of Technology, J.D. (1980)");
		stringToMap(info, education, "EDUCATION");
		stringToMap(info, admissions, "ADMISSIONS");
		stringToMap(info, jSection, "Jury Section");
		stringToMap(info, noHonor, "No records found");
		stringToMap(info, proTitle, "Professional");
		stringToMap(info, honor1, "Illinois State Bar Association");
		stringToMap(info, honors, "HONORS");
		stringToMap(info, civic, "Civic");
		stringToMap(info, wisconsin, "Wisconsin (1993)");

		for(Map.Entry<String, Boolean> entry : info.entrySet()){
    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
    		if(!entry.getValue()){System.err.println("Missing Label Text: " + entry.getKey());
    			condition1 = false;
    		}
    	tfTest();
    	}
	}
	/**
	 * Need Admin login to run this test
	 */
//	@Test
//	public void test3() throws Exception{
//		System.out.println("Verify Person Profile Edit Page");
//		
//		Map<String, Boolean> info = new HashMap<String, Boolean>();
//    	condition1=true;
//    	profile.getSwitchAttorney().click();
//    	forceWait(3);
//		profile.getEditButton().click();
//		forceWait(2);
//		String practiceAreas = getString(profile.getPracticeAreas());
//		System.out.println(practiceAreas);
//		String cdlb = getString(profile.getChicagoDLB());
//		System.out.println(cdlb);
//		String chiLawyer = getString(profile.getChicagoLawyer());
//		System.out.println(chiLawyer);
//		String jvr = getString(profile.getJVR());
//		System.out.println(jvr);
//		
//		stringToMap(info, practiceAreas, "");
//			stringToMap(info, cdlb, "CHICAGO DAILY LAW BULLETIN (86)");
//	stringToMap(info, chiLawyer, "CHICAGO LAWYER (6)");
//	stringToMap(info, jvr, "JURY VERDICT REPORTER (273)");
//
//		for(Map.Entry<String, Boolean> entry : info.entrySet()){
//    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
//    		if(!entry.getValue()){System.err.println("Missing Label Text: " + entry.getKey());
//    			condition1 = false;
//    		}
//		}
//    	tfTest();
//    	}
//	
}
