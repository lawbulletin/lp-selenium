package regression.postlogin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pagefactories.DirectoryPageFactory;
import pagefactories.DirectoryResultFactory;
import pagefactories.LPMainPageFactory;

public class VerifyDirectoryLandingPage extends AbstractSprint{

	DirectoryPageFactory dir = PFCaller.directoryFactory();
    DirectoryResultFactory res =  PFCaller.dirResultsFactory();
    LPMainPageFactory main = PFCaller.lpMainFactory();
    
    
    @Before
    public void reset(){
    main.getDirectoryTab().click();
    System.out.println("------------------------------");
    forceWait(2);
    }
    
    @After
    public void finish(){
    	main.getHomeIcon().click();
    	deleteCookies();
    	System.out.println("Test Complete");
    }
    
    @Test
    public void test1() throws Exception{
    	List<String> labels = new ArrayList<String>();
    	List<String> anticipatedLabels = new ArrayList<String>();
    	
    	System.out.println("Verify Labels");
    	String dc = getString(dir.getDirectoryCentral());
    	labels.add(dc);
    	String people = getString(dir.getPeopleTab());
    	labels.add(people);
    	String firstName = getString(dir.getFirstNameLabel());
    	labels.add(firstName);
    	String middleName = getString(dir.getMiddleNameLabel());
    	labels.add(middleName);
    	String lastName = getString(dir.getLastNameLabel());
    	labels.add(lastName);
    	String title = getString(dir.getTitleLabel());
    	labels.add(title);
    	String aka = getString(dir.getAkaLabel());
    	labels.add(aka);
    	String primaryOrg = getString(dir.getOrganizationLabel());
    	labels.add(primaryOrg);
    	String orgCity = getString(dir.getPrimaryOrganizationCityLabel());
    	labels.add(orgCity);
    	String orgType = getString(dir.getOrgTypeLabel());
    	labels.add(orgType);
    	String state = getString(dir.getStateLabel());
    	labels.add(state);
    	String st_start_yr = getString(dir.getOrgStartYearLabel());
    	labels.add(st_start_yr);
    	String st_end_yr = getString(dir.getOrgEndYearLabel());
    	labels.add(st_end_yr);
    	String ctAdmissions = getString(dir.getCtAdmissions());
    	labels.add(ctAdmissions);
    	String stAdmissions = getString(dir.getStAdmissions());
    	labels.add(stAdmissions);
    	String language = getString(dir.getLanguageLabel());
    	labels.add(language);
    	String practiceArea = getString(dir.getPracticeAreaLabel());
    	labels.add(practiceArea);
    	String orgTab = getString(dir.getOrgButton());
    	labels.add(orgTab);
    	
    	anticipatedLabels.add("DIRECTORY CENTRAL");
    	anticipatedLabels.add("PEOPLE");
    	anticipatedLabels.add("FIRST NAME");
    	anticipatedLabels.add("MIDDLE NAME");
    	anticipatedLabels.add("LAST NAME");
    	anticipatedLabels.add("TITLE");
    	anticipatedLabels.add("FORMERLY KNOWN AS");
    	anticipatedLabels.add("ORGANIZATION");
    	anticipatedLabels.add("PRIMARY ORGANIZATION CITY");
    	anticipatedLabels.add("ORGANIZATION TYPE");
    	anticipatedLabels.add("STATE");
    	anticipatedLabels.add("START YEAR"); 
    	anticipatedLabels.add("END YEAR"); 
    	anticipatedLabels.add("COURT AND OTHER ADMISSIONS"); 
    	anticipatedLabels.add("STATE ADMISSIONS");
    	anticipatedLabels.add("LANGUAGE");
    	anticipatedLabels.add("PRACTICE AREASADD");
    	anticipatedLabels.add("ORGANIZATION");
    	
    	List<String> sourceList = new ArrayList<String>(labels);
        List<String> destinationList = new ArrayList<String>(anticipatedLabels);
        
        sourceList.removeAll(anticipatedLabels);
        destinationList.removeAll(labels);
        
        condition1 = sourceList.size() == 0;
        condition2 = destinationList.size() == 0;
       System.out.println("Unexpected Field Labels: "+sourceList.size()); 
       System.out.println("Missing Field Labels: "+destinationList.size());
        
//       if(!condition1){
//        	System.err.println("Unexpected Field Labels: ");
//        	System.err.println(sourceList);
//        }
//        
//        if(!condition2){
//        	System.err.println("Missing Field Labels: ");
//        	System.err.println(destinationList);
//        }
        
       tfTest3();
        
    	}
    
    
    	@Test
    	public void test2() throws Exception, Throwable{
    		System.out.println("Verify Fields");
    		Map<String, Boolean> fields = new HashMap<String, Boolean>();
        	condition1=true;
        	
        	Boolean firstName = isWebElementPresent(dir.getFirstName());
        	fields.put("firstName", firstName);
        	Boolean middleName = isWebElementPresent(dir.getMiddleName());
        	fields.put("middleName", middleName);
        	Boolean lastName = isWebElementPresent(dir.getLastName());
        	fields.put("lastName",lastName);
        	Boolean title = isWebElementPresent(dir.getTitle());
        	fields.put("title",title);
        	Boolean formerlyKnownAs = isWebElementPresent(dir.getFormelyknownAs());
        	fields.put("formerlyKnownAs",formerlyKnownAs);
        	Boolean primaryOrg = isWebElementPresent(dir.getPrimaryOrg());
        	fields.put("primaryOrg",primaryOrg);
        	Boolean orgCity = isWebElementPresent(dir.getPeopleOrgCity());
        	fields.put("orgCity",orgCity);
        	Boolean orgType = isWebElementPresent(dir.getOrgType());
        	fields.put("orgType", orgType);
        	Boolean resetSearch = isWebElementPresent(dir.getReset());
        	fields.put("Reset Button", resetSearch);
        	Boolean searchButton = isWebElementPresent(dir.getSearchButton());
        	fields.put("Search Button", searchButton);
        
        	for(Map.Entry<String, Boolean> entry : fields.entrySet()){
        		if(!entry.getValue()){System.err.println("Missing Field: " + entry.getKey());
        			condition1 = false;
        		
        		}
        	}
        	tfTest();
    		}	
    	
    	@Test
    	public void test3() throws Exception{
    		System.out.println("Verify Directory Advanced Search");
    		
    		dir.getFirstName().sendKeys("Peter");
    		dir.getLastName().sendKeys("Mierzwa");
    		forceWait(2);
    		dir.getSearchButton().click();
    		
    		forceWait(2);
    		
    		String i= getString(res.getResultCount());
    		System.out.println(i);
    		String name = getString(res.getFirstResult());
    		System.out.println(name);
    		String org = getString(res.getFirstResultOrg());
    		System.out.println(org);
    		
    		condition1 = i.contains("1-1 of 1 People");
    		condition2 = name.contains("Peter Mierzwa");
    		condition3 = org.contains("Law Bulletin Publishing Company");
    		
    		tfTest4();
    		
    	}
    	
    	@Test
    	public void test4() throws Exception{
    		System.out.println("Verify Directory Advanced Search");
    		
    		dir.getFirstName().sendKeys("Tom");
    		dir.getLastName().sendKeys("Hogan");
    		forceWait(2);
    		dir.getSearchButton().click();
    		
    		forceWait(2);
    		
    		String i= getString(res.getResultCount());
    		System.out.println(i);
    		String name = getString(res.getSecondResult());
    		System.out.println(name);
    		
    		
    		condition1 = i.contains("1-3 of 3 People");
    		condition2 = name.contains("Hon. Thomas L. Hogan");
    		
    		
    		tfTest3();
    		
    	}
    	
    	
    }
    
    
    
    

