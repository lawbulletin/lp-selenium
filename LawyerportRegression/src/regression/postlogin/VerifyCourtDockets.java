package regression.postlogin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;

import pagefactories.CDPageFactory;
import pagefactories.CourtDocketResultFactory;
import pagefactories.LPMainPageFactory;

public class VerifyCourtDockets extends AbstractSprint{

	@Test
	public void test1() throws Exception {
		System.out.println("Verify Court Docket Search Funtionality");
		
		CDPageFactory cd = PFCaller.cdFactory();
		LPMainPageFactory main = PFCaller.lpMainFactory();
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String day = dateFormat.format(date).toString();
		
		main.getLegalResearchLink().click();
		forceWait(1);
		cd.getSearchReset().click();
		forceWait(2);

		 Random t = new Random();
	
		 int randomYear = t.nextInt(9);
			waitForElement(By.id("court-dockets-advanced-search-form-node")); 
			System.out.println("\nSearching by Case Number");
			waitForElement(By.id("form-field-0")).sendKeys("200"+randomYear+"L"); // Case number 
			System.out.println("Random Case Numer: "+"200"+randomYear+"L");
			cd.getDocketSearchButton().click();	
			forceWait(3);
		
			cdSearchResults();
			
			cd.getSearchReset().click();
			System.out.println("\nSearching by Random Plantiff/Defendent");
			waitForElement(By.id("form-field-5")).sendKeys(randomLastName()); // Plaintiff
			waitForElement(By.id("court-dockets-run-advanced-search")).click();
			forceWait(3);
			
			cdSearchResults();
			
			//main.getLegalResearchLink().click();
			cd.getSearchReset().click();
			System.out.println("\nSearching by Random Plantiff, Defendent, and Bounded Dates");
			String divorcees = randomLastName();
			forceWait(2);
			
			
			waitForElement(By.id("form-field-2")).sendKeys(divorcees);
			waitForElement(By.id("form-field-3")).sendKeys(divorcees);
			waitForElement(By.id("form-field-7")).sendKeys("04/22/2010");
			waitForElement(By.id("form-field-8")).clear();
			forceWait(1);
			waitForElement(By.id("form-field-8")).sendKeys(day);
			waitForElement(By.id("court-dockets-run-advanced-search")).click();
			
			cdSearchResults();
			

			main.getLegalResearchLink().click();
			forceWait(2);
			cd.getSearchReset().click();
			forceWait(2);
			System.out.println("\nPlantiff, Defendent, and Bounded Dates search passed");

			System.out.println("Searching by Judge");
			waitForElement(By.xpath("//li[contains(@id, 'yui_3')]/input")).sendKeys("Jones");
			System.out.println("Judge: JONES III, SIDNEY A.");
			waitForElement(By.xpath("//B")).click();
			waitForElement(By.id("court-dockets-run-advanced-search")).click();
			
			cdSearchResults();
			
			main.getLegalResearchLink().click();
			forceWait(2);
			waitForElement(By.id("court-dockets-reset-advanced-search-bottom")).click();
			forceWait(2);
			System.out.println("\nSearching by Lawyer/Lawfirm");
			waitForElement(By.xpath("(//input)[8]")).sendKeys("Jenner");
			waitForElement(By.xpath("(//B)[3]")).click();
			System.out.println("Firm: Jenner & Block");
			waitForElement(By.id("court-dockets-run-advanced-search")).click();
			cdSearchResults();
			
			main.getLegalResearchLink().click();
	}
	
	
	@Test
	public void test2() throws Exception{
		System.out.println("Verify Court Docket Search Accurracy No#2");
		
		
		CDPageFactory cd = PFCaller.cdFactory();
		LPMainPageFactory main = PFCaller.lpMainFactory();
		
		main.getLegalResearchLink().click();
		forceWait(2);
		cd.getPlaintiff().sendKeys("Filbert");
		
		cd.getDocketSearchButton().click();
		forceWait(3);
		int result = AbstractSprint.cdSearchResult();
		
		condition1 = result == 22;
		tfTest();
	}
	@Test
	public void test3() throws Exception{
		System.out.println("Verify Court Docket Search Accurracy No#2");
		
		CourtDocketResultFactory cdResults = PFCaller.cdResults();
		CDPageFactory cd = PFCaller.cdFactory();
		LPMainPageFactory main = PFCaller.lpMainFactory();
		
		main.getLegalResearchLink().click();
		forceWait(2);
		cd.getCaseNumber().sendKeys("2012L-1556");
		cd.getIlCookLaw().click();
		cd.getDocketSearchButton().click();
		forceWait(3);
		String result = cdResults.getCDFirstResult().getText().toString();
		System.out.println("Anticipated Reresult: 2012L-1556");
		System.out.println("Actual Result: "+ result);
		condition1 = result.contains("2012L-1556");
		//tfTest();
	}
	
	
	
}
