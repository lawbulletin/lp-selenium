package regression.postlogin;
//works

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;
import lawyerportdominion.RoboTester;

import org.junit.Test;
import org.openqa.selenium.WebElement;

import pagefactories.AppellateSummariesPageFactory;
import pagefactories.LPMainPageFactory;
import pagefactories.LegalResearchPageFactory;
import pagefactories.SearchResultsFactory;


public class VerifyAppellateSummaries extends AbstractSprint{
	AppellateSummariesPageFactory apps = PFCaller.appSummaryFactory();
	LPMainPageFactory main = PFCaller.lpMainFactory();
	LegalResearchPageFactory legal = PFCaller.lrFactory();
	SearchResultsFactory results = PFCaller.searchResultFactoy();
	
	@Test
	public void test1() throws Exception{
		System.out.println("Verify Appellate Summarries by Judge");
		main.getLegalResearchLink().click();
		forceWait(2);
		legal.getAppSummaries().click();
		forceWait(2);
//		apps.getJudge().sendKeys("Thomas L. Hog");
//		forceWait(2);
//		selectFromTypeAheadValues(apps.getJudge(), "Hon. Thomas L. Hogan");
//		forceWait(2);
		
		apps.getJudge().click();
		forceWait(2);
		apps.getJudge().getAttribute("autocomplete").replace("off", "on");
		apps.getJudge().sendKeys("Thomas L Hog");
		
		forceWait(3);
		apps.getJudge().getAttribute("autocomplete").replace("off", "on");
		RoboTester.firstDown();
		
		
		apps.getSearchButton().click();
		forceWait(2);
		
		WebElement article1 = results.getFirstAppellateResult();
		WebElement article2 = results.getSecondAppellateResult();
		String appSum1 = getString(article1);
		String appSum2 = getString(article2);
		System.out.println();
		System.out.println("Sample Appelate Summaries: ");
		System.out.println(appSum1);
		System.out.println(appSum2);
		
		condition1 = results.getAppellateResultCount().isDisplayed();
		condition2 = appSum1.length()>2 && appSum2.length()>2;
		
		tfTest3();
	}

	@Test
	public void test2() throws Exception{
		
		
		System.out.println("Verify Appellate Summarries by Court");
		main.getLegalResearchLink().click();
		forceWait(2);
		legal.getAppSummaries().click();
		forceWait(2);
		
		apps.getCourt().click();
		forceWait(2);
		apps.getCourt().clear();
		apps.getCourt().getAttribute("autocomplete").replace("off", "on");
		apps.getCourt().sendKeys("county law");
		
		forceWait(3);
		apps.getCourt().getAttribute("autocomplete").replace("off", "on");
		RoboTester.firstDown();


		forceWait(1);
		apps.getSearchButton().click();
		forceWait(3);
		WebElement article1 = results.getFirstAppellateResult();
		WebElement article2 = results.getSecondAppellateResult();
		String appSum1 = getString(article1);
		String appSum2 = getString(article2);
		System.out.println();
		System.out.println("Sample Appelate Summaries: ");
		System.out.println(appSum1);
		System.out.println(appSum2);
		
		
		condition1 = results.getAppellateResultCount().isDisplayed();
		condition2 = appSum1.length()>2 && appSum2.length()>2;
		
		tfTest3();
	}
}
