package regression.postlogin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pagefactories.DirectoryPageFactory;
import pagefactories.DirectoryResultFactory;
import pagefactories.LPMainPageFactory;
import pagefactories.OrgDirectoryPageFactory;
import seleniumwebdriver.SeleniumDriver;

public class VerifyOrgDirectory extends AbstractSprint{

	OrgDirectoryPageFactory org = PFCaller.orgFactory();
    DirectoryResultFactory res =  PFCaller.dirResultsFactory();
    LPMainPageFactory main = PFCaller.lpMainFactory();
    DirectoryPageFactory dir = PFCaller.directoryFactory();
    
    
    @Before
    public void reset(){
    main.getDirectoryTab().click();
    dir.getOrgButton().click();
    forceWait(1);
    System.out.println("------------------------------");
    forceWait(2);
    }
    
    @After
    public void finish(){
    	main.getHomeIcon().click();
    	forceWait(1);
    	main.getDirectoryTab().click();
    	dir.getOrgButton().click();
    	deleteCookies();
    	System.out.println("Test Complete");
    }
    
   
    
    
    @Test
    public void test1() throws Exception{
    	System.out.println("Verify Labels:\n");
		Map<String, Boolean> labels = new HashMap<String, Boolean>();
    	condition1=true;
    	stringElementToMap(labels, org.getOrgNameLabel(),  "ORGANIZATION NAME" );
    	stringElementToMap(labels, org.getOrgTypeLabel(),  "ORGANIZATION TYPE" );
    	stringElementToMap(labels, org.getPracticeAreasAddLabel(),  "PRACTICE AREAS");
    	stringElementToMap(labels, org.getPrimaryOrganizationLabel(),  "PRIMARY ORGANIZATION CITY");
    	stringElementToMap(labels, org.getORGResetButton(),"Reset Search");
    	stringElementToMap(labels, org.getSearch(), "Search"); //Figure out what's going on with Search Button
    	stringElementToMap(labels, org.getPeopleTab(), "PEOPLE");
    	
    	
    	for(Map.Entry<String, Boolean> entry : labels.entrySet()){
    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
    		if(!entry.getValue()){System.err.println("Missing Label Text: " + entry.getKey());
    			condition1 = false;
    		
    		}}
    	tfTest();
    
    }
    
    @Test
    public void test2() throws Exception{
    	System.out.println("Verify Fields:\n");
    	Map<String, Boolean> fields = new HashMap<String, Boolean>();
    	condition1 = true;
    	
    	enabledElementToMap(fields, org.getOrgCity(),"Primary City Field");
    	enabledElementToMap(fields, org.getOrgName(),"Organization Name Field");
    	enabledElementToMap(fields, org.getPracticcArea(), "Practice Area Field");
    	enabledElementToMap(fields, org.getORGResetButton(),"Reset Search");
    	enabledElementToMap(fields, org.getSearch(), "Search");
    	enabledElementToMap(fields, org.getPeopleTab(), "People Tab");
    	
    	for(Map.Entry<String, Boolean> entry : fields.entrySet()){
    		System.out.println(entry.getKey() + " enabled");
    		if(!entry.getValue()){System.err.println("Missing Field: " + entry.getKey());
    			condition1 = false;
    		
    		}}
    	tfTest();
    }
    
    @Test
    public void test3() throws Exception{
    	System.out.println("Verify Organization Type Dropdown:\n");
    	
    	Select typeSelect = org.getTypeSelect();
    	List<WebElement> types = typeSelect.getOptions();
    	List<String> options = new ArrayList<String>();
    	String[] anticipatedOptions = new String[]
    			{"Association or Society", "Civic","Corporate",
    			"Court","Select","Government","Law Firm",
    			"Other Entity","School"};

    	for(WebElement e : types){
    		options.add(e.getText().toString());
    	}
    	
    	System.out.println("Select Options");
    	for(String s : options){
    		System.out.println(s);
    	}
    	for(String s : anticipatedOptions){
    		condition1 = options.contains(s);
    		if(!condition1){
    			System.err.println("Missing Select Type: "+s);
    		}
    	}
    	
    	tfTest();

    }
    
    @Test
    public void test4() throws Exception{
    	System.out.println("Verify Advanced Directory Search\n");
    	System.out.println("Search by Organizaion Name: Law Bulletin");
    	org.getOrgName().sendKeys("Law Bulletin");
    	org.getSearch().click();
    	forceWait(3);
    	int i = res.getIntResultCount();
    	System.out.println("Results returned: "+i);
    	condition1 = i==10;
    	condition2 = SeleniumDriver.getDriver().findElement(By.linkText("Law Bulletin Publishing Company")).isDisplayed();
    	System.out.println("Expected Result: Law Bulletin Publishing Compnay Found: "+ condition2);
    	tfTest2();
    }
    
}
