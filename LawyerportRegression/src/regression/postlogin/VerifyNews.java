package regression.postlogin;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import pagefactories.LPMainPageFactory;
import pagefactories.NewsPageFactory;
import seleniumwebdriver.SeleniumDriver;

public class VerifyNews extends AbstractSprint{

	NewsPageFactory news = PFCaller.newsFactory();
	LPMainPageFactory main = PFCaller.lpMainFactory();
	@Before
	public void setTest(){
		main.getNewsLink().click();	
		forceWait(2);
	}
	@After
	public void endTest(){
		main.getHomeIcon().click();
    	forceWait(1);
    	deleteCookies();
    	System.out.println("Test Complete");
	}
	
	@Test
	public void test1() throws Exception{
		System.out.println("Verify News Link Directs to news page");
		
		
		String currentURL = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition1 = currentURL.contains("news");
		tfTest();
	}
	
	@Test
	public void test2() throws Exception{
		System.out.println("Verify Articles Headers");
		
		Map<String, Boolean> info = new HashMap<String, Boolean>();
		Map<String, Boolean> elements = new HashMap<String, Boolean>();
    	condition1=true;
    	condition2 = true;
    	
    	String appellateSummaries = getString(news.getAppellateCaseSummaries());
    	String inTheNews = getString(news.getInTheNews());
    	
    	WebElement jvrReporter = news.getJvrLogo();
    	WebElement cdlbLogo = news.getCdlbLogo();
    	
    	enabledElementToMap(elements, cdlbLogo, "Chicago Daily Lawy Bulletin");
    	enabledElementToMap(elements, jvrReporter, "Jury Verdict Reporter");
    	
    	stringToMap(info, appellateSummaries, "APPELLATE CASE SUMMARIES");
    	stringToMap(info, inTheNews, "IN THE NEWS");
    	
    	
    	for(Map.Entry<String, Boolean> entry : info.entrySet()){
    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
    		if(!entry.getValue()){System.err.println("Missing Header Text: " + entry.getKey());
    			condition1 = false;
    		}
    	tfTest();
    	}
    	

    	for(Map.Entry<String, Boolean> entry : elements.entrySet()){
    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
    		if(!entry.getValue()){System.err.println("Missing Header Text: " + entry.getKey());
    			condition2 = false;
    		
    		}}
    	tfTest2();
		
	}
	
	
//	@Test
	public void test3(){
		System.out.println("Verify News Search");
		Calendar cal = Calendar.getInstance();
		int day = Calendar.DAY_OF_MONTH;
		int month = Calendar.MONTH;
		int year = Calendar.YEAR;
		int aMonthAgo = Calendar.MONTH - 1;
		
		news.getPublishStart().sendKeys(aMonthAgo+"/"+day+"/"+year);
		news.getPublishEnd().sendKeys(month+"/"+day+"/"+year);
		news.getAuthor().sendKeys("storm");
		news.getSearch().click();
		
		
		news.getPublishStart().sendKeys(aMonthAgo+"/"+day+"/"+year);
		news.getPublishEnd().sendKeys(month+"/"+day+"/"+year);
		
	}
}
