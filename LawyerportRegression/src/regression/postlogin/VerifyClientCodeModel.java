package regression.postlogin;
//done

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.LoginCredentials;
import lawyerportdominion.PFCaller;

import org.junit.Test;

import pagefactories.ClientCodeFactory;
import pagefactories.LPMainPageFactory;
import pagefactories.LandingPageFactory;

public class VerifyClientCodeModel extends AbstractSprint {
	
	ClientCodeFactory ccf = PFCaller.ccFactory();
	LPMainPageFactory lp = PFCaller.lpMainFactory();
	LandingPageFactory home = PFCaller.landingFactory();
	private String client;
	private String modelContent;
	

	@Test
	public void tests1_through_4() throws Exception{
		System.out.println("Test - 1");
		System.out.println("Verify Clicking Edit Client Code Button Brings Up Client Modal");
	
		home.getEditCC().click();
		forceWait(1);
		modelContent = home.getConfirmCCModal().getText().toString();
	
		condition1 = modelContent.contains("Enter a Client");
		tfTest();
		
		System.out.println("Test - 2");
		System.out.println("Verify Client Name has been properly imported from log-in");
		client = ccf.getClientCodeInput().getText().toString();
		System.out.println("Current Client: "+ client);
		condition2 = client.contentEquals(LoginCredentials.getClient());
		tfTest2();
		
		System.out.println("Test - 3");
		System.out.println("Verify Edit Client Model Content");
		condition2 = modelContent.contains("Enter a Client Name and/or ID");
		tfTest2();
		
		System.out.println("Test - 4");
		System.out.println("Verify Edit Client Modal Cancel Button works and returns original client name");
		ccf.getCCCloseButton().click();
		String currentCleint = lp.getClientCodeInput().getText().toString();
		System.out.println(currentCleint);
		condition1 = currentCleint.contains(client);
		tfTest();
	}
	
	
	
	@Test
     public void test5() throws Exception{
		System.out.println("Test - 5");
		System.out.println("Verify Client Code Model Warning on Null Input");
		lp.getEditCC().click();
		forceWait(1);
		ccf.getClientCodeModalInput().clear();
		ccf.getClientCodeOK().click();
		forceWait(1);
		String s = ccf.getCCWarning().getText().toString();
		System.out.println(s);
		condition1 = s.contains("empty");
		
		tfTest();
		
		ccf.getCCCloseButton().click();
	}
	
	@Test
	public void test6() throws Exception{
		System.out.println("Test - 6");

		System.out.println("Verify user changes to client code are saved correctly");
		lp.getEditCC().click();
		forceWait(1);
		ccf.getClientCodeModalInput().clear();
		ccf.getClientCodeModalInput().sendKeys("Test User");
		ccf.getClientCodeOK().click();
		
		String newClient = lp.getClientCodeInput().getText().toString();
		condition1 = newClient.contains("Test User");
		tfTest();
	}
	
//	@Test
//	public void test3
//	

	
	
//    @Test
//    public void test2() throws Exception{
//    	write("Verify Client Modal Contents");
//    	 String parentWindowHandle = SeleniumDriver.getDriver().getWindowHandle(); // save the current window handle.
//         WebDriver popup = null;
//    	home.getEditCC().click();
//    	
//    	
//    	ccf.getClientCodeModalInput().sendKeys(randomLastName());
//    	ccf.getClientCodeOK().click();
//    	forceWait(1);
//    	ccf.getClientCodeOK().click();
//    	
//    	tfTest();
//    }
     
}
