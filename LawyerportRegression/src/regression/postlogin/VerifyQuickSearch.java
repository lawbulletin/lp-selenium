package regression.postlogin;

import lawyerportdominion.AbstractSprint;
import lawyerportdominion.PFCaller;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pagefactories.DirectoryResultFactory;
import pagefactories.LPMainPageFactory;
import seleniumwebdriver.SeleniumDriver;

public class VerifyQuickSearch extends AbstractSprint{

	LPMainPageFactory main = PFCaller.lpMainFactory();
	DirectoryResultFactory dir = PFCaller.dirResultsFactory();

	
	@Test
	public void test1() throws Exception{
		System.out.println("Veryify Quick Search for People");
		
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionPeople().click();
		main.getSearchTextbox().sendKeys(randomLastName());
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		String results = dir.getResultCount().getText().toString();
		System.out.println("QuickSearch Results:");
		System.out.println(results);
		condition1 = results.contains("People");
		tfTest();
	}
	
	@Test
	public void test2() throws Exception{
		System.out.println("Veryify Quick Search for Peter Mierzwa Returns Appropriate Content");
		
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionPeople().click();
		main.getSearchTextbox().sendKeys("Peter Mierzwa");
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		String results = dir.getResultCount().getText().toString();
		System.out.println("Checking Results:");
		System.out.println(results);
		
		condition1 = results.contains("1-1 of 1 People") && results.contains("Peter Mierzwa");
		forceWait(1);
		System.out.println("Checking Image:");
		WebElement img = SeleniumDriver.getDriver().findElement(By.xpath("(//img)[3]"));
		String path = img.getAttribute("src");
		int height = img.getSize().getHeight();
		int width = img.getSize().getWidth();
		
		System.out.println("Image Height: "+height +" "+"Image Width: "+width);		
		condition2 = img.isDisplayed() && (height == 72 && width == 48) && (path.contains("portraits/1135550"));
	
		//System.out.println(path);
		tfTest3();
		
	}
		
	@Test
	public void test3() throws Exception{
		System.out.println("Verify Search Filters");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionPeople().click();
		String name = randomLastName();
		main.getSearchTextbox().sendKeys(name);
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		String results = dir.getResultCount().getText().toString();
		Integer people = dir.getIntResultCount();
		System.out.println("Results prior to filter: "+ people);
		forceWait(1);
		dir.getLawFirmFilter().click();
		forceWait(1);
		Integer people2 = dir.getIntResultCount();
		String results2 = dir.getResultCount().getText().toString();
		System.out.println("Results after filter: "+ people2);
		condition1 = results2.contains(name) && results.contains(name);
		condition2 = people >= people2;
		
		tfTest3();
		
	}
	
	@Test
	public void test4() throws Exception{
		System.out.println("Verify Reset Filter Functionality");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionPeople().click();
		String name = randomLastName();
		main.getSearchTextbox().sendKeys(name);
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		Integer people = dir.getIntResultCount();
		System.out.println("Results prior to clicking filter: "+ people);
		forceWait(2);
		dir.getLawFirmFilter().click();
		forceWait(1);
		Integer people2 = dir.getIntResultCount();
		System.out.println("Results on clicking filter: "+ people2);
		forceWait(2);
		
		dir.getResetFilter().click();
		SeleniumDriver.getDriver().manage().deleteAllCookies();
		Integer people3 = dir.getIntResultCount();
		System.out.println("Results on reseting filters: "+ people3);
		forceWait(1);
		condition1 = people3 >= people2 ;
		condition2 = people == people3;
		tfTest3();
	}
	
	@Test
	public void test5() throws Exception{
		System.out.println("Verify Result Navigation Buttons");
		String results=null, results2=null;
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionPeople().click();
		String name = randomLastName();
		main.getSearchTextbox().sendKeys(name);
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		if(dir.getPage2Button().isDisplayed()){
		results = dir.getResultCount().getText().toString();
		forceWait(1);
		dir.getNextPage().click();
		forceWait(2);
		results2 = dir.getResultCount().getText().toString();
		}
		else{System.out.println("Random Name inappropriate for test, resetting test");
		     SeleniumDriver.getDriver().manage().deleteAllCookies();
			test5();}
		
		
		condition1 =! results.contentEquals(results2);
		tfTest();
	}
	
	@Test
	public void test6() throws Exception{
		System.out.println("Verify Quick Search for Organizations");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionOrganization().click();
		main.getSearchTextbox().sendKeys(randomOrg());
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		String results = dir.getResultCount().getText().toString();
		System.out.println("QuickSearch Results:");
		System.out.println(results);
		condition1 = results.contains("Organizations");
		tfTest();
		
	}
	
	@Test
	public void test7() throws Exception{
		System.out.println("Verify profile images");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionOrganization().click();
		main.getSearchTextbox().sendKeys("Jenner");
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		System.out.println("Checking Image:");
		WebElement img = SeleniumDriver.getDriver().findElement(By.xpath("(//img)[3]"));
		int height = img.getSize().getHeight();
		int width = img.getSize().getWidth();
		
		System.out.println("Image Height: "+height +" "+"Image Width: "+width);		
		condition1 = img.isDisplayed() && (height == 72 && width == 48);
		
		tfTest();
	}
	
	@Test
	public void test8() throws Exception{
		System.out.println("Verify Filters and Reset Filters");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionOrganization().click();
		main.getSearchTextbox().sendKeys("Jenner");
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		String results = dir.getResultCount().getText().toString();
		
		System.out.println("Results prior to filter: "+ results);
		forceWait(1);
		dir.getCityFilter().click();
		forceWait(1);
		String results2 = dir.getResultCount().getText().toString();
		System.out.println("Results after filter: "+ results2);
		condition1 = results2.contains("Jenner") && results.contains("Jenner");
		condition2 =! results.contains(results2);
		forceWait(1);
		
		dir.getResetFilter().click();
		deleteCookies();
		forceWait(1);
		String results3 = dir.getResultCount().getText().toString();
		System.out.println("Results after Filter Reset: "+results3);
		condition3 = results.contentEquals(results3);
		
		tfTest4();
	}
	
	@Test
	public void test9() throws Exception{
		System.out.println("Verify Filters");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionOrganization().click();
		main.getSearchTextbox().sendKeys("Jenner");
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		
String results = dir.getResultCount().getText().toString();
		
		System.out.println("Results prior to filter: "+ results);
		forceWait(1);
		dir.getCityFilter().click();
		forceWait(1);
		String results2 = dir.getResultCount().getText().toString();
		System.out.println("Results after filter: "+ results2);
		condition1 = results2.contains("Jenner") && results.contains("Jenner");
		condition2 =! results.contains(results2);
		forceWait(1);
		
		dir.getResetFilter().click();
		deleteCookies();
		forceWait(1);
		String results3 = dir.getResultCount().getText().toString();
		System.out.println("Results after Filter Reset: "+results3);
		condition3 = results.contentEquals(results3);
		
		tfTest4();
	}
	
	@Test
	public void testa10() throws Exception{
		System.out.println("Verify Organization Navigation Buttons");
		String results=null, results2 = null;
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionOrganization().click();
		main.getSearchTextbox().sendKeys("Smith");
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		if(dir.getPage2Button().isDisplayed()){
			 results = dir.getResultCount().getText().toString();
			forceWait(1);
			dir.getNextPage().click();
			forceWait(2);
			results2 = dir.getResultCount().getText().toString();
			}
			else{System.out.println("Random Name inappropriate for test, resetting test");
			     SeleniumDriver.getDriver().manage().deleteAllCookies();
				test5();}
			
			
			condition1 =! results.contentEquals(results2);
			tfTest();
		
	}
	
	@Test
	public void testa11() throws Exception{
		System.out.println("Verify Reset Search");
		main.getSearchDropdown().click();
		main.getSearchDropdownSelectionOrganization().click();
		main.getSearchTextbox().sendKeys("Jenner");
		forceWait(1);
		main.getSearchButton().click();
		forceWait(1);
		
		dir.getResetSearch().click();
		forceWait(1);
		String url = SeleniumDriver.getDriver().getCurrentUrl().toString();
		condition1 = url.contains("advanced-search");
		tfTest();
		
	}
	
}
