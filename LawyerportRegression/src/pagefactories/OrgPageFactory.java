package pagefactories;

import java.util.List;

import lawyerportdominion.AbstractSprint;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class OrgPageFactory extends AbstractSprint{
	
	@FindBy (how = How.XPATH, using = "//li[@id='navDir']")
	private WebElement getDirctory; 

	@FindBy (how = How.XPATH, using = "//li[@id='org']/a")
	private WebElement getOrg;
	
	@FindBy (how = How.ID, using = "form-field-0")
	private WebElement orgName; 
	
	@FindBy (how = How.ID, using = "dir-org-run-advanced-search")
	private WebElement orgSearchButton; 
	
//	public WebElement navToOrg(){
//		 getDirctory.click();
//		 randomHumanWait("wait for org page to be loaded");
//		 return getOrg; 
//	}
		
	public WebElement getOrgName() {
		return orgName;
	}
	
	public WebElement getOrgSearchButton() {
		return orgSearchButton;
	}
	
	
	/********************************************************************************
	 * TO BE USED AFTER  
	 **********************************************************************************/
	@FindBy(how = How.ID, using = "edit-profile")
	private WebElement editProfile;
	
	@FindBy(how = How.ID, using ="org-toc-corp-contact-li")
	private WebElement contactInfo;
		
	@FindBy(how = How.ID, using ="org-toc-corp-our-people-li")
	private WebElement ourPeople;
	
	@FindBy(how = How.ID, using ="org-toc-corp-our-people-li")
	private WebElement pratcieArea;

	@FindBy(how = How.ID, using ="button-org-contact-add")
	private WebElement addContantInfo;
	
	@FindBy(how = How.ID, using ="button-title-edit")
	private WebElement contactInfoEditName;
	
	@FindBy (how = How.ID, using ="form-field-6")
	private  WebElement country;
		
	@FindBy (how = How.ID, using ="form-field-2")
	private WebElement streetAddress;
	
	@FindBy (how = How.ID, using ="form-field-9")
	private WebElement city;
	
	@FindBy (how = How.ID, using ="form-field-10")
	private WebElement zipCode;
	
	@FindBy (how = How.ID, using ="form-field-11")
	private WebElement officePhone;
	
	@FindBy(how = How.ID, using ="form-button-Save")
	private WebElement save;
	
	@FindBy(how = How.ID, using = "form-button-Cancel")
	private WebElement cancel;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@id, 'button-org-contact-del-')])[2]")
	private WebElement deleteConatactInfo;
	
	@FindBy(how = How.XPATH, using = "(//span[contains(@id, 'button-org-contact-primary-')])[1]")
	private WebElement makePrimary;
	
	
	@FindBy (how = How.XPATH, using ="(//span[contains(@id, 'button-org-contact-edit-')])[1]")
	private WebElement editPrimaryContanctInfo;
	
	public WebElement getEditButton() {
		return editProfile;
	}
	
	public WebElement getMainContactInfo(){
		return contactInfo;
	}

	public WebElement getMainOurPeople() {
		return ourPeople;
	}
	
	public WebElement getMainPraticeArea(){
		return pratcieArea; 
	}
	
	public WebElement getContactInfoEditName() {
		return contactInfoEditName;
	}
	
	public WebElement getAddContanctInfo() {
		return addContantInfo;
	}
	
	public WebElement getStreetAddress(){
		return streetAddress;
	}
	
	public WebElement getCity(){
		return city;
	}
	
	public WebElement getZipCode(){
		return zipCode;
	}
	
	public Select selectCountry() {
		return new Select(country);
	}
	
	public WebElement getOfficePhone() {
		return officePhone;
	}
	
	public WebElement getSaveButton() {
		 return save;
	}
	
	public WebElement getCancelButton() {
		 return cancel;
	}
	
	public WebElement getDeleteContanctInfo(){
		return deleteConatactInfo;
	}
	
	public WebElement getMakePrimaryButton() {
		return makePrimary;
	}
	

	/****************************************
	 * our people stuff
	 * 
	 ****************************************/

	@FindBy (how = How.ID, using ="form-field-0")
	private WebElement ourPeopleFirstName;
	
	@FindBy (how = How.ID, using ="form-field-1")
	private WebElement ourPeopleLastName;
	
	@FindBy (how = How.ID, using ="button-org-ourPeople-search-btn")
	private WebElement ourPeopleSearchButton;
	
	@FindBy (how = How.XPATH, using ="//select[contains(@id,'edit-status-')]")
	private WebElement membership;
	
	@FindBy (how = How.ID, using ="form-button-Yes")
	private WebElement confirmDeleteYes;
	
	@FindBy (how = How.ID, using ="form-button-Ok")
	private WebElement membershipsUpdatedOK;

	@FindBy (how = How.ID, using ="button-org-ourPeople-submit-button")
	private WebElement submit;
	
	@FindBy (how = How.ID, using ="button-org-ourPeople-add-btn")
	private WebElement addNewMember;
	
	@FindBy (how = How.XPATH, using = "//input[contains(@class,'yui3-tokeninput-input')]")
	private WebElement memeberName; 
	
	@FindBy (how = How.ID, using = "form-field-2")
	private WebElement makePrimaryContact; 
	
	@FindBy (how = How.ID, using = "form-field-5")
	private WebElement address; 
	
	@FindBy(how = How.ID, using ="form-field-7")
	private WebElement addMemberTitle;
	
	@FindBy (how = How.XPATH, using ="//div[contains(@class,'yui3-datatable-content')]/table/tbody[2]/tr/td[1]")
	private List<WebElement> ourPeopleList; 
	
	public WebElement getOurPeopleFirstName() {
		return ourPeopleFirstName;
	}
	
	public WebElement getOurPeopleLastName() {
		return ourPeopleLastName;
	}
	
	public WebElement getOurPeopleSearchButtion() {
		return ourPeopleSearchButton;
	}
	
	public Select selectMembershitp() {
		return new Select(membership);
	}
	
	public WebElement getConfimDeleteYes() {
		return confirmDeleteYes;
	}
	
	public WebElement getSubmitButton() {
		return submit;
	}
	
	public WebElement getMembershipsUpdatedOK(){
		return membershipsUpdatedOK;
	}

	public WebElement getAddNewMemeber() {
		return addNewMember;
	}
	
	public WebElement getMakePrimaryContact() {
		return makePrimaryContact;
	}
	
	public WebElement getMemeberName() {
		return memeberName;
	}
	
	public Select selectAddress() {
		return new Select(address);
	}
	
	public List<WebElement> getOurPeopleList() {
		return ourPeopleList;
	}
	
	public WebElement getAddMemberTitle(){
		return addMemberTitle;
	}
}
