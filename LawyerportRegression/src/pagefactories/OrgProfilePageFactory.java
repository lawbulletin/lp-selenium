package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OrgProfilePageFactory {

	@FindBy(how = How.CLASS_NAME, using = "profile-title")
	private WebElement orgTitle;
	
	public WebElement getOrgTitle(){
		return orgTitle;
	}
	
	@FindBy(how = How.ID, using = "org-toc-corp-contact-li") 
	 private WebElement  contactInfo;

	 public WebElement getContactInfo(){ return contactInfo;}

	@FindBy(how = How.ID, using = "org-toc-corp-description-li") 
	 private WebElement  corporateDescription;

	 public WebElement getCorporateDescription(){ return corporateDescription;}

	@FindBy(how = How.ID, using = "org-toc-corp-management-li") 
	 private WebElement  corporateManagement;

	 public WebElement getCorporateManagement(){ return corporateManagement;}

	@FindBy(how = How.ID, using = "org-toc-corp-org-li") 
	 private WebElement  corporateOrgnaization;

	 public WebElement getCorporateOrgnaization(){ return corporateOrgnaization;}

	@FindBy(how = How.ID, using = "org-toc-corp-our-people-li") 
	 private WebElement  ourPeople;

	 public WebElement getOurPeople(){ return ourPeople;}

	@FindBy(how = How.ID, using = "org-toc-corp-industries-li") 
	 private WebElement  industries;

	 public WebElement getIndustries(){ return industries;}

	@FindBy(how = How.ID, using = "org-toc-corp-news-pubs-honors-li") 
	 private WebElement  newsPubsHonors;

	 public WebElement getNewsPubsHonors(){ return newsPubsHonors;}

	@FindBy(how = How.ID, using = "org-toc-corp-news-pubs-honors-li") 
	 private WebElement  profilePic;

	 public WebElement getProfilePic(){ return profilePic;}
}
