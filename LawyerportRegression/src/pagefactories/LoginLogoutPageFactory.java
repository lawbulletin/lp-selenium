package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginLogoutPageFactory {

	@FindBy(how = How.ID, using ="username")
	private WebElement username;
	
	@FindBy(how = How.ID, using ="password")
	private WebElement password;
	
	@FindBy(how = How.ID, using ="resetButton")
	private WebElement clickSignin;
	

	public void enterUserName(String searchTerm) {
		username.click();
		username.sendKeys(searchTerm);
	}
	
	public void enterPassword(String searchTerm) {
		password.clear();
		password.sendKeys(searchTerm);
	}
	
	
	public void signin() {
		clickSignin.click();
	}
}
