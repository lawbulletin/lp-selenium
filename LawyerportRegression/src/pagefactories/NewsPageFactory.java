package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class NewsPageFactory {
	
	@FindBy(how = How.XPATH, using ="//li[@id='navNews']")
	private WebElement news;
	
	@FindBy (how =How.ID, using ="form-field-0")
	private WebElement keywords; 

	@FindBy (how =How.ID, using ="form-field-1")
	private WebElement publishStart; 
	
	@FindBy (how =How.ID, using ="form-field-2")
	private WebElement PublishEnd; 
	
	@FindBy (how =How.ID, using ="form-field-3")
	private WebElement author; 
	
	@FindBy (how =How.ID, using ="form-field-4")
	private WebElement title; 
	
	@FindBy (how =How.ID, using ="form-field-5")
	private WebElement publication; 
	
	@FindBy(how = How.ID, using = "news-run-advanced-search")
	private WebElement search;
	
	@FindBy(how = How.ID, using = "news-reset-advanced-search-bottom")
	private WebElement reset;
	
	public WebElement navNews(){
		return news;
	}
	
	public WebElement getKeywords(){
		return keywords;
	}
	
	public WebElement getPublishStart() {
		return publishStart;
	}
	
	public WebElement getPublishEnd() {
		return PublishEnd;
	}
	
	public WebElement getAuthor(){
		return author;
	}
	
	public WebElement getTitle() {
		return title;
	}
	
	public Select getPublication(){
		return new Select(publication);
	}
	
	public WebElement getSearch(){
		return search;
	}
	
	public WebElement getReset(){
		return reset;
	}


@FindBy(how = How.ID, using = "news-title-id-small") 
	 private WebElement  jvrOfTheDay;

 public WebElement getJvrOfTheDay(){ return jvrOfTheDay;}

@FindBy(how = How.ID, using = "tn-news") 
 private WebElement  inTheNews;

 public WebElement getInTheNews(){ return inTheNews;}

@FindBy(how = How.ID, using = "jv-news-logo") 
 private WebElement  jvrLogo;

 public WebElement getJvrLogo(){ return jvrLogo;}

@FindBy(how = How.ID, using = "as-news") 
 private WebElement  appellateCaseSummaries;

 public WebElement getAppellateCaseSummaries(){ return appellateCaseSummaries;}

@FindBy(how = How.ID, using = "itn-news") 
 private WebElement  inTheNew;

 public WebElement getInTheNew(){ return inTheNew;}

@FindBy(how = How.ID, using = "cldb-news-logo") 
 private WebElement  cdlbLogo;

 public WebElement getCdlbLogo(){ return cdlbLogo;}
}
