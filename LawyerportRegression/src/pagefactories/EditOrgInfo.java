package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class EditOrgInfo {
	
	@FindBy (how = How.XPATH, using ="(//span[contains(@id, 'button-org-contact-edit-')])[1]")
	private WebElement editPrimaryContanctInfo;
	
	@FindBy (how = How.ID, using ="form-field-4")
	private WebElement streetAddress;
	
	@FindBy (how = How.ID, using ="form-field-8")
	private  WebElement country;
			
	@FindBy (how = How.ID, using ="form-field-11")
	private WebElement city;
	
	@FindBy (how = How.ID, using ="form-field-12")
	private WebElement zipCode;
	
	@FindBy (how = How.ID, using ="form-field-13")
	private WebElement officePhone;
	
	@FindBy(how = How.ID, using ="form-button-Save")
	private WebElement save;
	
	@FindBy(how = How.ID, using = "form-button-Cancel")
	private WebElement cancel;
	
	@FindBy(how = How.ID, using ="org-toc-corp-contact-li")
	private WebElement contactInfo;
		

	public WebElement getMainContactInfo(){
		return contactInfo;
	}
	
	public WebElement getStreetAddress(){
		return streetAddress;
	}
	
	public WebElement getCity(){
		return city;
	}
	
	public WebElement getZipCode(){
		return zipCode;
	}
	
	public Select selectCountry() {
		return new Select(country);
	}
	
	public WebElement getOfficePhone() {
		return officePhone;
	}
	
	public WebElement getSaveButton() {
		 return save;
	}
	
	public WebElement getCancelButton() {
		 return cancel;
	}
	
	public WebElement getEditPrimaryContacntInfo(){
		return editPrimaryContanctInfo;
	}
	
	

}
