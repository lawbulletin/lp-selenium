package pagefactories;

import lawyerportdominion.AbstractSprint;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


/**
 *  Page Factory Library to hold Court Docket element locator information 
 * @author hipatel
 *
 */
public class CDPageFactory extends AbstractSprint {
	
	@FindBy (how = How.XPATH, using = "//li[@id='navRes']")
	private WebElement getDocket; 
	

	
	@FindBy(how = How.XPATH, using ="(//div[contains(@class,'yui3-tokeninput-content')]/ul/li)[1]")
	private WebElement judge; 


	
	@FindBy(id = "court-dockets-reset-advanced-search-bottom")
	private WebElement resetSearch;
	
	public WebElement getSearchReset(){
		return resetSearch;
	}
	@FindBy(how = How.ID, using ="form-field-3")
	private WebElement defendent; 
	
	@FindBy(how = How.XPATH, using ="(//div[contains(@class,'yui3-tokeninput-content')]/ul/li)[2]")
	private WebElement lawyerFirm; 
	

	@FindBy(how = How.ID, using ="form-field-7")
	private WebElement startDate; 
	
	@FindBy(how = How.ID, using ="form-field-8")
	private WebElement endDate; 
	
	@FindBy(how = How.ID, using ="form-field-9")
	private WebElement jursidiction; 
	
	@FindBy(how = How.ID, using ="court-dockets-run-advanced-search")
	private WebElement search; 
	
	@FindBy(how = How.ID, using ="court-dockets-reset-advanced-search-bottom")
	private WebElement reset; 
	
	
	/**
	 * nav to court docket page
	 */
//	public WebElement navCourtDocket() {
//		return getDocket;
//	
//	}
	
	public WebElement getDocketSearchButton() {
		return search;
	}
	public WebElement getDocketRestSearchButton() {
		return reset;
	}
	
	
	public WebElement getDocketJudge() {
		return judge;
	}
	
	
	public WebElement geDocketDefendent() {
		return defendent;
	}
	public WebElement getDOcketLawyerFirm() {
		return lawyerFirm;
	}
	
	public WebElement getDocketStartDate() {
		return startDate;
	}
	public WebElement getDocketEndDate() {
		return endDate;
	}
	
	 @FindBy(how = How.ID, using = "form-field-0") 
	 private WebElement  caseNumber;

	 public WebElement getCaseNumber(){ return caseNumber;}

	@FindBy(how = How.ID, using = "form-field-2") 
	 private WebElement  plaintiff;

	 public WebElement getPlaintiff(){ return plaintiff;}




	@FindBy(how = How.ID, using = "form-field-5") 
	 private WebElement  plintiffAndORDefendent;

	 public WebElement getPlintiffAndORDefendent(){ return plintiffAndORDefendent;}





	@FindBy(how = How.ID, using = "form-field-9") 
	 private WebElement  jurisdiction;

	 public WebElement getJurisdiction(){ return jurisdiction;}

	@FindBy(how = How.XPATH, using = "//option[1]") 
	 private WebElement  jurisdictionIlCook1st;

	 public WebElement getJurisdictionIlCook1st(){ return jurisdictionIlCook1st;}



	@FindBy(how = How.XPATH, using = "//option[2]") 
	 private WebElement  jurisdictionIlCook2nd;

	 public WebElement getJurisdictionIlCook2nd(){ return jurisdictionIlCook2nd;}

	@FindBy(how = How.XPATH, using = "//option[3]") 
	 private WebElement  jurisdictionIlCook3rd;

	 public WebElement getJurisdictionIlCook3rd(){ return jurisdictionIlCook3rd;}

	@FindBy(how = How.XPATH, using = "//option[3]") 
	 private WebElement  jurisdictionIlCook4th;

	 public WebElement getJurisdictionIlCook4th(){ return jurisdictionIlCook4th;}



	@FindBy(how = How.XPATH, using = "//option[6]") 
	 private WebElement  jurisdictionIlCook6th;

	 public WebElement getJurisdictionIlCook6th(){ return jurisdictionIlCook6th;}

	@FindBy(how = How.XPATH, using = "//option[5]") 
	 private WebElement  jurisdictionIlCook5th;

	 public WebElement getJurisdictionIlCook5th(){ return jurisdictionIlCook5th;}

	@FindBy(how = How.XPATH, using = "//option[7]") 
	 private WebElement  ilCookChancery;

	 public WebElement getIlCookChancery(){ return ilCookChancery;}



	@FindBy(how = How.XPATH, using = "//option[8]") 
	 private WebElement  ilCookDomestic;

	 public WebElement getIlCookDomestic(){ return ilCookDomestic;}

	@FindBy(how = How.XPATH, using = "//option[9]") 
	 private WebElement  ilCookLaw;

	 public WebElement getIlCookLaw(){ return ilCookLaw;}
	
}


