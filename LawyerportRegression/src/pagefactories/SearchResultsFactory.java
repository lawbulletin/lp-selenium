package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


//This class contains elements for more than one kind of search result page,
//be sure to specify what kind of search results each call/method refers to in its name
public class SearchResultsFactory {

	@FindBy(how = How.XPATH, using = "//a[contains(@class, 'search-click-through')]")
	private WebElement directoryFirstResult;
	
	@FindBy(how = How.XPATH, using = "(//p [contains(@id, 'search-header-sub-title')])[1]")
	private WebElement directoryResultCount;
	
	@FindBy(how = How.XPATH, using = "(//a)[17]")
	private WebElement cdFirstResult;
	


@FindBy(how = How.XPATH, using = "(//p/a)[2]") 
 private WebElement  secondAppellateResult;

 public WebElement getSecondAppellateResult(){ return secondAppellateResult;}

@FindBy(how = How.XPATH, using = "(//p/a)[1]") 
 private WebElement  firstAppellateResult;

 public WebElement getFirstAppellateResult(){ return firstAppellateResult;}

@FindBy(how = How.XPATH, using = "(//p/a)[3]") 
 private WebElement  thirdAppellateResult;

 public WebElement getThirdAppellateResult(){ return thirdAppellateResult;}

@FindBy(how = How.XPATH, using = "(//p)[1]") 
 private WebElement  appellateResultCount;

 public WebElement getAppellateResultCount(){ return appellateResultCount;}

@FindBy(how = How.ID, using = "researching-app-summaries-link") 
 private WebElement  appellateResultSearchResultPageHeader;

 public WebElement getAppellateResultSearchResultPageHeader(){ return appellateResultSearchResultPageHeader;}

@FindBy(how = How.ID, using = "AS-result-button") 
 private WebElement  returnToAppellateSearchForm;

 public WebElement getReturnToAppellateSearchForm(){ return returnToAppellateSearchForm;}
 
	public WebElement getCDFirstResult(){
		return cdFirstResult;
	}
	
	public WebElement getDirectoryResultCount(){
		return directoryResultCount;
	}
	
	public WebElement getDirectoryFirstResult(){
		return directoryFirstResult;
	}
}
