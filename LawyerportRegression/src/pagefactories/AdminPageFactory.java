package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdminPageFactory {


@FindBy(how = How.ID, using = "zyuh") 
 private WebElement  manageEventsIcon;

 public WebElement getManageEventsIcon(){ return manageEventsIcon;}


@FindBy(how = How.ID, using = "button-manage-event-add-event-button") 
 private WebElement  addEvent;

 public WebElement getAddEvent(){ return addEvent;}

@FindBy(how = How.ID, using = "button-manage-event-search-button") 
 private WebElement  searchEvent;

 public WebElement getSearchEvent(){ return searchEvent;}
 
 @FindBy(how = How.ID, using = "button-manage-events-cancel-button") 
 private WebElement  eventCancelButton;

 public WebElement getEventCancelButton(){ return eventCancelButton;}

@FindBy(how = How.ID, using = "button-manage-events-submit-button") 
 private WebElement  eventSubmitButton;

 public WebElement getEventSubmitButton(){ return eventSubmitButton;}

 @FindBy(how = How.ID, using = "form-field-0") 
 private WebElement  eventTitleMainPage;

 public WebElement getEventTitleMainPage(){ return eventTitleMainPage;}
 
 @FindBy(how = How.XPATH, using = "//ul[1]/li[3]/a") 
 private WebElement  backToLawyerport;

 public WebElement getBackToLawyerport(){ return backToLawyerport;}
 
@FindBy(how = How.ID, using = "form-field-1") 
 private WebElement  eventTitle;

 public WebElement getEventTitle(){ return eventTitle;}

@FindBy(how = How.ID, using = "form-field-2") 
 private WebElement  eventStartDate;

 public WebElement getEventStartDate(){ return eventStartDate;}

@FindBy(how = How.ID, using = "form-field-3") 
 private WebElement  eventEndDate;

 public WebElement getEventEndDate(){ return eventEndDate;}

@FindBy(how = How.ID, using = "form-field-4") 
 private WebElement  eventTime;

 public WebElement getEventTime(){ return eventTime;}

@FindBy(how = How.ID, using = "form-field-5") 
 private WebElement  eventAbstract;

 public WebElement getEventAbstract(){ return eventAbstract;}

@FindBy(how = How.ID, using = "form-field-6") 
 private WebElement  eventVenue;

 public WebElement getEventVenue(){ return eventVenue;}

@FindBy(how = How.ID, using = "form-field-7") 
 private WebElement  eventAddress;

 public WebElement getEventAddress(){ return eventAddress;}

@FindBy(how = How.ID, using = "form-field-10") 
 private WebElement  eventCity;

 public WebElement getEventCity(){ return eventCity;}

@FindBy(how = How.ID, using = "cke_contents_form-field-18") 
 private WebElement  eventDescription;

 public WebElement getEventDescription(){ return eventDescription;}
}
