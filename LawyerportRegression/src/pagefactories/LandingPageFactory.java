package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LandingPageFactory {

	public WebElement getsignInButton(){
		return signInButton;
	}
	
	public WebElement getDirectoryBox(){
		return directoryBox;
	}
	
	public WebElement getNewsBox(){
		return newsBox;
	}
	
	public WebElement getLegalResearchBox(){
		return legalResearchBox;
	}
	
	public WebElement getDirectoryInfoButton(){
		return directoryInfoButton;
	}
	
	public WebElement getNewsInfoButton(){
		return newsInfoButton;
	}
	
	public WebElement getLRInfoButton(){
		return lrInfoButton;
	}
	
	public WebElement getImageContainer(){
		return imageContainer;
	}
	
	public WebElement getHeaderLogo(){
		return headerLogo;
	}
	
	public WebElement getFooterPartners(){
		return footerPartners;
	}
	
	public WebElement getCopyrightFooter(){
		return copyrightFooter;
	}
	
	public WebElement getEditCC(){
		return editCC;
	}
	
	public WebElement getClientCodeInput(){
		return ccInput;
	}
	public WebElement  getConfirmCCModal(){
		return modalConfirmation;
	}
	
	@FindBy(how = How.CLASS_NAME, using ="modal-content-confirmation")
	private WebElement modalConfirmation;
	
	
	@FindBy(id = "client-code-input")
	private WebElement ccInput;
	
	@FindBy(id="client-code-button")
		private WebElement editCC;
	
	@FindBy(id="signin-button")
		private WebElement signInButton;
	
	@FindBy( id="learn-more-cms-directory")
		private WebElement directoryBox;
	
	@FindBy(id="learn-more-cms-news")
		private WebElement newsBox;
	
	@FindBy(id="learn-more-cms-research")
		private WebElement legalResearchBox;
	
	@FindBy(how = How.XPATH, using = "(//a)[3]")
		private WebElement directoryInfoButton;
	
	@FindBy(how = How.XPATH, using = "(//a)[4]")
		private WebElement newsInfoButton;
	
	@FindBy(how = How.XPATH, using = "(//a)[5]")
		private WebElement lrInfoButton;
	
	@FindBy( id="image-container")
		private WebElement imageContainer;
		
	@FindBy( id="header-logo")
		private WebElement headerLogo;
	
	@FindBy(id="footer-partners")
		private WebElement footerPartners;
	
	@FindBy(id="footer-copyright")
		private WebElement copyrightFooter;

	@FindBy(id = "form-button-Save")
	private WebElement clientCodeOK;
	
	
	@FindBy(id = "client-code-modal-input")
	private WebElement ccModalInput;
	
	
	
	@FindBy(id = "client-code-warning")
	private WebElement ccWarning;
	
	@FindBy(id ="form-button-Close") 
	private WebElement formButtonClose;
	
	@FindBy(id = "client-code-modal-title")
	private WebElement ccTitle;
	
	
	
	public WebElement ccCloseButton(){
		return formButtonClose;
	}

	public WebElement getConfirmation(){
		return modalConfirmation;
	}
	
	
	public WebElement getCCTitle(){
		return ccTitle;
	}
	
	public WebElement getCCWarning(){
		return ccWarning;
	}
	

	public WebElement getClientCodeModalInput(){
		return ccModalInput;
	}
	
	public WebElement getClientCodeOK(){
		return clientCodeOK;
	}
	
}
