package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * 
 * @author hipatel
 *
 */
		

public class LawyerportPageFactory {

	@FindBy (how = How.ID, using = "signin-button")
	private WebElement loginButton;
	
	@FindBy (how = How.ID, using = "signin-button")
	private WebElement signinButtonQA;
	
	@FindBy(how = How.ID, using = "username")
	private WebElement username;

	@FindBy(how = How.ID, using = "password")
	private WebElement password;


	@FindBy(how = How.ID, using = "clientCode")
	private WebElement clientCode;

	
	@FindBy(how = How.ID, using = "resetButton")
	private WebElement clearButton;
	
	@FindBy(how = How.ID, using = "submitButton")
	private WebElement signInButton;
	
	@FindBy(how = How.CLASS_NAME, using = "login-forgot-password")
	private WebElement forgotPassword;
	
	@FindBy(how = How.ID, using = "my-profile-admin")
	private WebElement adminProfile;
	
	@FindBy(how = How.ID, using = "button-welcome-link")
	private WebElement welcomeLinkButton;
	
	@FindBy (how = How.ID, using ="my-profile-edit")
	private WebElement editProfile;
	
	@FindBy(how = How.ID, using = "my-profile-logout")
	private WebElement logout;
	
	/**
	 * @return the loginButton
	 */
	public WebElement getLoginButton() {
		return loginButton;
	}

	
	/**
	 * @return the loginButton
	 */
	public WebElement getQALoginButton() {
		return signinButtonQA;
	}
	/**
	 * @return the username
	 */
	public WebElement getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public WebElement getPassword() {
		return password;
	}
	
	public WebElement getClientCode() {
		return clientCode;
	}

	/**
	 * @return the signIn
	 */
	public WebElement getSignInButton() {
		return signInButton;
	}

	/**
	 * @return the clear
	 */
	public WebElement getClearButton() {
		return clearButton;
	}

	/**
	 * @return the forgotPassword
	 */
	public WebElement getForgotPassword() {
		return forgotPassword;
	}

	/**
	 * @return the adminProfile
	 */
	public WebElement getAdminProfile() {
		return adminProfile;
	}
	/**
	 * edit profile
	 * @return
	 */
	public WebElement getEditProfile(){
		return editProfile;
	}
	/**
	 * @return the welcomeLinkButton
	 */
	public WebElement getWelcomeLinkButton() {
		return welcomeLinkButton;
	}
	
	public WebElement getLogoutButton(){
		return logout;
	}
}

