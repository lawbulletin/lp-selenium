package pagefactories;




import lawyerportdominion.AbstractSprint;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class AppellateSummariesPageFactory extends AbstractSprint{
	
	@FindBy(how = How.XPATH, using ="//li[@id='navRes']")
	private WebElement legalResearch;
	
	@FindBy(how = How.XPATH, using ="//a[contains(@id, 'subtab-app-summaries')]")
	private WebElement appellateSummaries;
	
	@FindBy (how = How.XPATH, using ="//input[contains(@class,'yui3-tokeninput-input')]")
	private WebElement judge; 
	
	
	@FindBy (how = How.ID, using ="form-field-2")
	private WebElement startDate;
	
	@FindBy (how = How.ID, using ="form-field-3")
	private WebElement endDate;
	
	@FindBy (id ="app-summaries-run-advanced-search")
	private WebElement searchButton;
	
	@FindBy (how = How.ID, using ="app-summaries-reset-advanced-search-bottom")
	private WebElement resetButton;
	
	@FindBy (how = How.ID, using ="form-field-4")
	private WebElement keyword;
	
	@FindBy(id = "app-summaries")
	private WebElement appTab;
	
	public WebElement getAppTab(){
		return appTab;
	}
	
//	public WebElement navAppellateSummaries() {
//		legalResearch.click();
//		AbstractSprint.forceWait(2);
//		getAppTab().click();
//		AbstractSprint.forceWait(2);
//		return appellateSummaries;
//	}
	
	public WebElement getJudge() {
		return judge;
	}
	
	public WebElement getStartDate(){
		return startDate;
	}
	
	public WebElement getEndDate() {
		return endDate;
	}
	
	public WebElement getSearchButton() {
		return searchButton;
	}
	
	public WebElement getResetButton() {
		return resetButton;
	}
	
	public WebElement getKeyword() {
		return keyword;
	}
	
	@FindBy(how = How.XPATH, using = "(//ul/li/input)[2]")
	private WebElement court;
	
	public WebElement getCourt(){
		return court;
	}
	
	@FindBy(how = How.CLASS_NAME, using ="yui3-aclist-item yui3-aclist-item")
	private WebElement option;
	
	public WebElement getOption(){
		return option;
	}
	
	//for use with appellate summary result page in regression test only
	@FindBy(id = "article-link-100026646")
	private WebElement article1;
	
	@FindBy(id = "article-link-22273531")
	private WebElement article2;
	
	public WebElement getHoganArticle1(){
		return article1;
	}
	
	public WebElement getHoganArticle2(){
		return article2;
	}
	
	@FindBy(id = "article-link-10003700")
	private WebElement courtArticle1;
	
	@FindBy(id = "article-link-22813349")
	private WebElement courtArticle2;
	
	public WebElement getCourtArticle1(){
		return courtArticle1;
	}
	
	public WebElement getCourtArticle2(){
		return courtArticle2;
	}
}
