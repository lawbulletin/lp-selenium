package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CourtDocketResultFactory {
	
	@FindBy(how = How.XPATH, using = "(//a)[17]")
	private WebElement firstResult;
	
	public WebElement getCDFirstResult(){
		return firstResult;
	}
@FindBy(id = "subTabContent2")	
private WebElement noResultsBar;

public WebElement getNoResultsBar(){
	return noResultsBar;
}

@FindBy(how = How.ID, using = "cd-back-form") 
 private WebElement  searchFormButton;

 public WebElement getSearchFormButton(){ return searchFormButton;}

@FindBy(how = How.ID, using = "resultCount") 
 private WebElement  resultCount;

 public WebElement getResultCount(){ return resultCount;}

@FindBy(how = How.ID, using = "first") 
 private WebElement  firstNavigationBar;

 public WebElement getFirstNavigationBar(){ return firstNavigationBar;}

@FindBy(how = How.ID, using = "next") 
 private WebElement  nextNavigationBar;

 public WebElement getNextNavigationBar(){ return nextNavigationBar;}

@FindBy(how = How.ID, using = "last") 
 private WebElement  lastNavigationBar;

 public WebElement getLastNavigationBar(){ return lastNavigationBar;}
}
 
