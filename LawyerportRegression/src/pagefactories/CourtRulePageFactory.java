package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CourtRulePageFactory {
	
	@FindBy (how = How.XPATH, using = "//li[@id='navRes']")
	private WebElement legalResearch; 

	@FindBy (how = How.XPATH, using = "//a[contains(@id, 'subtab-court-rules')]")
	private WebElement courtRule; 
		

	@FindBy(how = How.ID, using="form-field-0")
	private WebElement keywords; 
	
	@FindBy(how = How.ID, using="form-field-1")
	private List<WebElement> jursidiction; 
	
	@FindBy(how = How.ID, using="form-field-2")
	private WebElement ruleNumber; 
	
	@FindBy(how = How.ID, using="form-field-3")
	private WebElement ruleTitle; 
	
	@FindBy(how = How.ID, using="court-rules-run-advanced-search")
	private WebElement search; 
	
	@FindBy(how = How.ID, using="court-rules-reset-advanced-search-bottom")
	private WebElement reset; 
	
	public WebElement navCourtRule() {
		legalResearch.click();
		return courtRule;
	}
	
	public WebElement getKeywords() {
		return keywords;
	}
	
	public List<WebElement> selectJurisdiction() {
		return jursidiction;
	}
	
	public WebElement getRuleNumber() {
		return ruleNumber;
	}
	
	public WebElement getRuleTitle() {
		return ruleTitle;
	}
	
	public WebElement getSearchButton() {
		return search;
	}
	
	public WebElement getRestButton() {
		return reset;
	}
}
