package pagefactories;



import lawyerportdominion.AbstractSprint;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ArticlePageFactory extends AbstractSprint{

	@FindBy(how = How.ID, using = "back-results") 
	 private WebElement  backResults;

	 public WebElement getBackResults(){ return backResults;}

	@FindBy(how = How.ID, using = "back-form") 
	 private WebElement  backForm;

	 public WebElement getBackForm(){ return backForm;}

	@FindBy(how = How.CLASS_NAME, using = "article-header-row") 
	 private WebElement  articleHeader;

	 public WebElement getArticleHeader(){ return articleHeader;}

	@FindBy(how = How.ID, using = "article-view-text") 
	 private WebElement  articleViewText;

	 public WebElement getArticleViewText(){ return articleViewText;}

	@FindBy(how = How.CLASS_NAME, using = "pub-name-display") 
	 private WebElement  publicationType;

	 public WebElement getPublicationType(){ return publicationType;}

	@FindBy(how = How.ID, using = "article-description") 
	 private WebElement  articleParagraphs;

	 public WebElement getArticleParagraphs(){ return articleParagraphs;}

	@FindBy(how = How.ID, using = "article-footer-areas-text") 
	 private WebElement  articleFooter;

	 public WebElement getArticleFooter(){ return articleFooter;}

	@FindBy(how = How.XPATH, using = "//p[contains(@class, 'article-footer-areas-text')]/a") 
	 private WebElement  practiceAreasDiscussed;

	 public WebElement getPracticeAreasDiscussed(){ return practiceAreasDiscussed;}
}
