package lawyerportdominion;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pagefactories.CDPageFactory;
import pagefactories.CourtDocketResultFactory;
import pagefactories.LPMainPageFactory;
import pagefactories.LandingPageFactory;
import pagefactories.LoginPageFactory;
import seleniumwebdriver.SeleniumDriver;
import seleniumwebdriver.SeleniumDriver.BrowserType;

import com.google.common.base.Function;


/**
 * 
 * @author hipatel + bgalatzer-levy
 *
 */

public class AbstractSprint extends ReuseableMethod{
	

	
	public static Boolean condition1;
	public static Boolean condition2;
	public static Boolean condition3;
	public static WebElement e;
	public static WebDriverWait tock;
	
	public static LoginPageFactory log = PFCaller.loginFactory();
	public static LandingPageFactory land = PFCaller.landingFactory();
	public static LPMainPageFactory home = PFCaller.lpMainFactory();
	public static CourtDocketResultFactory cdResults = PFCaller.cdResults();
	public static CDPageFactory cd = PFCaller.cdFactory();
	
	@BeforeClass
	public static void setUp() {
	
		
		 SeleniumDriver.setDriver(BrowserType.FF); 
		
		LoginCredentials.inQA(); // which Environment? 
		
		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
	
		SeleniumDriver.getDriver().manage().window().maximize();
	//	SeleniumDriver.getDriver().manage().deleteAllCookies();
		
	//	SeleniumDriver.getDriver().findElement(By.linkText("Sign In")).click();
		e = getSignIn();
		e.click();
		forceWait(2);
		SeleniumDriver.getDriver().findElement(By.id("username")).sendKeys(LoginCredentials.getUsername());
		SeleniumDriver.getDriver().findElement(By.id("password")).sendKeys(LoginCredentials.getPassword());//.getPassword().sendKeys(LoginCredentials.getPassword());
		SeleniumDriver.getDriver().findElement(By.id("clientCode")).sendKeys(LoginCredentials.getClient());
		//log.getClient().sendKeys(LoginCredentials.getClient()); //Not available in beta, manually comment this line for beta testing
	//	log.getSubmitButton().click();
		SeleniumDriver.getDriver().findElement(By.id("submitButton")).click();
		
		
	}
	
	@Before
	public void startTest(){
	
		forceWait(1);
	}
	
	public static WebElement getSignIn(){
		WebElement signin = SeleniumDriver.getDriver().findElement(By.className("signin"));
		Boolean b = signin.isEnabled();
		if(!b){forceWait(5);
		getSignIn();}
		
		return signin;
	}
	
	public static void logIn(){
		
		int j=0;
		
		if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://wwwqa.lawyerport.com/")){
			LoginCredentials.inQA();
			j =1;
		}
		else if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://beta1.lawyerport.com/")){
			LoginCredentials.inProd();
			j =2;
		}
		else if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://wwwdev.lawyerport.com/")){
			LoginCredentials.inDev();
			j =3;
		}
		
		System.out.println("Logging in as : "+LoginCredentials.getUsername());
		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
		forceWait(1);	
		
		land.getsignInButton().click();
		forceWait(1);	
		
			log.getUserName().sendKeys(LoginCredentials.getUsername());
			log.getPassword().sendKeys(LoginCredentials.getPassword());
			log.getClient().sendKeys(LoginCredentials.getClient());
				
				if(j==2){log.getSubmitButton().click();}
				else{log.getResetButton().click();}
					
				forceWait(1);
				if(SeleniumDriver.getDriver().getCurrentUrl().contains("redirect")){
					System.err.println("Log-in Redicrted to wrong page (Successful log-in page) attempting to resolve error");
					home.headerLogo().click();
					forceWait(1);
				}
	
				if(SeleniumDriver.getDriver().findElement(By.id("signin-button")).isDisplayed()){
					System.err.println("Log-in Redicrted to wrong page (No-login homepage) attempting to resolve error");
					SeleniumDriver.getDriver().findElement(By.id("signin-button")).click();
					forceWait(1);
				}
			forceWait(1);
	}
	
	public static String getLoginURL(){
		String login;
		
		login = (LoginCredentials.getWebsite() + "lb-cas/login");
		return login;
	}
	
	@After
	public void endTest(){
		SeleniumDriver.getDriver().findElement(By.id("navHome")).click();
		System.out.println("----------------------------");
		System.out.println();
		forceWait(1);
	}
	
	
	@AfterClass
	public static void tearDown(){
	
		if(SeleniumDriver.getDriver() != null){
//		if(home.getHomeIcon().isDisplayed()){
//			SeleniumDriver.getDriver().close();
//			home.getWelcomeLink().click();
//			forceWait(1);
//			home.getLogoutLink().click();
//			SeleniumDriver.getDriver().manage().deleteAllCookies();
//			forceWait(2);
//			SeleniumDriver.getDriver().quit();
//		   }
		
		SeleniumDriver.getDriver().manage().deleteAllCookies();
		forceWait(2);
		SeleniumDriver.getDriver().close();

        try
        {
            Thread.sleep(5000);
            SeleniumDriver.getDriver().quit();
        }
        catch(Exception e)
        {
        }
	}
	
	}
	
	@Rule
	public ErrorCollector collector;
	
	/**
	 * 
	 * @param s = Element ID
	 * @return = WebElement identified by id = s
	 */
	public  WebElement LPElement(String s){
		if(ExpectedConditions.presenceOfElementLocated(By.id(s)) != null);
		e = SeleniumDriver.getDriver().findElement(By.id(s));
		
		return e;
	}
	
	/**
	 * returns number of search results from a court docket search, if any
	 * @throws Exception
	 */
	public  void cdSearchResults() throws Exception{
		
		if(isElementPresent(By.id("resultCount")) == true){
			System.out.print("Search Successful: ");
			String result = waitForElement(By.id("resultCount")).getText().toString();
			result = result.substring(25);
		
			System.out.println(result +" results returned");
			
		forceWait(1);
		main.getLegalResearchLink().click();
		forceWait(2);
//		cd.getSearchReset().click();
//		forceWait(1);
		}
		
		else if(cdResults.getNoResultsBar().getText().toString().contains("No results were found")){
			System.out.println("No Results Found (search properly returned no results");
			forceWait(1);
			main.getLegalResearchLink().click();
			forceWait(2);
			cd.getSearchReset().click();
			forceWait(1);
		}
		else {System.err.println("Search Failed");
		forceWait(1);
		main.getLegalResearchLink().click();
		forceWait(2);
		cd.getSearchReset().click();
		forceWait(1);}
		}
	
	
	public static String getString(WebElement e){
		String temp = e.getText().toString().trim();
		return temp;
	}
	
	 public static HashMap<String, Boolean> stringElementToMap(Map<String, Boolean> map, WebElement e, String text){
	    	map.put(text, (getString(e)).contains(text));
	    	return (HashMap<String, Boolean>) map;
	    }
	    
	  public static HashMap<String, Boolean> enabledElementToMap(Map<String, Boolean> map, WebElement e, String name){
	    	map.put(name, (e.isEnabled()));
	    	return (HashMap<String, Boolean>) map;
	  }
	  
	  public static HashMap<String, Boolean> stringToMap(Map<String, Boolean> map, String e, String text){
	    	map.put(text, e.contains(text));
	    	return (HashMap<String, Boolean>) map;
	    }
	  /**
	   * stringElementToMap(labels, org.getPeopleTab(), "PEOPLE");
    	
    	
    	for(Map.Entry<String, Boolean> entry : labels.entrySet()){
    		if(entry.getValue()){System.out.println(entry.getKey() + " text displayed");}
    		if(!entry.getValue()){System.err.println("Missing Label Text: " + entry.getKey());
    			condition1 = false;
    		
    		}}
    	tfTest();
	   * @return
	   * @throws Exception
	   */
	
	public static int cdSearchResult() throws Exception{
		
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) != null)
			System.out.print("Search Successful ");
			String result = waitForElement(By.id("resultCount")).getText().toString();
			result = result.substring(25).trim();
			int results = Integer.parseInt(result); 
			System.out.println(results);
			System.out.println(" results returned");
			forceWait(1);
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) == null)	
			if(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(.,'Return to the Search Form')]")) != null)
			System.out.println("No Results Found");
			else System.out.println("Search failed");
		
		return results;
	}
	//waits for an element  to appear on page, the default time is 30 seconds.
	//default time to wait for element is 30 seconds.
	public static WebElement waitForElement(final By Locator) throws Exception {
		 WebDriverWait wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);

	    WebElement function = wait.until(new Function<WebDriver, WebElement>() {
	        public WebElement apply(WebDriver driver) {
	            return driver.findElement(Locator);
	        }
	    });

	    return  function;
	}
	
	//the purpose of this methods is to randomly select one of the options to do a search for organization in directory page
		public static String orgKeyWords(){
			String kewWords[] = {"ASSOCIATION", "CIVIC", "CORPORATION", "COURT", "GOVERNMENT", "LAWFIRM", "OTHER", "SCHOOL" }; 
			Random randWords = new Random(); 
			int select = randWords.nextInt(kewWords.length);
			System.out.println("Organization Type: " + kewWords[select] );
			return kewWords[select];
		}
		
		
		
		// using random function to select random name from the lastName Array 
//		public static String randomLastName() { 
//			String lastName[] = {"smith", "williams", "johnson", "jones"};
//			Random randomLName = new Random(); 
//			int select = randomLName.nextInt(lastName.length);
//			System.out.println("Randomly selected last name: " + lastName[select]);
//			return lastName[select];  
//		}
		
			
		
		
		
		/* A method that emails  when an error occurs in performing
		 * Copied from Alan's arm test
		 */
/*		protected static boolean email(String case_content){
			// SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
	        String from = "hipatel@lbpc.com";
	        // SUBSTITUTE YOUR ISP'S MAIL SERVER HERE!!!
	        String host = "lb-sunsmtp";
	        // Create properties, get Session
	        Properties props = new Properties();
	        // If using static Transport.send(),
	        // need to specify which host to send it to
	        props.put("mail.smtp.host", host);
	        // To see what is going on behind the scene
	        //props.put("mail.debug", "true");
	        Session session = Session.getInstance(props);
	        try {
	            //Instantiate a message
	            Message msg = new MimeMessage(session);
	            
	            //Set message attributes
	            msg.setFrom(new InternetAddress(from));
	           
	            String [] to = {"hipatel@lbpc.com", "patelhiren1590@gmail.com"};
	        	InternetAddress[] address = new InternetAddress[to.length];
	            for (int i = 0; i < to.length; i++){
	            	address [i] = new InternetAddress(to[i]);
	            }
	        	msg.setRecipients(Message.RecipientType.TO, address);
	        	msg.setSubject("Search Term Do Not Match ");
	            
	            msg.setSentDate(new Date());
	            // Set message content
	            msg.setText(case_content);
	            //Send the message
	            Transport.send(msg);
	        }
	        catch (MessagingException mex) {
	            // Prints all nested (chained) exceptions as well
	            mex.printStackTrace();
	        }
	        return true;
		}	*/
		
		public void tfTest() throws Exception{
		//	String result = null;
			if(condition1){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}
			
//			if(result.contains("Pass"))
//				printColor("Lawyerport conditional test" + ": "+result, 34);
//			else if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//			else
//				System.err.println("Error in Test");
//			try {result.contains("Pass");
//				System.out.println("Lawyerport conditional test" + ": "+result);
//			} catch (Throwable t){
//				if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//				else System.out.println("Error in Test");
//			}
			System.out.println("------------------");

		}
		
		public void tfTest2() throws Exception{
			if(condition2){System.out.println("Lawyerport conditional test: Pass");
			}
			if(!condition2){System.err.println("Lawyerport conditional test: Fail");}
			
//			String result = null;
//			if(condition2){result="Pass";}else{result="Fail";}
//			
//			if(result.contains("Pass"))
//				printColor("Lawyerport conditional test" + ": "+result,34);
//			else if(result.contains("Fail"))
//				System.err.println("Lawyerport conditional test" + ": "+result);
//			else
//				System.err.println("Error in Test");
			System.out.println("------------------");

		}
		
		public void tfTest3() throws Exception{
			if(condition1 && condition2){System.out.println("Lawyerport conditional test: Pass");
			}
			if(!condition1 || !condition2){System.err.println("Lawyerport conditional test: Fail");
				if(!condition1){System.err.print(" on Condition 1");}
				if(!condition2){System.err.print(" on Condition 2");}
				}
			}
		
		
		public void tfTest4() throws Exception{
			if(condition1 && condition2 && condition3){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}
			System.out.println("------------------");
		}
		
		public void deleteCookies(){
			SeleniumDriver.getDriver().manage().deleteAllCookies();
		}
		
		@Deprecated
		public void write(String s){
			System.out.println(s);
		}
		
		/**
		 * 
		 * @param type: 1 -id, 2 -class, 3 -xpath, 4 -name, 5 -css
		 * 				6 - link text 7 -id or name 8 -partial link text
		 * 				9 -tag name
		 * @param locater - PageFactory unique locater
		 * @param name - pageFactory element name
		 */
	public static void PFWriter(int type, String locater, String name){
			String[] returns = new String[2];
			String Name = name.substring(0).toUpperCase() + name.substring(1, name.length());
			String how = "@FindBy(how = How.";
			String using = ", using = \"" + locater +  "\"); " + "\n  private WebElement " +name +"\"";
			String get = "public WebElement get";
			String give = "(){ return ";
			String dingbat = ";}";
			String findBy = null;
			String caller = get + Name + give + name + dingbat;
			String getter = null;
			switch(type){
				case 1: 
					findBy = "@FindBy(id = \"" + locater + "\"); private WebElement " + name;
					getter = caller;
					break;
				case 2:
					findBy = how + "CLASS_NAME" + using;
					getter = caller;
					break;
				case 3:
					findBy = how + "XPATH" +using;
					getter = caller;
					break;
				case 4:
					findBy = "@FindBy(name = " + locater + "\"); private WebElement " + name;
					getter = caller;
					break;
				case 5:
					findBy = how + "CSS" +using;
					getter = caller;
					break;
				case 6:
					findBy = how + "LINK_TEXT"+using;
					getter = caller;
					break;
				case 7:
					findBy = how + "ID_OR_NAME"+using;
					getter = caller;
					break;
				case 8:
					findBy = how + "PARTIAL_LINK_TEXT"+using;
					getter = caller;
					break;
				case 9:
					findBy = how + "TAG_NAME"+using;
					getter = caller;
					break;	
			}
			returns[0] = findBy;
			returns[1] = getter;
			
			System.out.println(findBy);
			System.out.println(getter);
			
		}
		
}


	
		
	
		
		
