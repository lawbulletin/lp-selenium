package lawyerportdominion;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

public class RoboTester {
	public Robot robot;
	public WebElement e;
	
	public void lClick(){
		robot.setAutoDelay(500);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}
	
	public void rPressAndHold(){
		robot.setAutoDelay(500);
		robot.mousePress(InputEvent.BUTTON2_MASK);
		//robot.mouseRelease(InputEvent.BUTTON2_MASK);
	}
	
	public void rClick(){
		robot.setAutoDelay(500);
		robot.mousePress(InputEvent.BUTTON2_MASK);
		robot.mouseRelease(InputEvent.BUTTON2_MASK);
	}
	
	public void down(){
		robot.setAutoDelay(1000);
		robot.keyPress(KeyEvent.VK_DOWN);
	    robot.keyRelease(KeyEvent.VK_DOWN);
		
	}
	
	public void roboHover(WebElement e){
		robot.setAutoDelay(500);
		Point p = e.getLocation();
		int x = p.getX();
		int y = p.getY();
		robot.mouseMove(x,y);
		
	}
	
	public void roboSelect(WebElement e){
		robot.setAutoDelay(500);
		roboHover(e);
		lClick();
	}
	
	public void roboDoubleClick(WebElement e){
		robot.setAutoDelay(500);
		roboSelect(e);
		lClick();
	}
	
	public void roboSelectAndEnterText(WebElement e, String txt){
		roboSelect(e);
		e.sendKeys(txt);
	}
	
	public static void firstDown(){
	try {
		Robot robo = new Robot();

        robo.keyPress(KeyEvent.VK_DOWN);
        robo.keyRelease(KeyEvent.VK_DOWN);
        robo.keyPress(KeyEvent.VK_ENTER);
        robo.keyRelease(KeyEvent.VK_ENTER);
        
} catch (AWTException e) {
        e.printStackTrace();
}
	}
	
	
}

