package lawyerportdominion;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pagefactories.LPMainPageFactory;
import pagefactories.LandingPageFactory;
import pagefactories.LoginPageFactory;
import seleniumwebdriver.SeleniumDriver;
import seleniumwebdriver.SeleniumDriver.BrowserType;


/**
 * 
 * @author hipatel + bgalatzer-levy
 *
 * AbstractTest is Used for Tests that Require Users not to be signed in to Lawyerport
 * AbstractSprint should be used in all other cases
 */


public class AbstractPreLoginSprint extends ReuseableMethod{

	public static Boolean condition1;
	public static Boolean condition2;
	public static Boolean condition3;
	public static WebElement e;
	public static WebDriverWait tock;
	public static LoginPageFactory log = PFCaller.loginFactory();
	public static LandingPageFactory land = PFCaller.landingFactory();
	public static LPMainPageFactory home = PFCaller.lpMainFactory();
	
	@BeforeClass
	public static void setUp() {

		SeleniumDriver.setDriver(BrowserType.FF); 
		forceWait(2);
		
		LoginCredentials.inProd(); // which Environment? 
		
		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
		
		SeleniumDriver.getDriver().manage().window().maximize();

	}
	
	@Before
	public void startUp(){
		forceWait(1);
	}
	
	@After
	public void endTest(){
		forceWait(1);
		System.out.println("-----------------------------------");
		System.out.println();
	}
	
	@AfterClass
	public static void tearDown(){

		forceWait(2);
		SeleniumDriver.getDriver().manage().deleteAllCookies();
		SeleniumDriver.getDriver().quit();

	}
	
	
	public static void logIn(){
		
		int j=0;
		
		if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://wwwqa.lawyerport.com/")){
			LoginCredentials.inQA();
			j =1;
		}
		else if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://beta1.lawyerport.com/")){
			LoginCredentials.inProd();
			j =2;
		}
		else if(SeleniumDriver.getDriver().getCurrentUrl().contains("https://wwwdev.lawyerport.com/")){
			LoginCredentials.inDev();
			j =3;
		}
		
		System.out.println("Logging in as : "+LoginCredentials.getUsername());
		SeleniumDriver.getDriver().get(LoginCredentials.getWebsite());
		forceWait(1);	
		
		land.getsignInButton().click();
		forceWait(1);	
		
			log.getUserName().sendKeys(LoginCredentials.getUsername());
			log.getPassword().sendKeys(LoginCredentials.getPassword());
			log.getClient().sendKeys(LoginCredentials.getClient());
				
				if(j==2){log.getSubmitButton().click();}
				else{log.getResetButton().click();}
					
				forceWait(1);
				if(SeleniumDriver.getDriver().getCurrentUrl().contains("redirect")){
					System.err.println("Log-in Redicrted to wrong page (Successful log-in page) attempting to resolve error");
					home.headerLogo().click();
					forceWait(1);
				}
	
				if(SeleniumDriver.getDriver().findElement(By.id("signin-button")).isDisplayed()){
					System.err.println("Log-in Redicrted to wrong page (No-login homepage) attempting to resolve error");
					SeleniumDriver.getDriver().findElement(By.id("signin-button")).click();
					forceWait(1);
				}
			forceWait(1);
	}
	
	public static String getLoginURL(){
		String login;
		
		login = (LoginCredentials.getWebsite() + "lb-cas/login");
		return login;
	}

	public static void forceWait(int sleepTime){
		
	//	System.out.println("human wait of: " + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
	
		/**
	 * returns number of search results from a court docket search, if any
	 * @throws Exception
	 */
	public static void cdSearchResults() throws Exception{
		
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) != null)
			System.out.print("Search Successful: ");
			String result = waitForElement(By.id("resultCount")).getText().toString();
			result = result.substring(25);
		
			System.out.println(result);
			System.out.print(" results returned");
		//	stupidHumanWait(1);
		if(ExpectedConditions.presenceOfElementLocated(By.id("subTabContent2")) != null)	
			if(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(.,'Return to the Search Form')]")) != null)
			System.out.println("No Results Found");
			else System.out.println("Search failed");
		
		
	}
	
	public static int cdSearchResult() throws Exception{
		
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) != null)
			System.out.print("Search Successful ");
			String result = waitForElement(By.id("resultCount")).getText().toString();
			result = result.substring(25).trim();
			int results = Integer.parseInt(result); 
			System.out.println(results);
			System.out.println(" results returned");
		//	stupidHumanWait(1);
		if(ExpectedConditions.presenceOfElementLocated(By.id("resultCount")) == null)	
			if(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(.,'Return to the Search Form')]")) != null)
			System.out.println("No Results Found");
			else System.out.println("Search failed");
		
		return results;
	}
	
	// using random function to select random name from the firstName Array
	public void dirSearch() throws Exception {
				waitForElement(By.id("form-field-0")).clear();
				waitForElement(By.id("form-field-0")).sendKeys(randomName());
				waitForElement(By.id("form-field-4")).clear();
				waitForElement(By.id("form-field-4")).sendKeys(randomLastName());
				waitForElement(By.id("dir-run-advanced-search")).click();
				clickResult(); 
	}
	
	// After a search is perform this method is to called to click on search results. 
	//Directory Page Only
	
	
	//the purpose of this methods is to randomly select one of the options to do a search for organization in directory page
	public static String orgKeyWords(){
			String kewWords[] = {"ASSOCIATION", "CIVIC", "CORPORATION", "COURT", "GOVERNMENT", "LAWFIRM", "OTHER", "SCHOOL" }; 
			Random randWords = new Random(); 
			int select = randWords.nextInt(kewWords.length);
			System.out.println("Organization Type: " + kewWords[select] );
			return kewWords[select];
		}
		
	
		
		//Randomly select from element list
		public static int RandomSelectInt(List<WebElement> waitForElements ){
		    int options = waitForElements.size();
		    Random random = new Random();
		    //System.out.println("index term " + random.nextInt(options));
		    return random.nextInt(options);
		}
		
		// checks to see if an element is displayed or not. 
		public static boolean isElementPresent(By locator) throws Exception, IOException,SQLException {
			try {
		    	waitForElement(locator).isDisplayed();
		    	//System.out.println("element exists");
		    	System.out.println("element exists");
		    	return true;
		    }catch (Throwable t ){
		    	String s = locator.toString();
		    	System.out.println(s + " element does not exists"); 	
				return false;
			}
		}
		
		//checks to see if edit button is visible or not. 
		public boolean isEditButPresent() throws Exception {
			try {
		    	if (isElementPresent(By.xpath("(//div[contains(@class, 'hidden')])[2]"))) { 
					System.out.println("Edit option is disable");
				} else {
					waitForElement(By.id("edit-profile")).click();
					System.out.println("clicking on edit profile");
				}
		    }catch (Throwable t){
		    	System.out.println("Cannot find Edit Button");
				return false;
			}
		    return true;
		}
		
		/**
		 * from http://santoshsarmajv.blogspot.in/ <br>
		 * use this website to verify if date are output correctly http://www.timeanddate.com/date/dateadd.html <br>
		 * 
		 * @param period - The date can be changed by  inserting navigate or positive number. Negative number = past/later date
		 * and positive number = future date. if today date is 11/19/2013, calling getDate(5, "MM/dd/yyyy") 
		 * is 5 days later from today so it will get 11/24/2013. enter -5 will get past date, so it will be 11/14/2013 <br>
		 * 
		 * @param format - date format <br>
		 * 
		 * @return 
		 * getDate(period, format); 
		 * 
		 */
		public String getDate(int period,String format){
		     Calendar currentDate = Calendar.getInstance();
		     SimpleDateFormat formatter= new SimpleDateFormat(format);
		     currentDate.add(Calendar.DAY_OF_MONTH, period);
		     String date = formatter.format(currentDate.getTime());
		     return date;
		}
	
		//finds internal links in an article, clicks on each, and returns to original article after each click
		public void articleSubLinks( ) throws Exception {
			List<WebElement> dirSubLink = SeleniumDriver.getDriver().findElements(By.xpath("//div[contains(@id,'article-description')]/p/a")); //list of people profile 
			//dirSubLink = driver.findElements(By.xpath("//div[contains(@id,'article-description')]/a"));
			List<String> linkTitles = new ArrayList<String>();
			for (WebElement eachlink : dirSubLink){ // get links text and save into list 
				linkTitles.add(eachlink.getText()); 
			}
			for (String eachLink : linkTitles) { // 
				List<WebElement> templinks = SeleniumDriver.getDriver().findElements(By.tagName("a")); // 
				for (WebElement e : templinks) {
					if (eachLink.equals(e.getText())) {
						if (e.isDisplayed() && e.isEnabled()) {
							System.out.println("Found a link. Clicking " + e.getText());						
							e.click();
							Thread.sleep(5000);	
							SeleniumDriver.getDriver().navigate().back();
							break;
						}
					}
				}
			}
		}
		

		/**
		 * 
		 * @param category 	 JVR Search Category i.e Injury/Disease </p>
		 * @param subCategory   JVR Sub Category appears after category is selected i.e Injury/Disease > J-L - 32 </p>
		 * @param selectALL  	Boolean, True means it will select all of the Terms after Category adn SubCateogry are selected. 
		 * 						False means do not select all of the terms. NOTE, if False is selected you have to enter index number </p>
		 * @param index 		uses integer defined by user to randomly selects terms 
		 * @throws Exception
		 */
		
		public void jVRCategories(String category, String subCategory, boolean selectALL,  String oneTerm) throws Exception {
			waitForElement(By.xpath("//a[contains(@id,'add-jv-terms')]")).click(); // open 

			if (isElementPresent(By.className("modal-content"))){
				 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-left')]/ul"));
				 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
				    for(WebElement option : categoryOptions){
				        if(option.getText().equals(category)) {
				            option.click();
				            break;
				        }
				    }
			    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-mid')]/ul"));
				List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));

			    	for(WebElement option : categorySuboptions){
			    		if(option.getText().equals(subCategory)) {
			    			option.click();
			    			break;
			    		}
			    	}
			    	
			    	if (selectALL) {
			    		//click select all 
			    		waitForElement(By.id("jv-term-select-select-all")).click();
			    	} else {
			    			WebElement selectTerms = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'jv-term-select-edit-right')]/ul"));
							List<WebElement> terms = selectTerms.findElements(By.tagName("li"));
							for(WebElement option : terms){
						        if(option.getText().equals(oneTerm)) {
						            option.click();
						            break;
						        }
							}
			    		
			    	}
			}
			waitForElement(By.id("form-button-Save")).click();
		}
		
		//checks to see if element is clickable
		public static boolean isElementEnable(By locator) throws Exception, IOException,SQLException { 
			try {
		    	waitForElement(locator).isEnabled();
		    	//System.out.println("element exists");
		    	System.out.println("element exists");
		    	return true;
		    }catch (Throwable t ){
		    	System.out.println("element does not exists");  	
		    	return false;
			}
		}
		
		
		/**
		 * 
		 * @param category
		 * @param selectALL
		 * @param subCategory if selectall-false then subCategorgy must be defined, else null
		 * @throws Exception
		 */
		public void selectPraticeArea(String category, boolean selectALL, String subCategory ) throws Exception {
			if (SeleniumDriver.getDriver().findElement(By.className("modal-content")).isDisplayed()){
				 WebElement selectCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'aop-edit-left')]/ul"));
				 List<WebElement> categoryOptions = selectCategory.findElements(By.tagName("li"));
				    for(WebElement option : categoryOptions){
				        if(option.getText().equals(category)) {
				            option.click();
				            break;
				        }
				    }
					if (selectALL) {
			    		//click select all 
			    		SeleniumDriver.getDriver().findElement(By.xpath("(//div[contains(@class, 'aop-edit-label')]/a)[1]")).click();
			    	} else {
					    WebElement selectSubCategory = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@id, 'aop-edit-mid')]/ul"));
						List<WebElement> categorySuboptions = selectSubCategory.findElements(By.tagName("li"));

					    	for(WebElement option : categorySuboptions){
					    		if(option.getText().equals(subCategory)) {
					    			option.click();
					    			break;
					    		}
					    	}
					    	
			    	}
			}
			SeleniumDriver.getDriver().findElement(By.id("form-button-Save")).click();
		}
		
		public void selectTypeAheadValue(WebElement locator, String text) {
			WebElement getTypeAheadField = locator; 
			getTypeAheadField.clear();
			getTypeAheadField.sendKeys(text);
			forceWait(1);
			
			WebElement getActiveList = SeleniumDriver.getDriver().findElement(By.className("yui3-aclist-list")); 
			List<WebElement> select = getActiveList.findElements(By.tagName("li")); 
				for (WebElement option : select){
					if (option.getText().contains(text)){
						option.click();
						break;
					}
				}	
		}
		
		public void selectStreamingContainer(WebElement locator, boolean chicagoDaily, boolean chicagoLawyer, boolean clickMore){
			WebElement steramingCOntainer = locator; 
			
		}
		
		public void selectAndClickResult(String clickResultText) {
			WebElement searchRow = SeleniumDriver.getDriver().findElement(By.xpath("//div[contains(@class,'search-row row-detail')]"));
			WebElement test2 = searchRow.findElement(By.className("search-detail-container")); 
			List<WebElement> clickName = test2.findElements(By.xpath("//a[contains(@class,'search-click-through')]"));
			for (WebElement option : clickName){
				if (option.getText().contains(clickResultText)){
					option.click();
					break;
			}}}
		
		public void profileLinks( ) throws Exception {
			WebElement findLinks = waitForElement(By.id("profile-links"));
			WebElement profileLinks = findLinks.findElement(By.tagName("ul"));
			List<WebElement> activeLinks = profileLinks.findElements(By.className("clickable-nav"));
			List<String> linkTitles = new ArrayList<String>();
			for (WebElement clickActiveLink : activeLinks ){
				linkTitles.add(clickActiveLink.getText());
			}
			
			for (String activeLink : linkTitles){
				List<WebElement> tempLinks = SeleniumDriver.getDriver().findElements(By.className("clickable-nav"));
				 
				for (WebElement e : tempLinks){
					if (activeLink.equals(e.getText())) {
						if(e.isDisplayed() && e.isEnabled()){
							System.out.println("Found an active links... Clicking " + e.getText());
							e.click();
							forceWait(5);
							break;
			}}}}}
		
		public void tfTest() throws Exception{
			if(condition1){System.out.println("Lawyerport conditional test: Pass");
			}
			if(!condition1){System.err.println("Lawyerport conditional test: Fail");}

			System.out.println("------------------");
		}
		
		public void tfTest2() throws Exception{
			if(condition2){System.out.println("Lawyerport conditional test: Pass");
			}
			if(!condition2){System.err.println("Lawyerport conditional test: Fail");}
		
			System.out.println("------------------");

		}
		
		public void tfTest3() throws Exception{
			if(condition1 && condition2){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}//{result="Pass";}else{result="Fail";}

			System.out.println("------------------");

		}
		
		public void tfTest4() throws Exception{
			if(condition1 && condition2 && condition3){System.out.println("Lawyerport conditional test: Pass");
			}else{System.err.println("Lawyerport conditional test: Fail");}

			System.out.println("------------------");

		}
		
		
		
		//Returns a random name from a list of common first names
		public String randomName(){
			
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("John");
			list.add("William");
			list.add("Benjamin");
			list.add("Robert");
			list.add("David");
			list.add("Noah");
			list.add("Liam");
			list.add("Ryan");
			list.add("Bryan");
			list.add("Ethan");
			list.add("Eric");
			list.add("Alexander");
			list.add("Jason");
			list.add("Joshua");
			list.add("Neil");
			list.add("Michael");
			list.add("Alejandro");
			list.add("Dylan");
			list.add("James");
			list.add("Jose");
			list.add("Javiar");
			list.add("James");
			list.add("Matthew");
			list.add("Muhammad");
			list.add("Martin");
			list.add("Joseph");
			list.add("Richard");
			list.add("Charles");
			list.add("Thomas");
			list.add("Nicholas");
			list.add("Jack");
			list.add("Sebastian");
			list.add("Mary");
			list.add("Patricia");
			list.add("Linda");
			list.add("Barbara");
			list.add("Elizabeth");
			list.add("Jennifer");
			list.add("Sophia");
			list.add("Sarah");
			list.add("Emily");
			list.add("Olivia");
			list.add("Emma");
			list.add("Miriam");
			list.add("Maria");
			list.add("Margaret");
			list.add("Dorothy");
			list.add("Rachel");
			list.add("Ashley");
			list.add("Julia");
			list.add("Angela");
			list.add("Zoe");
			list.add("Mia");
			list.add("Isaac");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Name: " + name);
			return name;
		}

		//Returns a random name from a list of common surnames 
		public String randomLastName(){
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("Jones");
			list.add("Smith");
			list.add("Williams");
			list.add("Stewart");
			list.add("Martinson");
			list.add("Cohen");
			list.add("Levy");
			list.add("Hogan");
			list.add("Patel");
			list.add("Kennedy");
			list.add("Davis");
			list.add("Miller");
			list.add("Taylor");
			list.add("Anderson");
			list.add("White");
			list.add("Jackson");
			list.add("Garcia");
			list.add("Martinez");
			list.add("Lee");
			list.add("Lewis");
			list.add("Walker");
			list.add("Hall");
			list.add("Allen");
			list.add("Young");
			list.add("King");
			list.add("Lopez");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Last Name: " + name);
			return name;
		}
		
		public void write(String s){
			System.out.println(s);
		}
		
		
		@Deprecated
		/** 
		 * @param s -0 output string
		 * @param i - color selector (34 = Blue)
		 * @return
		 */
		public void color(String s, Integer i){
			s = "\u001b[1;"+ i+ "m"+  s + "\u001b[0m";
			System.out.println(s);	
		}
	
	
}


	
		
	
		
		
