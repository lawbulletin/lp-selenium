package lawyerportdominion;

public class LoginCredentials {
	
	//private static String website = "https://wwwqa.lawyerport.com";
	private static String website = "NA";
	private static String user = "NA";
	private static String password = "NA";
	private static String client = "NA";
	
	public static void inDev() {
		website = "https://wwwdev.lawyerport.com/";
		user = "automatedtester2000@lawyerport.com";
		password = "test";
		client = "client200";
	}
	
	public static void inQA() {
		website = "https://wwwqa.lawyerport.com/";
		user = "bgalatzer-levy@lbpc.com";
		password = "test1";
		client = "Client2000";
	}
	
	public static void inProd() {
		website = "https://beta1.lawyerport.com/";
		user = "automatedtester2000@lawyerport.com";
		password = "test";
		client = "Client2000";
	}
	
	public static String getWebsite() {
		return website;
	}

	public static String getUsername() {
		return user;
	}

	public static String getPassword() {
		return password;
	}

	public static String getClient(){
		return client;
	}
}
