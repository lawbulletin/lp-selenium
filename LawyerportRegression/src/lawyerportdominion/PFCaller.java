package lawyerportdominion;

import org.openqa.selenium.support.PageFactory;

import pagefactories.AppellateSummariesPageFactory;
import pagefactories.CDPageFactory;
import pagefactories.CaseDetailsPageFactory;
import pagefactories.ClientCodeFactory;
import pagefactories.CourtDocketResultFactory;
import pagefactories.CourtRulePageFactory;
import pagefactories.DirectoryPageFactory;
import pagefactories.DirectoryResultFactory;
import pagefactories.JVRPageFactory;
import pagefactories.LPMainPageFactory;
import pagefactories.LandingPageFactory;
import pagefactories.LegalMarketPlacePageFactory;
import pagefactories.LegalResearchPageFactory;
import pagefactories.LoginPageFactory;
import pagefactories.NewsPageFactory;
import pagefactories.OrgDirectoryPageFactory;
import pagefactories.OrgProfilePageFactory;
import pagefactories.ProfilePageFactory;
import pagefactories.SearchResultsFactory;
import seleniumwebdriver.SeleniumDriver;


public class PFCaller{
	
	public static  ClientCodeFactory ccFactory(){
		ClientCodeFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), ClientCodeFactory.class);
		return (ClientCodeFactory) pfName;
	}
	
	public static LegalResearchPageFactory lrFactory(){
		LegalResearchPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LegalResearchPageFactory.class);
		return (LegalResearchPageFactory) pfName;
	}
	
	public static LandingPageFactory landingFactory(){
		LandingPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LandingPageFactory.class);
		return (LandingPageFactory) pfName;
	}
	
	public static LPMainPageFactory lpMainFactory(){
		LPMainPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LPMainPageFactory.class);
		return (LPMainPageFactory) pfName;
	}
	
	public static CourtRulePageFactory crFactory(){
		CourtRulePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CourtRulePageFactory.class);
		return (CourtRulePageFactory) pfName;
	}
	
	public static OrgProfilePageFactory orgProfileFactory(){
		OrgProfilePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), OrgProfilePageFactory.class);
		return (OrgProfilePageFactory) pfName;
	}
	
	public static CDPageFactory cdFactory(){
		CDPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CDPageFactory.class);
		return (CDPageFactory) pfName;
	}
	
	public static DirectoryPageFactory directoryFactory(){
		DirectoryPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), DirectoryPageFactory.class);
		return (DirectoryPageFactory) pfName;
	}
	
	public static DirectoryResultFactory dirResultsFactory(){
		DirectoryResultFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), DirectoryResultFactory.class);
		return pfName;
	}
	
	public static JVRPageFactory jvrFactory(){
		JVRPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), JVRPageFactory.class);
		return (JVRPageFactory) pfName;
	}
	
	public static OrgDirectoryPageFactory orgFactory(){
		OrgDirectoryPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), OrgDirectoryPageFactory.class);
		return (OrgDirectoryPageFactory) pfName;
	}
	
	public static CaseDetailsPageFactory caseDetailsFactory(){
		CaseDetailsPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CaseDetailsPageFactory.class);
		return (CaseDetailsPageFactory) pfName;}
	
	public static AppellateSummariesPageFactory appSummaryFactory(){
		AppellateSummariesPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), AppellateSummariesPageFactory.class);
		return (AppellateSummariesPageFactory) pfName;}

	
	public static LegalMarketPlacePageFactory lmFactory(){
		LegalMarketPlacePageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LegalMarketPlacePageFactory.class);
		return (LegalMarketPlacePageFactory) pfName;}

	public static NewsPageFactory newsFactory(){
		NewsPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), NewsPageFactory.class);
		return (NewsPageFactory) pfName;}

	public static LoginPageFactory loginFactory(){
		LoginPageFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), LoginPageFactory.class);
		return pfName;}

	public static ProfilePageFactory profileFactory(){
		ProfilePageFactory pfName =  PageFactory.initElements(SeleniumDriver.getDriver(), ProfilePageFactory.class);
		return pfName;}

	public static SearchResultsFactory searchResultFactoy(){
		SearchResultsFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), SearchResultsFactory.class);
		return pfName;}
	
	public static CourtDocketResultFactory cdResults(){
		CourtDocketResultFactory pfName = PageFactory.initElements(SeleniumDriver.getDriver(), CourtDocketResultFactory.class);
		return pfName;}
	
	}



