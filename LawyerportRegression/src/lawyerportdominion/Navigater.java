package lawyerportdominion;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pagefactories.LPMainPageFactory;
import pagefactories.LegalResearchPageFactory;
import seleniumwebdriver.SeleniumDriver;

import com.google.common.base.Function;


public class Navigater{
	LPMainPageFactory main = PFCaller.lpMainFactory();
	LegalResearchPageFactory legal = PFCaller.lrFactory();
	WebDriverWait tock = AbstractSprint.tock;
	WebElement e = AbstractSprint.e;
	
	/**
	 * @param courtDate - Must be a date in the format MM/dd/yyyy
	 * @return - long epoch (epoch -> courtDate represented as a Epoch & Unix Time-stamp)
	 * @throws Exception
	 */
	public static long DateToEpoch(String courtDate) throws Exception{
		long epoch = 0;
		Date date;
		//System.out.println("date");
		DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		date = format.parse(courtDate);
		
		Calendar myDate = new GregorianCalendar();
		myDate.setTime(date);		
		epoch = myDate.getTimeInMillis();
		epoch = epoch/1000;
		
		System.out.println(epoch);
	
		return epoch;
	}
	
	public void  Directory(){
		main.getDirectoryTab().click();
	}
	
	public void Research(){
		main.getLegalResearchLink().click();
	
	}
	
	public void News(){
		main.getNewsLink().click();
	
	}
	
	public  void MarketPlace(){
		main.getLegalMarketplace().click();
	}
	
	public  void Events(){
		main.getEvents().click();
	}
	
	public void navCourtDocket() {
		main.getLegalResearchLink().click();
	
	}
	
	public void navJVR(){
		main.getLegalResearchLink().click();
		legal.getJVS().click();
	
	}
	
	public void navAppellateSummaries() {
		main.getLegalResearchLink().click();
		legal.getAppSummaries().click();
	}
	
	//waits for an element for to appear on page, the default time is 30 seconds.
	//default time to wait for element is 30 seconds.
	public static WebElement waitForElement(final By Locator) throws Exception {
			 WebDriverWait wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);

		    WebElement function = wait.until(new Function<WebDriver, WebElement>() {
		    	
		    	public WebElement apply(WebDriver driver) {
		            return driver.findElement(Locator);
		        }
		    });

		    return  function;
		}
		
		

	public WebElement LPElement(String elementID){
		tock.until(ExpectedConditions.presenceOfElementLocated(By.id(elementID)));
		e = SeleniumDriver.getDriver().findElement(By.id(elementID));
		
		return e;
	}

		
	public void selectStreamingContainer(WebElement locator, boolean chicagoDaily, boolean chicagoLawyer, boolean clickMore){
		WebElement steramingCOntainer = locator; 
		
	}


			//checks to see if edit button is visible or not. 
			public boolean isEditButPresent() throws Exception {
				try {
			    	if (isElementPresent(By.xpath("(//div[contains(@class, 'hidden')])[2]"))) { 
						System.out.println("Edit option is disable");
					} else {
						waitForElement(By.id("edit-profile")).click();
						System.out.println("clicking on edit profile");
					}
			    }catch (Throwable t){
			    	System.out.println("Cannot find Edit Button");
					return false;
				}
			    return true;
			}

			// checks to see if an element is displayed or not. 
	public static boolean isElementPresent(By locator) throws Exception, IOException,SQLException {
				try {
			    	waitForElement(locator).isDisplayed();
			    	//System.out.println("element exists");
			    	System.out.println("element exists");
			    	return true;
			    }catch (Throwable t ){
			    	String s = locator.toString();
			    	System.out.println(s + " element does not exists"); 	
					return false;
				}
	}

					//checks to see if element is clickable
	public static boolean isElementEnable(By locator) throws Exception, IOException,SQLException { 
						try {
					    	waitForElement(locator).isEnabled();
					    	//System.out.println("element exists");
					    	System.out.println("element exists");
					    	return true;
					    }catch (Throwable t ){
					    	System.out.println("element does not exists");  	
					    	return false;
						}
	}

	public static void forceWait(int sleepTime){
	//	System.out.println("human wait of: " + sleepTime);
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
			//do nothing just wait
		}
	}
}
