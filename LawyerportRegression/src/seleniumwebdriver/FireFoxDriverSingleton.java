package seleniumwebdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public final class FireFoxDriverSingleton {

	private static WebDriver fireFoxDriver = null;

	/**
	 * @return
	 */
	protected static WebDriver getFireFoxDriver() {
		if (fireFoxDriver != null) {
			return fireFoxDriver;
		}
		fireFoxDriver = new FirefoxDriver();
		fireFoxDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		return fireFoxDriver;
	}

	/**
	 * Quits the Internet Explorer Driver, clears the cache and nullifies the
	 * driver object.
	 */
	protected static void quit() {
		if (fireFoxDriver != null) {
			fireFoxDriver.manage().deleteAllCookies();
			fireFoxDriver.quit();
			fireFoxDriver = null;
		}
	}

}
