package seleniumwebdriver;

import java.net.MalformedURLException;
import java.net.URL;

import lawyerportdominion.PFCaller;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class gridNG {
	
	 WebDriver driver;
		pagefactories.LandingPageFactory land = PFCaller.landingFactory();
		pagefactories.LoginPageFactory login = PFCaller.loginFactory();
		pagefactories.LPMainPageFactory main = PFCaller.lpMainFactory();
		
		
		
		@Test
		public static void forceWait(int sleepTime){
			//	System.out.println("human wait of: " + sleepTime);
				long startTime = System.currentTimeMillis();
				while ((System.currentTimeMillis() - startTime) < sleepTime * 1000){
					//do nothing just wait
				}
			}
	
  @Test(dataProvider = "dp")
  public void f(Integer n, String s) {
  }

  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, "a" },
      new Object[] { 2, "b" },
    };
  }
  @BeforeClass
  public void beforeClass() throws MalformedURLException {
	    DesiredCapabilities capability = DesiredCapabilities.firefox();
	    capability.setBrowserName("firefox" );
	    // Set the platform we want our tests to run on
	   
	    driver = new RemoteWebDriver(new URL("http://<grid hub>:4444/wd/hub"), capability);

	    driver.get("https://beta1.lawyerport.com/home");
	    forceWait(2);
	    land.getsignInButton().click();  
	  
  }

  @AfterClass
  public void afterClass() {
	  
	  forceWait(3);
	    driver.quit();
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
