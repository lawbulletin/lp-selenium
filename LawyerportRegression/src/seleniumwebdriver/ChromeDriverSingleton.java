package seleniumwebdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public final class ChromeDriverSingleton {

	private static WebDriver chromeDriver = null;

	/**
	 * @return
	 */
	protected static WebDriver getChromeDriver() {
		if (chromeDriver != null) {
			return chromeDriver;
		}
		System.setProperty("webdriver.chrome.driver",
				//"C:\\Users\\arms\\Documents\\ARM Resources\\chromedriver.exe");
		"C:\\Users\\bgalatzer-levy\\Desktop\\JavaLibs\\chromedriver.exe");
		chromeDriver = new ChromeDriver();
		chromeDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		return chromeDriver;
	}

	/**
	 * Quits the Internet Explorer Driver, clears the cache and nullifies the
	 * driver object.
	 */
	protected static void quit() {
		if (chromeDriver != null) {
			chromeDriver.manage().deleteAllCookies();
			chromeDriver.quit();
			chromeDriver = null;
		}
	}
}
